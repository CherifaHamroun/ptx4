#!/usr/bin/env python
# -*- coding: UTF-8 -*-

###################################################################################
# Outils de lecture de texte dans OOo Writer, OOoDraw et OooImpress
# en utilisant un outil de synthèse vocale.
#
# Copyright (c) 2013-2014 by Marie-Pierre Brungard
#
# GNU General Public Licence (GPL) version 3
#
# This is a free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this software; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
###################################################################################

import threading
import tempfile
import os
import string
import time
import math
import gettext
from gettext import gettext as _

try:
	import uno
	import unohelper
except:
	pass

try:
	# subprocess n'existe pas avant python 2.4
	import subprocess
except:
	import commands as subprocess

try:
	from com.sun.star.awt import XActionListener
	from com.sun.star.task import XJobExecutor
	from com.sun.star.task import XJob
	from com.sun.star.awt import XKeyHandler
	
	from com.sun.star.awt.KeyModifier import MOD2
	from com.sun.star.awt.Key import W as keyW
	from com.sun.star.awt.Key import P as keyP
	from com.sun.star.awt.Key import K as keyK
	from com.sun.star.awt.Key import S as keyS
	from com.sun.star.awt.Key import R as keyR
	from com.sun.star.awt.Key import SPACE as keySpace
	from com.sun.star.awt.Key import RETURN as keyReturn
	from com.sun.star.awt.Key import TAB as keyTab
	from com.sun.star.awt.Key import COMMA as keyComma
	from com.sun.star.awt.Key import POINT as keyPoint
	from com.sun.star.awt.Key import LEFT as keyLeft
	from com.sun.star.awt.Key import RIGHT as keyRight
	from com.sun.star.awt.Key import ADD as keyAdd
	from com.sun.star.awt.Key import SUBTRACT as keySubst
	from com.sun.star.awt.Key import MULTIPLY as keyMult
	from com.sun.star.awt.Key import DIVIDE as keyDiv
	from com.sun.star.awt.Key import LESS as keyLess
	from com.sun.star.awt.Key import GREATER as keyGreater
	from com.sun.star.awt.Key import EQUAL as keyEqual
except:
	pass

try:
	# python 3
	from urllib.request import pathname2url
	from urllib.request import url2pathname
	from functools import reduce
	from urllib.parse import urlparse
	from urllib.parse import unquote
except:
	# python 2
	from urllib import pathname2url
	from urllib import url2pathname
	from urlparse import urlparse
	from urllib import unquote

try:
	import winsound
except:
	pass

try:
	import pico
except:
	pass

# create LANG environment variable
import sys
if sys.platform.startswith('win'):
	import locale
	if os.getenv('LANG') is None:
		lang, enc = locale.getdefaultlocale()
		os.environ['LANG'] = lang

# Thread de synthèse/lecture
picothread = None
surligneurthread = None

# codes de touches qui déclenchent la lecture en cours de frappe
__codesLectureFrappe__ = [keySpace, keyReturn, keyPoint, keyComma, keyTab,
							keyMult, keyDiv, keyAdd, keySubst, keyLess, keyGreater, keyEqual]

# information sur la lecture en cours
__modePhrase__ = 1
__modeParagraphe__ = 2
__modeMot__ = 3
__modeFrappe__ = 4
__modeSelection__ = 0

# sens de parcours
__lectureSuivant__ = 1
__lecturePrecedent__ = 2

memoShortcuts = {}
memoTyping = {}

###################################################################################
# Fonctions utilitaires
###################################################################################
"""
	Get the name of the extension directory
"""
def getExtensionDirectory():
	"""Get the name of the extension directory """
	try:
		url = urlparse(__file__)
		return os.path.dirname(os.path.abspath(url2pathname(url.path)))
	except:
		pass

	try:
		pip = uno.getComponentContext().getValueByName("/singletons/com.sun.star.deployment.PackageInformationProvider")
		packageLocation = pip.getPackageLocation("PicoSvoxOOo")
		url = urlparse(packageLocation)
		return url2pathname(url.path)
	except:
		pass

	return ""

"""

"""
def i18n():
	localdir = os.sep.join([getExtensionDirectory(), 'locale'])	
	gettext.bindtextdomain('picosvoxooo', localdir)
	gettext.textdomain('picosvoxooo')

"""
	passage éventuel en unicode (sauf pour Python 3)
"""
def __u__(txt):
	try:
		return unicode(txt, 'utf-8')
	except:
		return txt

def createUnoService(serviceName, ctx=None):
	if ctx is None:
		ctx = uno.getComponentContext()

	sm = ctx.ServiceManager
	try:
		serv = sm.createInstanceWithContext(serviceName, ctx)
	except:
		serv = sm.createInstance(serviceName)

	return serv

def createUnoStruct(cTypeName):
	"""Create a UNO struct and return it.
	Similar to the function of the same name in OOo Basic. -- Copied from Danny Brewer library
	"""
	sm = uno.getComponentContext().ServiceManager
	oCoreReflection = sm.createInstance( "com.sun.star.reflection.CoreReflection" )
	# Get the IDL class for the type name
	oXIdlClass = oCoreReflection.forName( cTypeName )
	# Create the struct.
	oReturnValue, oStruct = oXIdlClass.createObject( None )
	return oStruct

"""
	calcule l'intersection entre 2 formes
"""
def intersection(shape1, shape2):

	if shape1 is None or shape2 is None:
		return 0

	x1min = shape1.Position.X
	x1max = x1min + shape1.Size.Width
	y1min = shape1.Position.Y
	y1max = y1min + shape1.Size.Height
		
	x2min = shape2.Position.X
	x2max = x2min + shape2.Size.Width
	y2min = shape2.Position.Y
	y2max = y2min + shape2.Size.Height

	if x1max < x2min or x2max < x1min:
		return 0
	if y1max < y2min or y2max < y1min:
		return 0
	
	# recherche des limites d'intersection
	ximin = max(x1min, x2min)
	ximax = min(x1max, x2max)
	yimin = max(y1min, y2min)
	yimax = min(y1max, y2max)

	return 1.0*(ximax-ximin)*(yimax-yimin)

"""
	retrouve une forme de la couche "layout" a partir de la position d'une cible
"""
def findObjectsByPosition(oDrawPage, oDrawTarget, service = None):
	shapes = []
	if oDrawTarget is None:
		return shapes

	targetSurf = 1.0*oDrawTarget.Size.Width*oDrawTarget.Size.Height
	nNumShapes = oDrawPage.getCount()
	for x in range(nNumShapes):
		oShape = oDrawPage.getByIndex(x)
		if oShape != oDrawTarget:
			intersectSurf = intersection(oDrawTarget, oShape)
			if intersectSurf/targetSurf > 0:
				if service is None:
					shapes.append(oShape)
				else:
					if oShape.supportsService(service):
						shapes.append(oShape)
	return shapes

"""
	cherche à récupérer le texte d'une forme. Dans l'ordre : le champ Titre, le texte contenu et enfin le nom
"""
def getObjectValue(oShape):
	if oShape is None:
		return None

	res = ""
	if oShape.supportsService('com.sun.star.drawing.ControlShape'):
		# champ de formulaire
		try:
			res = oShape.getControl().Text
		except:
			pass
	else:
		# forme normale
		res = ""
		# première possibilité : la forme contient du texte
		try:
			res = oShape.getText().getString()
		except:
			pass
		if len(res) == 0:
			# deuxième possibilité : la forme a un champ Titre renseigné
			try:
				res = oShape.Title.strip()
			except:
				pass

	# dernière étape : évaluation éventuelle de la valeur
	try:
		return eval(res)
	except:
		pass

	return __u__(res)

######################################################################################
# Lecture de la configuration dans le fichier .picosvoxooo
######################################################################################
def readAppData():
	"""Lecture des informations dans le fichier .picosvoxooo"""
	appdata = '.picosvoxooo'
	if 'APPDATA' in os.environ:
		appdata = os.environ.get('APPDATA')+os.sep+'.picosvoxooo'
	elif 'HOME' in os.environ:
		appdata = os.environ.get('HOME')+os.sep+'.picosvoxooo'

	# get the configuration data
	adata = {}
	try:
		if os.path.isfile(appdata):
			fappdata = open(appdata)
			line = fappdata.read()
			fappdata.close()
			
			# eval gives the dict
			adata = eval(line)
	except:
		pass
	return adata

######################################################################################
# Sauvegarde de la configuration dans le fichier .picosvoxooo
######################################################################################
def saveAppData(nappdata, dappdata):
	"""Sauvegarde des informations dans le fichier .picosvoxooo"""
	appdata = '.picosvoxooo'
	if 'APPDATA' in os.environ:
		appdata = os.environ.get('APPDATA')+os.sep+'.picosvoxooo'
	elif 'HOME' in os.environ:
		appdata = os.environ.get('HOME')+os.sep+'.picosvoxooo'

	# first read the data
	adata = readAppData()
	try:
		# then introduce the data in the dict
		adata[nappdata] = dappdata
		
		# and now save the whole dict
		f = open(appdata, 'w')
		f.write(str(adata))
		f.close()
	except:
		pass
	return

######################################################################################
# Sauvegarde de la vitesse de lecture dans le fichier .picosvoxooo
######################################################################################
def saveMaskSpeed(speed):
	"""Sauvegarde de la vitesse de lecture dans le fichier .picosvoxooo"""
	saveAppData('__speed__', str(speed))

######################################################################################
# Lecture de la vitesse de lecture dans le fichier .picosvoxooo
######################################################################################
def handleMaskSpeed():
	"""Lecture de la vitesse de lecture dans le fichier .picosvoxooo"""

	# par défaut : 100
	speed = 100

	# read the file content
	adata = readAppData()

	# lecture dans la structure du fichier
	try:
		speed = int(adata['__speed__'])
	except:
		pass

	return speed

######################################################################################
# Sauvegarde de l'info de surlignage du texte lu dans le fichier .picosvoxooo
######################################################################################
def saveMaskMarker(marker):
	"""Sauvegarde de l'info de surlignage du texte lu dans le fichier .picosvoxooo"""
	saveAppData('__marker__', str(marker))

######################################################################################
# Lecture de l'info de surlignage du texte lu dans le fichier .picosvoxooo
######################################################################################
def handleMaskMarker():
	"""Lecture de l'info de surlignage du texte lu dans le fichier .picosvoxooo"""

	# par défaut : 1
	marker = 1

	# read the file content
	adata = readAppData()

	# lecture dans la structure du fichier
	try:
		marker = int(adata['__marker__'])
	except:
		pass

	return marker

######################################################################################
# Sauvegarde de l'info de lecture pendant la frappe dans le fichier .picosvoxooo
######################################################################################
def saveMaskTyping(marker):
	"""Sauvegarde de l'info de lecture pendant la frappe dans le fichier .picosvoxooo"""
	saveAppData('__typing__', marker)

######################################################################################
# Lecture de l'info de lecture pendant la frappe dans le fichier .picosvoxooo
######################################################################################
def handleMaskTyping():
	"""Lecture de l'info de lecture pendant la frappe dans le fichier .picosvoxooo"""

	# par défaut : non
	marker = False

	# read the file content
	adata = readAppData()

	# lecture dans la structure du fichier
	try:
		marker = adata['__typing__']
	except:
		pass

	return marker

######################################################################################
# Sauvegarde de l'info des raccourcis clavier dans le fichier .picosvoxooo
######################################################################################
def saveMaskShortcuts(marker):
	"""Sauvegarde de l'info des raccourcis clavier dans le fichier .picosvoxooo"""
	saveAppData('__shortcuts__', marker)

######################################################################################
# Lecture de l'info des raccourcis clavier dans le fichier .picosvoxooo
######################################################################################
def handleMaskShortcuts():
	"""Lecture de l'info des raccourcis clavier dans le fichier .picosvoxooo"""

	# par défaut : non
	marker = False

	# read the file content
	adata = readAppData()

	# lecture dans la structure du fichier
	try:
		marker = adata['__shortcuts__']
	except:
		pass

	return marker

######################################################################################
# Sauvegarde de la langue de lecture dans le fichier .picosvoxooo
######################################################################################
def saveMaskLanguage(lge):
	"""Sauvegarde de la langue de lecture dans le fichier .picosvoxooo"""
	saveAppData('__language__', lge)

######################################################################################
# Lecture de la langue de lecture dans le fichier .picosvoxooo
######################################################################################
def handleMaskLanguage():
	"""Lecture de la langue de lecture dans le fichier .picosvoxooo"""

	# par défaut : fr
	lge = 'fr-FR'

	# read the file content
	adata = readAppData()

	# lecture dans la structure du fichier
	try:
		lge = adata['__language__']
	except:
		pass

	return lge

######################################################################################
# Surlignage temporaire du texte en cours de lecture
######################################################################################
class ThreadSurligneur(threading.Thread):
	def __init__(self, txt = '', xTextViewCursor = None):
		threading.Thread.__init__(self)
		self.xTextViewCursor = xTextViewCursor
		self.l_txt = len(txt)
		self.speed = handleMaskSpeed()
		self.ncurs = None
	
	def run(self):
		if not self.xTextViewCursor is None:
			self.ncurs = self.xTextViewCursor.getText().createTextCursorByRange(self.xTextViewCursor)
			self.xTextViewCursor.collapseToEnd()

			self.ncurs.setPropertyValue('CharBackColor', 0x00ffff00)
			time.sleep(self.l_txt*10/(self.speed*math.log10(self.l_txt)))
			self.stop()

	def stop(self):
		if not self.ncurs is None:
			self.ncurs.setPropertyValue('CharBackColor', 0x00ffffff)
			del self.ncurs
			self.ncurs = None

###################################################################################
# Thread lecteur
###################################################################################
class ThreadLecteur(threading.Thread):
	__pico_lge__ = {'fr-FR':'fr', 'it-IT':'it', 'es-ES':'es', 'de-DE':'de', 'en-GB':'en-gb', 'en-US':'en-us'}
	__temp_wav_file__ = tempfile.mkstemp(suffix=".wav", prefix="svoxpico_")[1]
	
	def __init__(self, txt = '', wavfile = "", speed = -1):
		threading.Thread.__init__(self)

		# lecture de la langue et de la vitesse de lecture
		self.langue = handleMaskLanguage()
		self.speed = handleMaskSpeed()

		self.text = txt
		if speed > 0:
			self.speed = speed
		self.synthd = None
		self.player = None
		self._stopevent = threading.Event()
	
	def run(self):
		## synthèse
		langue = ThreadLecteur.__pico_lge__[self.langue]
		base_path = getExtensionDirectory()
		
		base_path = unquote(base_path)
		if not base_path.endswith('pythonpath'):
			base_path = os.sep.join([base_path, 'pythonpath'])
		self.synthd = pico.SynthDriver(base_path)
		self.synthd.load_resources(*self.synthd.voice_resources[langue])
		self.synthd.rate = int(self.speed/2)
		pico.speechDictHandler.initialize(langue, base_path)
		data1 = '<genfile file="'+ThreadLecteur.__temp_wav_file__+'">'+pico.speechDictHandler.processText(self.text)+'</genfile>'
		self.synthd.speak_text(self.synthd.build_string(data1))
		del self.synthd
		self.synthd = None

		# lecture du fichier généré
		if not self._stopevent.isSet():
			if os.name == 'posix':
				try:
					# appel de aplay en mode non verbeux
					self.player = subprocess.Popen(["aplay", "-q", ThreadLecteur.__temp_wav_file__])
				except:
					pass
				while self.player.poll() is None and not self._stopevent.isSet():
					self._stopevent.wait(0.1)
				self.player = None
			else:
				# Windows (appel non bloquant)
				self.player = True
				winsound.PlaySound(ThreadLecteur.__temp_wav_file__, winsound.SND_FILENAME|winsound.SND_ASYNC)
		
	def stop(self):
		self._stopevent.set()
		if not self.synthd is None:
			self.synthd.stop()

		if not self.player is None:
			if os.name == 'posix':
				self.player.terminate()
			else:
				winsound.PlaySound(None, winsound.SND_ASYNC)
				self.player = None

###################################################################################
# Lit le passage courant sous le curseur
###################################################################################
class Lire():
	modeLecture = __modeParagraphe__ # mode de lecture par défaut
	
	"""Lit le passage courant sous le curseur"""
	def __init__(self, xDocument):
		self.xDocument = xDocument
		self.xController = self.xDocument.getCurrentController()

	def paragraphe(self):
		if not self.xDocument.supportsService("com.sun.star.text.TextDocument"):
			return False
		Lire.modeLecture = __modeParagraphe__

		xTextViewCursor = self.xController.getViewCursor()
		xtr = getXTextRange(self.xDocument)
		if xtr == None:
			return False
		if not xtr.isStartOfParagraph():
			xtr.gotoStartOfParagraph(False)
		if not xtr.isEndOfParagraph():
			xtr.gotoEndOfParagraph(True)
		xTextViewCursor.gotoRange(xtr, False)
		__lire_texte__(xtr.getString(), xTextViewCursor)
		del xtr
		
	def phrase(self):
		if not self.xDocument.supportsService("com.sun.star.text.TextDocument"):
			return False
		Lire.modeLecture = __modePhrase__

		xTextViewCursor = self.xController.getViewCursor()
		xtr = getXTextRange(self.xDocument)
		if xtr == None:
			return False
		if not xtr.isStartOfSentence():
			xtr.gotoStartOfSentence(False)
		if not xtr.isEndOfSentence():
			xtr.gotoEndOfSentence(True)
		xTextViewCursor.gotoRange(xtr, False)
		__lire_texte__(xtr.getString(), xTextViewCursor)
		del xtr

	def selection(self):
		Lire.modeLecture = __modeSelection__
		xtr = getXTextRange(self.xDocument)
		if xtr == None:
			return False
				
		if self.xDocument.supportsService("com.sun.star.text.TextDocument"):
			# récupération du curseur physique
			xTextViewCursor = self.xController.getViewCursor()
			if len(xtr.getString()) == 0:
				if not xtr.isStartOfWord():
					xtr.gotoStartOfWord(False)
				if not xtr.isEndOfWord():
					xtr.gotoEndOfWord(True)
			Lire.modeLecture = __modeMot__
			xTextViewCursor.gotoRange(xtr, False)
			__lire_texte__(xtr.getString(), xTextViewCursor)
		else:
			oDrawPage = self.xController.getCurrentPage()

			spc = getObjectValue(xtr)
			if len(spc) == 0:
				lsh = findObjectsByPosition(oDrawPage, xtr)
				if len(lsh) > 0:
					spc = getObjectValue(lsh[0])
				del lsh
			if len(spc) > 0:
				__lire_texte__(spc)
		del xtr

	def frappe(self, kcode):
		if not self.xDocument.supportsService("com.sun.star.text.TextDocument"):
			return False
		Lire.modeLecture = __modeFrappe__
	
		xtr = getXTextRange(self.xDocument)
		if xtr == None:
			return False
		if not xtr.isStartOfWord():
			xtr.gotoStartOfWord(False)
		if not xtr.isEndOfWord():
			xtr.gotoEndOfWord(True)

		txtALire = (xtr.getString()+kcode.value).strip()
		__lire_texte__(txtALire, None, False)
		del xtr

###################################################################################
# Déplacement du curseur vers le passage suivant ou le passage précédent
###################################################################################
def __deplacement__(xDocument, deplacement=__lectureSuivant__):
	"""Déplacement du curseur vers le passage suivant ou le passage précédent"""
	if Lire.modeLecture == __modeSelection__:
		return False

	# i18n
	i18n()

	# récupération du curseur physique
	xViewCursorSupplier = xDocument.getCurrentController()
	xTextViewCursor = xViewCursorSupplier.getViewCursor()

	#if deplacement == __lectureSuivant__:
		#xTextViewCursor.collapseToEnd()
	#else:
		#xTextViewCursor.collapseToStart()

	xtr = xTextViewCursor.getText().createTextCursorByRange(xTextViewCursor)
	alire = ''
	if deplacement == __lecturePrecedent__:
		if Lire.modeLecture == __modeParagraphe__:
			xtr.gotoStartOfParagraph(False)
			while len(alire) == 0:
				# passage au paragraphe précédent
				if not xtr.gotoPreviousParagraph(False):
					__lire_texte__(__u__(_("debut_texte")))
					del xtr
					return
				xtr.gotoEndOfParagraph(True)
				alire = xtr.getString()
		elif Lire.modeLecture == __modePhrase__:
			# passage à la phrase précédente
			xtr_p = xtr.getText().createTextCursorByRange(xtr)
			xtr_p.gotoStartOfParagraph(False)
			while len(alire) == 0:
				xtr.gotoStartOfSentence(False)
				if xtr_p.getText().compareRegionStarts(xtr_p, xtr) == 0:
					if not xtr_p.gotoPreviousParagraph(False):
						__lire_texte__(__u__(_("debut_texte")))
						del xtr
						del xtr_p
						return
					# dernière phrase du paragraphe
					xtr.gotoRange(xtr_p, False)
					xtr.gotoEndOfParagraph(False)
					xtr.gotoStartOfSentence(False)
				else:
					if not xtr.gotoPreviousSentence(False):
						__lire_texte__(__u__(_("debut_texte")))
						del xtr
						del xtr_p
						return
				xtr.gotoEndOfSentence(True)
				alire = xtr.getString()
			del xtr_p
		else:
			# passage au mot précédent
			xtr_p = xtr.getText().createTextCursorByRange(xtr)
			xtr_p.gotoStartOfParagraph(False)
			while len(alire) == 0:
				xtr.gotoStartOfWord(False)
				if xtr_p.getText().compareRegionStarts(xtr_p, xtr) < 0:
					xtr.gotoRange(xtr_p, False)
				elif xtr_p.getText().compareRegionStarts(xtr_p, xtr) == 0:
					if not xtr_p.gotoPreviousParagraph(False):
						__lire_texte__(__u__(_("debut_texte")))
						del xtr
						del xtr_p
						return
					# dernier mot de la dernière phrase du paragraphe
					xtr.gotoRange(xtr_p, False)
					xtr.gotoEndOfParagraph(False)
					xtr.gotoEndOfSentence(False)
					xtr.gotoStartOfWord(False)
				else:
					if not xtr.gotoPreviousWord(False):
						__lire_texte__(__u__(_("debut_texte")))
						del xtr
						del xtr_p
						return
				xtr.gotoEndOfWord(True)
				alire = xtr.getString()

		# lecture du texte sélectionné
		xTextViewCursor.gotoRange(xtr, False)
		__lire_texte__(alire, xTextViewCursor)

	else:
		if Lire.modeLecture == __modeParagraphe__:
			# passage au paragraphe suivant
			while len(alire) == 0:
				if not xtr.gotoNextParagraph(False):
					__lire_texte__(__u__(_("fin_texte")))
					del xtr
					return
				xtr.gotoEndOfParagraph(True)
				alire = xtr.getString()
		elif Lire.modeLecture == __modePhrase__:
			# passage à la phrase suivante
			xtr_p = xtr.getText().createTextCursorByRange(xtr)
			xtr_p.gotoEndOfParagraph(False)
			while len(alire) == 0:
				xtr.gotoEndOfSentence(False)
				if xtr_p.getText().compareRegionEnds(xtr_p, xtr) == 0:
					if not xtr_p.gotoNextParagraph(False):
						__lire_texte__(__u__(_("fin_texte")))
						del xtr
						del xtr_p
						return
					# première phrase du paragraphe
					xtr.gotoRange(xtr_p, False)
					xtr.gotoStartOfParagraph(False)
					xtr.gotoStartOfSentence(False)
				else:
					if not xtr.gotoNextSentence(False):
						__lire_texte__(__u__(_("fin_texte")))
						del xtr
						del xtr_p
						return
				xtr.gotoEndOfSentence(True)
				alire = xtr.getString()
			del xtr_p
		else:
			# passage au mot suivant
			xtr_p = xtr.getText().createTextCursorByRange(xtr)
			xtr_p.gotoEndOfParagraph(False)
			while len(alire) == 0:
				xtr.gotoEndOfWord(False)
				if xtr_p.getText().compareRegionEnds(xtr_p, xtr) > 0:
					xtr.gotoRange(xtr_p, False)
				elif xtr_p.getText().compareRegionEnds(xtr_p, xtr) == 0:
					if not xtr_p.gotoNextParagraph(False):
						__lire_texte__(__u__(_("fin_texte")))
						del xtr
						del xtr_p
						return
					# premier mot de la première phrase du paragraphe
					xtr.gotoRange(xtr_p, False)
					xtr.gotoStartOfParagraph(False)
					xtr.gotoStartOfSentence(False)
					xtr.gotoStartOfWord(False)
				else:
					if not xtr.gotoNextWord(False):
						__lire_texte__(__u__(_("fin_texte")))
						del xtr
						del xtr_p
						return
				xtr.gotoEndOfWord(True)
				alire = xtr.getString()


		# lecture du texte sélectionné
		xTextViewCursor.gotoRange(xtr, False)
		__lire_texte__(alire, xTextViewCursor)
		del xtr

	return True

###################################################################################
# Récupère le textRange correspondant au mot sous le curseur ou à la sélection 
###################################################################################
def getXTextRange(xDocument):
	"""Récupère le textRange correspondant au mot sous le curseur ou à la sélection"""

	#the writer controller impl supports the css.view.XSelectionSupplier interface
	xSelectionSupplier = xDocument.getCurrentController()
 
	xIndexAccess = xSelectionSupplier.getSelection()
	try:
		count = xIndexAccess.getCount()
	except:
		return None
	xTextRange = xIndexAccess.getByIndex(0)
	xText = xTextRange.getText()
	return xText.createTextCursorByRange(xTextRange)

###################################################################################
# Applique pico2wave et joue le fichier son généré
###################################################################################
def __lire_texte__(txt, xTextViewCursor=None, surligner=True, wfile="", spd=-1):
	global perroquet
	global picothread
	global surligneurthread
	
	perroquet = txt
	if len(txt) == 0:
		return True

	# arrêt préalable de la lecture en cours s'il y a
	__stop_lecture__()
	
	# prétraitement préalable : le texte est lu ligne par ligne
	ltxt = txt.split(os.linesep)
	for i in range(len(ltxt)):
		ltxt[i] = ltxt[i].strip()
		if not ltxt[i].endswith(('.',';','!','?')):
			ltxt[i] += '.'
	ntxt = ' '.join(ltxt)
	del ltxt

	if handleMaskMarker() == 1 and surligner:
		surligneurthread = ThreadSurligneur(ntxt, xTextViewCursor)
		surligneurthread.start()
	else:
		if not xTextViewCursor is None:
			xTextViewCursor.collapseToEnd()

	picothread = ThreadLecteur(ntxt, wavfile=wfile, speed=spd)
	picothread.start()

	return True

#########################################################################################################
#########################################################################################################
###									   FONCTIONS D'INTERFACE
#########################################################################################################
#########################################################################################################

###################################################################################
# Lit la phrase courante sous le curseur
###################################################################################
def lire_phrase( args=None ):
	"""Lit la phrase courante sous le curseur"""
	lit = Lire(XSCRIPTCONTEXT.getDocument())
	lit.phrase()

###################################################################################
# Lit la phrase courante sous le curseur
###################################################################################
def lire_paragraphe( args=None ):
	"""Lit le paragraphe courant sous le curseur"""
	lit = Lire(XSCRIPTCONTEXT.getDocument())
	lit.paragraphe()

###################################################################################
# Lit le texte sélectionné
###################################################################################
def lire_selection( args=None ):
	"""Lit le texte sélectionné"""
	lit = Lire(XSCRIPTCONTEXT.getDocument())
	lit.selection()

###################################################################################
# Arrête la lecture
###################################################################################
def stop_lecture(args=None):
	"""Arrête la lecture"""
	__stop_lecture__(XSCRIPTCONTEXT.getDocument())

def __stop_lecture__(xDocument = None):
	global picothread
	global surligneurthread

	"""Arrête la lecture"""
	if not xDocument is None:
		if xDocument.supportsService("com.sun.star.text.TextDocument"):
			# récupération du curseur physique
			xViewCursorSupplier = xDocument.getCurrentController()
			xTextViewCursor = xViewCursorSupplier.getViewCursor()
			xTextViewCursor.collapseToEnd()

	if not picothread is None:
		picothread.stop()
	if not surligneurthread is None:
		surligneurthread.stop()

###################################################################################
# Répète la dernière chose lue
###################################################################################
def repeter(args=None):
	"""Répète la dernière chose lue"""
	__repeter__()

def __repeter__():
	global perroquet
	
	__lire_texte__(perroquet)

###################################################################################
# En fonction du mode de lecture passe au passage suivant
###################################################################################
def passage_suivant(args=None):
	"""En fonction du mode de lecture passe au passage suivant"""
	__deplacement__(XSCRIPTCONTEXT.getDocument(), __lectureSuivant__)

###################################################################################
# En fonction du mode de lecture passe au passage précédent
###################################################################################
def passage_precedent(args=None):
	"""En fonction du mode de lecture passe au passage précédent"""
	__deplacement__(XSCRIPTCONTEXT.getDocument(), __lecturePrecedent__)

def createRadioButton(dialogModel, px, py, name, index, label, etat, w=44):
	checkBP = dialogModel.createInstance("com.sun.star.awt.UnoControlRadioButtonModel")
	checkBP.PositionX = px
	checkBP.PositionY = py
	checkBP.Width  = w
	checkBP.Height = 10
	checkBP.Name = name
	checkBP.TabIndex = index
	checkBP.State = etat
	checkBP.Label = label
	return checkBP

###################################################################################
# Crée une boite de dialogue pour configurer Pico SVOX
###################################################################################
def creer_dialog_config( args=None ):
	"""Ouvrir une fenêtre de dialogue pour modifier le champ Title d'une ou de plusieurs formes."""
	__creer_dialog_config__(XSCRIPTCONTEXT.getDocument(), XSCRIPTCONTEXT.getComponentContext())

def __creer_dialog_config__(xDocument, xContext):
	"""Ouvrir une fenêtre de dialogue pour configurer Pico SVOX."""	
	
	# i18n
	i18n()

	smgr = xContext.ServiceManager
	
	# lecture de la langue et de la vitesse de lecture
	langue = handleMaskLanguage()
	vitesse = handleMaskSpeed()

	# create the dialog model and set the properties 
	dialogModel = smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialogModel", xContext)

	dialogModel.PositionX = 100
	dialogModel.PositionY = 50
	dialogModel.Width = 190 
	dialogModel.Height = 130
	dialogModel.Title = 'Configuration'

	labelLge = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
	labelLge.PositionX = 10
	labelLge.PositionY = 2
	labelLge.Width  = 120
	labelLge.Height = 12
	labelLge.Name = "labelLge"
	labelLge.TabIndex = 1
	labelLge.Label = _('Langue de lecture :')
	
	rb1 = createRadioButton(dialogModel, 10, labelLge.PositionY+8, 'rb1', 5, 'fr-FR', (langue == 'fr-FR'), w=44)
	rb2 = createRadioButton(dialogModel, (dialogModel.Width-rb1.Width)/2, rb1.PositionY, 'rb2', 5, 'en-GB', (langue == 'en-GB'), w=rb1.Width)
	rb3 = createRadioButton(dialogModel, dialogModel.Width-10-rb1.Width, rb1.PositionY, 'rb3', 5, 'en-US', (langue == 'en-US'), w=rb1.Width)
	rb4 = createRadioButton(dialogModel, rb1.PositionX, rb1.PositionY+10, 'rb4', 5, 'de-DE', (langue == 'de-DE'), w=rb1.Width)
	rb5 = createRadioButton(dialogModel, rb2.PositionX, rb4.PositionY, 'rb5', 5, 'es-ES', (langue == 'es-ES'), w=rb1.Width)
	rb6 = createRadioButton(dialogModel, rb3.PositionX, rb4.PositionY, 'rb6', 5, 'it-IT', (langue == 'it-IT'), w=rb1.Width)

	sep1 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedLineModel")
	sep1.PositionX = 10
	sep1.PositionY = rb6.PositionY+12
	sep1.Width  = dialogModel.Width - 20
	sep1.Height  = 5
	sep1.Name = "sep1"
	sep1.TabIndex = 1

	labelSpd = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
	labelSpd.PositionX = 10
	labelSpd.PositionY = sep1.PositionY+5
	labelSpd.Width  = 50
	labelSpd.Height = 12
	labelSpd.Name = "labelSpd"
	labelSpd.TabIndex = 2
	labelSpd.Label = _('Vitesse :')

	speedNF = dialogModel.createInstance("com.sun.star.awt.UnoControlNumericFieldModel")
	speedNF.PositionX = labelSpd.PositionX+labelSpd.Width
	speedNF.PositionY = labelSpd.PositionY
	speedNF.Width  = labelSpd.PositionY
	speedNF.Height = labelSpd.Height-2
	speedNF.Name = 'speedNF'
	speedNF.TabIndex = 10
	speedNF.Value = vitesse
	speedNF.ValueMin = 0
	speedNF.ValueMax = 200
	speedNF.ValueStep = 10
	speedNF.Spin = True
	speedNF.DecimalAccuracy = 0

	sep2 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedLineModel")
	sep2.PositionX = 10
	sep2.PositionY = labelSpd.PositionY+10
	sep2.Width  = dialogModel.Width - 20
	sep2.Height  = 5
	sep2.Name = "sep2"
	sep2.TabIndex = 1

	checkMark = dialogModel.createInstance("com.sun.star.awt.UnoControlCheckBoxModel")
	checkMark.PositionX = 10
	checkMark.PositionY = sep2.PositionY+5
	checkMark.Width  = dialogModel.Width-20
	checkMark.Height = 12
	checkMark.Name = 'checkMark'
	checkMark.TabIndex = 12
	checkMark.State = (handleMaskMarker() == 1)
	checkMark.Label = _('Surligner le texte lu')

	checkRac = dialogModel.createInstance("com.sun.star.awt.UnoControlCheckBoxModel")
	checkRac.PositionX = 10
	checkRac.PositionY = checkMark.PositionY+10
	checkRac.Width  = dialogModel.Width-20
	checkRac.Height = 12
	checkRac.Name = 'checkRac'
	checkRac.TabIndex = 12
	checkRac.State = handleMaskShortcuts()
	checkRac.Label = _('Activer les raccourcis clavier')

	labelRC1 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
	labelRC1.PositionX = checkRac.PositionX+5
	labelRC1.PositionY = checkRac.PositionY+12
	labelRC1.Width  = (dialogModel.Width-2*labelRC1.PositionX)/3
	labelRC1.Height = 12
	labelRC1.Name = "labelRC1"
	labelRC1.TabIndex = 1
	labelRC1.Label = 'Alt+w = '+_('mot')

	labelRC2 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
	labelRC2.PositionX = labelRC1.PositionX+labelRC1.Width
	labelRC2.PositionY = labelRC1.PositionY
	labelRC2.Width  = labelRC1.Width
	labelRC2.Height = labelRC1.Height
	labelRC2.Name = "labelRC2"
	labelRC2.TabIndex = 1
	labelRC2.Label = 'Alt+k = '+_('phrase')

	labelRC3 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
	labelRC3.PositionX = labelRC2.PositionX+labelRC2.Width
	labelRC3.PositionY = labelRC1.PositionY
	labelRC3.Width  = labelRC1.Width
	labelRC3.Height = labelRC1.Height
	labelRC3.Name = "labelRC3"
	labelRC3.TabIndex = 1
	labelRC3.Label = 'Alt+p = '+_('paragraphe')

	labelRC4 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
	labelRC4.PositionX = labelRC1.PositionX
	labelRC4.PositionY = labelRC1.PositionY+10
	labelRC4.Width  = labelRC1.Width
	labelRC4.Height = labelRC1.Height
	labelRC4.Name = "labelRC4"
	labelRC4.TabIndex = 1
	labelRC4.Label = 'Alt+-> = '+_('suivant')

	labelRC5 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
	labelRC5.PositionX = labelRC4.PositionX+labelRC4.Width
	labelRC5.PositionY = labelRC4.PositionY
	labelRC5.Width  = labelRC4.Width
	labelRC5.Height = labelRC4.Height
	labelRC5.Name = "labelRC5"
	labelRC5.TabIndex = 1
	labelRC5.Label = 'Alt+<- = '+_('retour')

	labelRC6 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
	labelRC6.PositionX = labelRC5.PositionX+labelRC5.Width
	labelRC6.PositionY = labelRC4.PositionY
	labelRC6.Width  = labelRC4.Width
	labelRC6.Height = labelRC4.Height
	labelRC6.Name = "labelRC6"
	labelRC6.TabIndex = 1
	labelRC6.Label = 'Alt+R = '+__u__(_('repeter'))

	checkFrap = dialogModel.createInstance("com.sun.star.awt.UnoControlCheckBoxModel")
	checkFrap.PositionX = checkRac.PositionX
	checkFrap.PositionY = labelRC5.PositionY+10
	checkFrap.Width  = dialogModel.Width-20
	checkFrap.Height = 12
	checkFrap.Name = 'checkFrap'
	checkFrap.TabIndex = 12
	checkFrap.State = handleMaskTyping()
	checkFrap.Label = _('Activer la lecture en cours de frappe')

	# create the button model and set the properties 
	buttonModel = dialogModel.createInstance("com.sun.star.awt.UnoControlButtonModel")
	buttonModel.Width = 50
	buttonModel.Height = 14
	buttonModel.PositionX = dialogModel.Width/2-buttonModel.Width-2
	buttonModel.PositionY  = dialogModel.Height-16
	buttonModel.Name = "myButtonName"
	buttonModel.TabIndex = 0
	buttonModel.Label = _("Valider")
	
	cancelButtonModel = dialogModel.createInstance("com.sun.star.awt.UnoControlButtonModel")
	cancelButtonModel.Width = 50
	cancelButtonModel.Height = 14
	cancelButtonModel.PositionX = dialogModel.Width/2+2
	cancelButtonModel.PositionY  = dialogModel.Height-16
	cancelButtonModel.Name = "cancelButton"
	cancelButtonModel.TabIndex = 0
	cancelButtonModel.Label = _("Annuler")

	# insert the control models into the dialog model 
	dialogModel.insertByName(buttonModel.Name, buttonModel)
	dialogModel.insertByName(cancelButtonModel.Name, cancelButtonModel)
	dialogModel.insertByName(labelLge.Name, labelLge)
	dialogModel.insertByName(labelSpd.Name, labelSpd)
	dialogModel.insertByName(speedNF.Name, speedNF)
	dialogModel.insertByName(rb1.Name, rb1)
	dialogModel.insertByName(rb2.Name, rb2)
	dialogModel.insertByName(rb3.Name, rb3)
	dialogModel.insertByName(rb4.Name, rb4)
	dialogModel.insertByName(rb5.Name, rb5)
	dialogModel.insertByName(rb6.Name, rb6)
	dialogModel.insertByName(sep1.Name, sep1)
	dialogModel.insertByName(sep2.Name, sep2)
	dialogModel.insertByName(checkMark.Name, checkMark)
	dialogModel.insertByName(checkRac.Name, checkRac)
	dialogModel.insertByName(checkFrap.Name, checkFrap)
	dialogModel.insertByName(labelRC1.Name, labelRC1)
	dialogModel.insertByName(labelRC2.Name, labelRC2)
	dialogModel.insertByName(labelRC3.Name, labelRC3)
	dialogModel.insertByName(labelRC4.Name, labelRC4)
	dialogModel.insertByName(labelRC5.Name, labelRC5)
	dialogModel.insertByName(labelRC6.Name, labelRC6)

	# create the dialog control and set the model 
	controlContainer = smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialog", xContext)
	controlContainer.setModel(dialogModel)
	
	# add the action listener
	controlContainer.getControl("myButtonName").addActionListener(ConfigActionListener(xDocument,
													controlContainer,
													controlContainer.getControl(checkMark.Name),
													controlContainer.getControl(checkRac.Name),
													controlContainer.getControl(checkFrap.Name),
													controlContainer.getControl(speedNF.Name),
													controlContainer.getControl(rb1.Name),
													controlContainer.getControl(rb2.Name),
													controlContainer.getControl(rb3.Name),
													controlContainer.getControl(rb4.Name),
													controlContainer.getControl(rb5.Name),
													controlContainer.getControl(rb6.Name)))
	controlContainer.getControl("cancelButton").addActionListener(CancelActionListener(controlContainer))

	# create a peer 
	toolkit = smgr.createInstanceWithContext("com.sun.star.awt.ExtToolkit", xContext)

	controlContainer.setVisible(False);	   
	controlContainer.createPeer(toolkit, None);

	# execute it
	controlContainer.execute()

	# dispose the dialog 
	controlContainer.dispose()

###################################################################################
# Déclaration uniquement si le module uno a été chargé
###################################################################################
if 'uno' in sys.modules.keys():
	"""
		
	"""
	class PicoSvoxOOoShortcutsHandler(unohelper.Base, XKeyHandler):
		enabled = True

		def __init__(self, xDocument):
			self.xDocument = xDocument
			self.lit = Lire(self.xDocument)
			self.is_text_doc = self.xDocument.supportsService("com.sun.star.text.TextDocument")

		def keyPressed(self, event):
			if not PicoSvoxOOoShortcutsHandler.enabled:
				return False
			try:
				if self.is_text_doc:
					if event.Modifiers == MOD2:
						# touche ALT enfoncée
						if event.KeyCode == keyK:
							# ALT + K
							self.lit.phrase()
							return True
						elif event.KeyCode == keyP:
							# ALT + P
							self.lit.paragraphe()
							return True
						elif event.KeyCode == keyW:
							# ALT + W
							self.lit.selection()
							return True
						elif event.KeyCode == keyLeft:
							# ALT + <-
							__deplacement__(self.xDocument, __lecturePrecedent__)
							return True
						elif event.KeyCode == keyRight:
							# ALT + ->
							__deplacement__(self.xDocument, __lectureSuivant__)
							return True
						elif event.KeyCode == keyS:
							# ALT + S
							__stop_lecture__(self.xDocument)
							return True
						elif event.KeyCode == keyR:
							# ALT + R
							__repeter__()
							return True
				else:
					if event.Modifiers == MOD2:
						if event.KeyCode == keyS:
							# ALT + S
							__stop_lecture__(self.xDocument)
							return True
						elif event.KeyCode == keyK:
							# ALT + K
							self.lit.selection()
							return True
			except:
				pass
			return False

		def keyReleased(self, event):
			return False
			
		def disposing(self, event):
			pass
			
		def enable(self, val=True):
			PicoSvoxOOoShortcutsHandler.enabled = val

	"""
		
	"""
	class PicoSvoxOOoSpaceKeyHandler(unohelper.Base, XKeyHandler):
		enabled = True

		def __init__(self, xDocument):
			self.xDocument = xDocument
			self.lit = Lire(self.xDocument)
			self.mot = False
			self.is_text_doc = self.xDocument.supportsService("com.sun.star.text.TextDocument")

		def keyPressed(self, event):
			global __codesLectureFrappe__

			if not (PicoSvoxOOoSpaceKeyHandler.enabled and self.is_text_doc):
				return False
			try:
				if event.KeyCode in __codesLectureFrappe__:
					if self.mot:
						self.mot = False
						self.lit.frappe(event.KeyChar)
				else:
					self.mot = True
			except:
				pass
			return False

		def keyReleased(self, event):
			return False
			
		def disposing(self, event):
			pass
			
		def enable(self, val=True):
			PicoSvoxOOoSpaceKeyHandler.enabled = val

	######################################################################################
	# Gestionnaire d'événement de la boite de dialogue
	######################################################################################
	class ConfigActionListener(unohelper.Base, XActionListener):
		xDocument = None
		
		def __init__(self, xDocument, controlContainer, checkMark, checkRac, checkFrap, speedNF, rb1, rb2, rb3, rb4, rb5, rb6):
			self.controlContainer = controlContainer
			self.checkMark = checkMark
			self.checkRac = checkRac
			self.checkFrap = checkFrap
			self.speedNF = speedNF
			self.rb1 = rb1
			self.rb2 = rb2
			self.rb3 = rb3
			self.rb4 = rb4
			self.rb5 = rb5
			self.rb6 = rb6
			self.xDocument = xDocument

		def actionPerformed(self, actionEvent):
			global memoShortcuts
			global memoTyping
			
			# lecture de la langue et de la vitesse de lecture
			langue = handleMaskLanguage()
			vitesse = handleMaskSpeed()
			
			# (dés)activer pour tous les documents ouverts
			for key in memoShortcuts.keys():
				memoShortcuts[key]['handler'].enable(self.checkRac.getState() == 1)
			saveMaskShortcuts(self.checkRac.getState())

			# (dés)activer pour tous les documents ouverts
			for key in memoTyping.keys():
				memoTyping[key]['handler'].enable(self.checkFrap.getState() == 1)
			saveMaskTyping(self.checkFrap.getState())

			vitesse = self.speedNF.getValue()
			if self.rb1.getState():
				langue = 'fr-FR'
			if self.rb2.getState():
				langue = 'en-GB'
			if self.rb3.getState():
				langue = 'en-US'
			if self.rb4.getState():
				langue = 'de-DE'
			if self.rb5.getState():
				langue = 'es-ES'
			if self.rb6.getState():
				langue = 'it-IT'

			if self.checkMark.getState():
				saveMaskMarker(1)
			else:
				saveMaskMarker(0)
			saveMaskLanguage(langue)
			saveMaskSpeed(int(vitesse))

			self.controlContainer.endExecute()

	class CancelActionListener(unohelper.Base, XActionListener):
		def __init__(self, controlContainer):
			self.controlContainer = controlContainer

		def actionPerformed(self, actionEvent):
			self.controlContainer.endExecute()

	###################################################################################
	# lists the scripts, that shall be visible inside OOo. Can be omitted.
	###################################################################################
	g_exportedScripts = lire_phrase, lire_paragraphe, lire_selection, stop_lecture, \
	passage_suivant, passage_precedent, creer_dialog_config,

	class OnLoadEvent(unohelper.Base, XJob):
		"""Implantation de l'action sur nouveau document
		https://wiki.openoffice.org/wiki/Documentation/DevGuide/WritingUNO/Jobs/Configuration
		http://www.0d.be/docs/ecrire-une-extension-openoffice-avec-python/evenements/
		http://www.unixgarden.com/index.php/linux-pratique-hs/openofficeorg-a-vos-ordres
		"""
		def __init__(self, ctx):
			self.ctx = ctx
		def execute(self, args):
			global memoShortcuts
			global memoTyping
			
			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			xDocument = desktop.getCurrentComponent()
			for arg in args:
				if arg.Name == 'Environment':
					for val in arg.Value:
						if val.Name == 'Model':
							xDocument = val.Value
			if xDocument is None:
				return

			key = xDocument.RuntimeUID
			memoShortcuts[key] = {'doc':xDocument, 'handler':PicoSvoxOOoShortcutsHandler(xDocument)}
			memoTyping[key] = {'doc':xDocument, 'handler':PicoSvoxOOoSpaceKeyHandler(xDocument)}

			# enable/disable the key handlers
			memoShortcuts[key]['handler'].enable(handleMaskShortcuts() == 1)
			memoTyping[key]['handler'].enable(handleMaskTyping() == 1)

			# register the key handlers
			xDocument.getCurrentController().addKeyHandler(memoShortcuts[key]['handler'])
			xDocument.getCurrentController().addKeyHandler(memoTyping[key]['handler'])

	class OnCloseEvent(unohelper.Base, XJob):
		"""Implantation de l'action sur fermeture de document
		https://wiki.openoffice.org/wiki/Documentation/DevGuide/WritingUNO/Jobs/Configuration
		http://www.0d.be/docs/ecrire-une-extension-openoffice-avec-python/evenements/
		http://www.unixgarden.com/index.php/linux-pratique-hs/openofficeorg-a-vos-ordres
		"""
		def __init__(self, ctx):
			self.ctx = ctx
		def execute(self, args):
			global memoShortcuts
			global memoTyping

			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			xDocument = desktop.getCurrentComponent()
			key = xDocument.RuntimeUID

			if handleMaskShortcuts():
				try:
					xDocument.getCurrentController().removeKeyHandler(memoShortcuts[key]['handler'])
					del memoShortcuts[key]['handler']
					memoShortcuts[key]['handler'] = None
					memoShortcuts[key]['doc'] = None
				except:
					pass

			if handleMaskTyping():
				try:
					xDocument.getCurrentController().removeKeyHandler(memoTyping[key]['handler'])
					del memoTyping[key]['handler']
					memoTyping[key]['handler'] = None
					memoTyping[key]['doc'] = None
				except:
					pass

	class LirePhrase(unohelper.Base, XJobExecutor):
		"""Lit la phrase courante sous le curseur"""
		def __init__(self, ctx):
			self.ctx = ctx
		def trigger(self, args):
			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			lit = Lire(desktop.getCurrentComponent())
			lit.phrase()

	class LireParagraphe(unohelper.Base, XJobExecutor):
		"""Lit le paragraphe courant sous le curseur"""
		def __init__(self, ctx):
			self.ctx = ctx
		def trigger(self, args):
			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			lit = Lire(desktop.getCurrentComponent())
			lit.paragraphe()

	class LireSelection(unohelper.Base, XJobExecutor):
		"""Lit le texte sélectionné"""
		def __init__(self, ctx):
			self.ctx = ctx
		def trigger(self, args):
			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			lit = Lire(desktop.getCurrentComponent())
			lit.selection()

	class StopLecture(unohelper.Base, XJobExecutor):
		"""Arrête la lecture"""
		def __init__(self, ctx):
			self.ctx = ctx
		def trigger(self, args):
			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			__stop_lecture__(desktop.getCurrentComponent())

	class PassageSuivant(unohelper.Base, XJobExecutor):
		"""En fonction du mode de lecture passe au passage suivant"""
		def __init__(self, ctx):
			self.ctx = ctx
		def trigger(self, args):
			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			__deplacement__(desktop.getCurrentComponent(), __lectureSuivant__)

	class PassagePrecedent(unohelper.Base, XJobExecutor):
		"""En fonction du mode de lecture passe au passage précédent"""
		def __init__(self, ctx):
			self.ctx = ctx
		def trigger(self, args):
			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			__deplacement__(desktop.getCurrentComponent(), __lecturePrecedent__)

	class CreerDialogConfig(unohelper.Base, XJobExecutor):
		"""Ouvrir une fenêtre de dialogue pour configurer Pico SVOX."""
		def __init__(self, ctx):
			self.ctx = ctx
		def trigger(self, args):
			desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
			__creer_dialog_config__(desktop.getCurrentComponent(), self.ctx)

	# --- faked component, dummy to allow registration with unopkg, no functionality expected
	g_ImplementationHelper = unohelper.ImplementationHelper()

	g_ImplementationHelper.addImplementation( \
		LirePhrase,'org.picosvoxooo.LirePhrase', \
		('com.sun.star.task.Job',))

	g_ImplementationHelper.addImplementation( \
		LireParagraphe,'org.picosvoxooo.LireParagraphe', \
		('com.sun.star.task.Job',))

	g_ImplementationHelper.addImplementation( \
		LireSelection,'org.picosvoxooo.LireSelection', \
		('com.sun.star.task.Job',))

	g_ImplementationHelper.addImplementation( \
		StopLecture,'org.picosvoxooo.StopLecture', \
		('com.sun.star.task.Job',))

	g_ImplementationHelper.addImplementation( \
		PassageSuivant,'org.picosvoxooo.PassageSuivant', \
		('com.sun.star.task.Job',))

	g_ImplementationHelper.addImplementation( \
		PassagePrecedent,'org.picosvoxooo.PassagePrecedent', \
		('com.sun.star.task.Job',))

	g_ImplementationHelper.addImplementation( \
		CreerDialogConfig,'org.picosvoxooo.CreerDialogConfig', \
		('com.sun.star.task.Job',))

	g_ImplementationHelper.addImplementation( \
		OnLoadEvent,'org.picosvoxooo.OnLoadEvent', \
		('com.sun.star.task.Job',))

	g_ImplementationHelper.addImplementation( \
		OnCloseEvent,'org.picosvoxooo.OnCloseEvent', \
		('com.sun.star.task.Job',))

if __name__ == "__main__":
	import time

	data1 = 'Un. Il était une fois.\
	Deux. Il était deux fois.\
	Trois. Il était trois fois.\
	Quatre. Il était quatre fois.\
	Cinq. Il était cinq fois.\
	Six. Il était six fois.\
	Sept. Il était sept fois.\
	Huit. Il était huit fois.\
	Neuf. Il était neuf fois.\
	Dix. Il était dix fois.\
	'
	__lire_texte__(data1, xTextViewCursor=None, surligner=False, wfile="temp.wav", spd=100)
	time.sleep(6.0)
	__stop_lecture__()
