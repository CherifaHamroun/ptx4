var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.multiclic = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
var aNombres, conteneur, etiquetteCalcul, carreClique, soluce, carreSoluceintId, repEleve;
var aPlageA = []; var aPlageB = []; var aCouple = []; 
//conteneur = disp.createEmptySprite();


// Référencer les ressources de l'exercice (image, son)

exo.oRessources = {
	illustration : "multiclic/images/illustration.png"
};

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
		totalQuestion:10,
		totalEssai:1,
		temps_question:0,
		temps_correction:20,
		temps_exo:0,
		plageA:"2-9",
		plageB:"2-10"
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
	aPlageA = util.getArrayNombre(exo.options.plageA);
	aPlageB = util.getArrayNombre(exo.options.plageB);
	var lengthA = aPlageA.length;
	var lengthB = aPlageB.length;
	// on genere tous les couples possibles 
	for( var i = 0 ; i < lengthA ; i++ ) {
		var nA = aPlageA[i];
		for ( var j = 0 ; j < lengthB ; j++ ) {
			var nB = aPlageB[j];
			if (aCouple.indexOf([nA,nB])) aCouple.push([nA,nB]);
		}
	}
	util.shuffleArray(aCouple);
	//aCouple.sort(function(){return Math.floor(Math.random()*3)-1});
	// on evite de manipuler un tableau trop long (100 couples maxi)
	if( aCouple.length > 100 ) {
		aCouple = aCouple.slice(0,exo.options.totalQuestion);
	}
	// si les parametres ne génèrent pas assez de couples on répète
	else if ( aCouple.length < exo.options.totalQuestion ) {
		var n = 0;
		while ( (exo.options.totalQuestion - aCouple.length)>0 || n < 100) {
		n++;
		aCouple = aCouple.concat(aCouple.slice(0,exo.options.totalQuestion - aCouple.length));
		}
	}
};

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Multiclic");
    exo.blocConsigneGenerale.html("Cliquer sur la case qui correspond au nombre demandé.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
	var q = exo.indiceQuestion;
	var cnt = disp.createSvgJsContainer(430,430).css({left:300,top:20});
	conteneur = cnt.paper.group();
	for ( var i = 0 ; i < 100 ; i++) {
		var posX = (i % 10) * 43;
		var posY = Math.floor(i / 10) * 43;
		var carre  = cnt.paper.group().attr("cursor","pointer");
		carre.move(posX,posY);
		carre.data("valeur",(i+1));
		var fond = cnt.paper.rect(36,36).fill("#48BCFF").stroke("none");
		carre.add(fond);
		var label = cnt.paper.plain(""+(i+1));
		label.attr({"font-size":20,fill:"#fff","text-anchor":"middle"});
		label.center(18,24);
		carre.add(label);
		
		conteneur.add(carre);
	}
	//
	exo.keyboard.config({
		numeric : "disabled",
		arrow : "disabled",
		large : "disabled"
    });
	//
	
	repEleve = 0;
	exo.btnValider.hide();
	exo.blocInfo.css({
		left:100,
		top:300
	});
	//
	exo.blocAnimation.append(cnt);
	//
	var texte = "";
	var fontSize,largeur,gauche;
	if (exo.indiceQuestion % 3 === 0) {
		texte =aCouple[q][0]+" x "+aCouple[q][1]+" = ? ";
		soluce = aCouple[q][0]*aCouple[q][1];
		fontSize = 32;
		largeur=150;
		gauche = 80;
	} else if (exo.indiceQuestion % 3 == 1) {
		texte =aCouple[q][1]+" x "+aCouple[q][0]+" = ? ";
		soluce = aCouple[q][0]*aCouple[q][1];
		fontSize = 32;
		largeur=150;
		gauche = 80;
	} else {
		//texte =aCouple[q][1]+" x "+aCouple[q][0]+" = ? ";
		fontSize = 24;
		largeur=200;
		gauche = 50;
		texte = "Combien de fois "+aCouple[q][0]+" dans "+aCouple[q][1]*aCouple[q][0]+" ?";
		soluce = aCouple[q][1];
	}
	//
	conteneur.each(function(){
		if (exo.tabletSupport) {
			this.on("touchstart",gestionTouchCarre);
		} else{
			this.on("mousedown",gestionTouchCarre);
			this.on("mouseover",gestionMouseOver);
			this.on("mouseout",gestionMouseOut);
		}
		
		var fond = this.children()[0];
		fond.stroke('none');
		if (this.data("valeur") == soluce) {
			carreSoluce=this;
		}
		

	});
	
	
	etiquetteCalcul = disp.createTextLabel(texte);
	etiquetteCalcul.css({
		fontSize:fontSize,
		fontWeight:'bold',
		border:"1px solid #ccc",
		opacity:1,
		padding:20,
		width:largeur,
		textAlign:"center"
	});
	exo.blocAnimation.append(etiquetteCalcul);
	
	
	etiquetteCalcul.animate({opacity:1},1000,'linear');

	etiquetteCalcul.css({
		left:gauche,
		top:180
	});

    //
    
	function gestionTouchCarre(e) {
		conteneur.each(function(){
			if (exo.tabletSupport) {
				this.off("touchstart",gestionTouchCarre);
			} else {
				this.off("mousedown",gestionTouchCarre);
				this.off("mouseover",gestionMouseOver);
				this.off("mouseout",gestionMouseOut);
			}
		});
		e.stopPropagation();
		e.preventDefault();
		
		var cible = e.currentTarget.instance;
		repEleve = cible.data("valeur");
		var fond = cible.children()[0];
		fond.stroke({width:5,color:"#FFCA00"});
		exo.poursuivreExercice(500,exo.options.temps_correction*100);
		return false;
	}
	
	function gestionMouseOver(e) {
		//console.log("enter");
		var fond = e.currentTarget.instance.children()[0];
		fond.stroke({width:5,color:"#FFCA00"});
	}
	
	function gestionMouseOut(e) {
		var fond = e.currentTarget.instance.children()[0];
		fond.stroke('none');
	}
};
// Evaluation doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    if(repEleve == soluce) {
        return "juste";
    } else {
        return "faux";
    }
};
// Correction (peut rester vide)
exo.corriger = function() {
    //conteneur.append(carreSoluce);
    //anim.glow(carreSoluce,"red",5,5);
	carreSoluce.children()[0].stroke({width:5,color:"#ff0000"});
};
// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_question",
        texte:"Temps pour réaliser une question (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
	type:"text",
        largeur:46,
        taille:3,
        nom:"plageA",
        texte:"Nombre A :"
    });
    exo.blocParametre.append(controle);
	//
	controle = disp.createOptControl(exo,{
	type:"text",
        largeur:46,
        taille:3,
        nom:"plageB",
        texte:"Nombre B :"
    });
    exo.blocParametre.append(controle);
	//
    controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_correction",
        texte:"Temps d'exposition de la correction (en dixième de secondes) : "
    });
    exo.blocParametre.append(controle);
};


/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));