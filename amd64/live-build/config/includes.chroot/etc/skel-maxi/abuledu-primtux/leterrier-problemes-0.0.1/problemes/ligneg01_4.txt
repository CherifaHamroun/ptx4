set etapes 1
set niveaux {0 1 2 3 5}
::1
set niveau 2
set ope {{5 10} {1 5}}
set interope {{1 15 1} {1 6 1}}
set scaleb {0 50 1}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set operations [list [list [expr $ope1 ]+[expr $ope2]] [list [expr $ope2]+[expr $ope1]]]
set editenon "(Le nombre du d� est $ope2.)"
set enonce "Le jeu de l'oie.\nPim est sur la case $ope1.\nIl lance son d�.\nSur quelle case se trouve-t-il maintenant?"
set source {{0}}
set reponse [list [list {1} [list {Il est sur la case} [expr $ope2 + $ope1]]]]
set resultdessin [expr $ope2 + $ope1]
set dpt $ope1
set opnonautorise {}
set canvash 140
set c1height 100
set orgy 50
set orgsourcexorig 50
canvas .frametop.c1 -width 200 -bg grey -insertbackground grey -highlightbackground grey -height $c1height
pack .frametop.c1 -expand true -side left
.frametop.c1 create image $orgsourcexorig $orgy -image [image create photo -file [file join sysdata de[expr $ope2].gif]]
set placearriv none
::
