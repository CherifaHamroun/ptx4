#!/bin/sh
#compagnon.tcl
#\
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2006 David Lucardi <davidlucardi@aol.com>
#   
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : compagnon.tcl
#  Author  : David Lucardi <davidlucardi@aol.com>
#  Modifier:
#  Date    : 30/12/2006
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: compagnon.tcl,v 1.2 2007/01/07 06:53:11 david Exp $
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi
# 
#***********************************************************************

proc tux_presente {} {.wtux.speech configure -wraplength 300
wm protocol .wtux WM_DELETE_WINDOW ""
.wtux.speech configure -text "[mc {Premiere utilisation?}] \n \n [mc {1 - Choisis d'abord un dossier dans le menu dossier de textes.}] \n \n [mc {2 - Choisis ensuite un texte dans le menu Textes.}] \n \n [mc {3 - Clique sur une des activites proposees pour ce texte.}] \n \n [mc {4 - Tu peux enfin imprimer ton bilan dans le menu Fichier.}]"
.wtux.tux configure -image tux
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_commence {} {
global user
#wm transient .wtux .
.wtux.speech configure -text [format [mc {Bonjour %1$s, pour commencer, observe bien le texte puis clique sur le bouton ->}] [string map {.log \040} [file tail $user]]]
.wtux.tux configure -image tux_bonjour
bind .wtux.speech <1> "speaktexte [list [format [mc {Bonjour %1$s, pour commencer, observe bien le texte puis clique sur le bouton fl�che}] [string map {.log \040} [file tail $user]]]]"
}

proc tux_exo {what} {
global startdirect
#wm transient .wtux .
.wtux.speech configure -text [mc $what]
if {$startdirect == 1} {.wtux.tux configure -image tux} else {.wtux.tux configure -image tux_bonjour}
bind .wtux.speech <1> "speaktexte [list $what]"
}

proc tux_reussi {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {Bien joue!}]
.wtux.tux configure -image tux_bien
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_continuebon {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {C'est bien, maintenant ecris le mot correct.}]
.wtux.tux configure -image tux
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_continue_bien {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {C'est juste!}]
.wtux.tux configure -image tux
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_continuebonponctuation {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {C'est bien, maintenant ecris le signe correct.}]
.wtux.tux configure -image tux
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_phrasesuivante {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {Passons a la phrase suivante.}]
.wtux.tux configure -image tux
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}


proc tux_continue {} {
#wm transient .wtux .
.wtux.speech configure -text ""
.wtux.tux configure -image tux
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_continue_ponctuation {} {
#wm transient .wtux .
.wtux.speech configure -text "Appuie sur entree pour valider."
.wtux.tux configure -image tux
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_echoue1 {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {C'est faux, recommence.}]
.wtux.tux configure -image tux_echoue
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_echoue2 {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {C'est toujours faux!}]
.wtux.tux configure -image tux_rechoue
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_triste {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {C'est pas terrible! Ton score est de %1$s sur 100. Clique sur le bouton X pour terminer.}] $score]
.wtux.tux configure -image tux_rechoue
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_moyen {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {C'est pas mal! Ton score est de %1$s sur 100. Clique sur le bouton X pour terminer.}] $score]
.wtux.tux configure -image tux_pasmal
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_content {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {C'est bien! Ton score est de %1$s sur 100. Clique sur le bouton X pour terminer.}] $score]
.wtux.tux configure -image tux_bien
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_triste_final {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {L'exercice est termine. C'est pas terrible! Ton score est de %1$s sur 100. Clique sur le bouton X pour terminer.}] $score]
.wtux.tux configure -image tux_rechoue
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_moyen_final {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {L'exercice est termine. C'est pas mal! Ton score est de %1$s sur 100. Clique sur le bouton X pour terminer.}] $score]
.wtux.tux configure -image tux_pasmal
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_content_final {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {L'exercice est termine. C'est bien! Ton score est de %1$s sur 100. Clique sur le bouton X pour terminer.}] $score]
.wtux.tux configure -image tux_bien
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_triste_phrase {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {C'est pas terrible! Ton score est de %1$s sur 100 pour cette phrase.}] $score]
.wtux.tux configure -image tux_rechoue
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_moyen_phrase {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {C'est pas mal! Ton score est de %1$s sur 100 pour cette phrase.}] $score]
.wtux.tux configure -image tux_pasmal
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_content_phrase {score} {
#wm transient .wtux .
.wtux.speech configure -text [format [mc {C'est bien! Ton score est de %1$s sur 100 pour cette phrase.}] $score]
.wtux.tux configure -image tux_bien
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_triste_phrase2 {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {C'est encore faux! Recommence!}]
.wtux.tux configure -image tux_rechoue
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_moyen_phrase2 {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {C'est faux, recommence.}]
.wtux.tux configure -image tux_echoue
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

proc tux_content_phrase2 {} {
#wm transient .wtux .
.wtux.speech configure -text [mc {C'est bien!}]
.wtux.tux configure -image tux_bien
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"
}

catch {destroy .wtux}
toplevel .wtux
wm geometry .wtux +55+340

set compagnon [expr int(rand()*2) +1]
switch $compagnon {
1 {
image create photo tux -file [file join sysdata tux.gif] 
image create photo tux_bonjour -file [file join sysdata tux.gif] 
image create photo tux_bien -file [file join sysdata tux_bien.gif] 
image create photo tux_echoue -file [file join sysdata tux_echoue1.gif] 
image create photo tux_rechoue -file [file join sysdata tux_echoue2.gif] 
image create photo tux_pasmal -file [file join sysdata tux_pasmal.gif] 
wm title .wtux Tux
}

2 {
image create photo tux -file [file join sysdata bob.gif] 
image create photo tux_bonjour -file [file join sysdata bob_bonjour.gif] 
image create photo tux_bien -file [file join sysdata bob_bien.gif] 
image create photo tux_echoue -file [file join sysdata bob_echoue1.gif] 
image create photo tux_rechoue -file [file join sysdata bob_echoue2.gif] 
image create photo tux_pasmal -file [file join sysdata bob_pasmal.gif] 
wm title .wtux Bob
}
}
label .wtux.speech -wraplength 120
pack .wtux.speech -side top -fill both -expand 1
label .wtux.tux -image tux
pack .wtux.tux -side top -fill both -expand 1
bind .wtux.speech <Any-Enter> ".wtux.speech configure -cursor target"
bind .wtux.speech <Any-Leave> ".wtux.speech configure -cursor left_ptr"
wm protocol .wtux WM_DELETE_WINDOW "return"
update

wm transient .wtux .
