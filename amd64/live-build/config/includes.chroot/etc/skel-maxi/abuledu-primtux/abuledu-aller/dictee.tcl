#!/bin/sh
#closure.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************

global sysFont nbreu essais auto couleur aide longchamp xcol ycol listevariable categorie startdirect user Homeconf repertoire listexo tabaide tablistevariable tablongchamp tabstartdirect Home initrep baseHome aller_reconstitution lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache validation
source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}


#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

set nbreu 0
set essais 0
set auto 0
set couleur red
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1
set validation 0
#######################################################################################"
#set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


frame .menu -height 40
pack .menu -side bottom -fill both
button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right
#frame .barre -width 40
#pack .barre -side left -fill both

tux_commence

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

#recup�ration des options de reglages
catch {

set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_dictee [lindex $aller 12]
close $f
set aide [lindex $aller_dictee 0]
set longchamp [lindex $aller_dictee 1]
set listevariable [lindex $aller_dictee 2]
#set categorie [lindex $aller_dictee 3]
set startdirect [lindex $aller_dictee 4]
set lecture_mot [lindex $aller_dictee 5]
set lecture_mot_cache [lindex $aller_dictee 6]
}
set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]

bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"

bind . <KeyPress> "cancelkey %A"

if {$auto == 1} {
set aide $tabaide(dictee)
set longchamp $tablongchamp(dictee)
set listevariable $tablistevariable(dictee)
set startdirect $tabstartdirect(dictee)
set lecture_mot $tablecture_mot(dictee)
set lecture_mot_cache $tablecture_mot_cache(dictee)
}

#focus .text
#.text configure -state disabled

wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 13] 1]"
label .menu.titre -text "[lindex [lindex $listexo 1] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1

proc litphrases {} {
global plist2 numphrase
speaktexte [list [lindex $plist2 $numphrase]]
}

proc spkt {i} {
global listemotscaches plist numphrase
set numphrase 0
speaktexte [lindex $listemotscaches $i]
set compteur 0

	for {set j 0} {$j< [llength $plist]} {incr j} {
	set compt [llength [lindex $plist $j]]
	set compteur [expr $compt + $compteur]
		if {$i < $compteur} {
		#tk_messageBox -message "i $i compteur $compteur j $j"
		set numphrase $j
		break
		} 		
		
	}
}


proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot listexo iwish aide user startdirect textdict done numphrase
set nbmotscaches 0
set numphrase 0
set textdict ""
#catch {destroy .menu.b1}
#button .menu.b1 -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
#pack .menu.b1 -side right
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
button .menu.ok -image [image create photo imagbut -file [file join sysdata okk.gif] ] -command "verif $t"
pack .menu.ok -side left

catch {destroy .menu.titre}
label .menu.titre -text [lindex [lindex $listexo 1] 2] -justify center
pack .menu.titre -side left -fill both -expand 1

set what "Ecris puis appuie sur la touche TAB pour avancer.\nQuand tu as fini, relis puis appuie sur le bouton pouce pour v�rifier."

if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what
#bind .menu  <Destroy> "fin"
$t configure -state normal

# S�lection du mode auto ou manuel pour la g�n�ration de l'exercice
    if {$auto==0} {
    pauto $t
    } else {
    pmanuel $t
    }
set textdict $listemotscaches
	for {set i 0} {$i < [llength $listemotscaches]} {incr i} {
	set done($i) 0
	}

    $t configure -state disabled -selectbackground white -selectforeground black
bind .text <ButtonRelease-1> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""

wm geometry .wtux +200+340
button .menu.bbb1 -image [image create photo speak1 -file [file join sysdata speakphrase.gif]] -command "litphrases"
pack .menu.bbb1 -side right
.menu.bb1 configure -image [image create photo speak -file [file join sysdata speaktout.gif]]
}

proc pauto {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide iwish filuser lecture_mot lecture_mot_cache
set plist [parse2 $t]
#le compte total de mots
set compte [comptemots $plist]


#on transforme la liste principale ( de phrases en une liste de mots)
set tmp ""
    foreach phrase $plist {
    set tmp [concat $tmp $phrase]
    }
#on r�cup�re les mots � masquer
   foreach mot $tmp {
     #if {[string length $mot] > 3} {
     lappend listemotscaches $mot
     incr nbmotscaches
     #}
  }
   if {$nbmotscaches == 0} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }

#on recherche les mots cach�s dans le texte, on les supprime et on les remplace par une zone de saisie
    set re1 {\m}
    set re2 {\M} 
    set cur 0.0
    for {set i 0} {$i < $nbmotscaches} {incr i 1} {

    set cur [$t search -regexp $re1[lindex $listemotscaches $i]$re2 $cur end]

    $t delete $cur "$cur + [string length [lindex $listemotscaches $i]] char"

    entry $t.ent$i -font $sysFont(l) -width [expr [string length [lindex $listemotscaches $i]] +2] -bg yellow
    $t window create $cur -window $t.ent$i
    bind $t.ent$i <KeyPress> "tux_continue"

#   if {$lecture_mot_cache == "1" } {
bind $t.ent$i <1> "spkt $i"
bind $t.ent$i <FocusIn> "spkt $i"
bind $t.ent$i <Any-Enter> "$t.ent$i configure -cursor target"
bind $t.ent$i <Any-Leave> "$t.ent$i configure -cursor left_ptr"

#}
 #   if {$lecture_mot == "0" } {catch destroy .menu.bb1}

    set listessai($i) 0
    }
    set listeaide $listemotscaches

}

proc pmanuel {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches couleur aide longchamp longmot listeaide  iwish lecture_mot lecture_mot_cache
set nbmotscaches 0
set listemots ""
set listemotscaches ""
set tmp ""
# Construction de la liste des mots � cacher, � partir des tags
set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    set tmp [concat $tmp $str]
    }
if {$tmp !=""} {
$t delete 0.0 end
$t insert end $tmp
}
pauto $t

}


proc verif {t} {
global sysFont listemotscaches listessai nbreu essais aide listeaide listevariable disabledfore validation textdict done
incr validation
	for {set i 0} {$i < [llength $listemotscaches]} {incr i} {
	incr essais
    		if {[lindex $listemotscaches $i] == [string trim [$t.ent$i get]]} {
    			if {$done($i)==0} {incr nbreu}
    		bind $t.ent$i <Return> {}
    		catch {destroy .w1}
    		$t.ent$i configure -state disabled -$disabledfore blue
			if {[llength [lindex $textdict $i]]>1 && $done($i)==0} {
			set tmp "[lindex $textdict $i] - [$t.ent$i get]"
			set textdict [lreplace $textdict $i $i $tmp]
			}
    		set done($i) 1
		} else {
		set tmp "[lindex $textdict $i] - [$t.ent$i get]"
		set textdict [lreplace $textdict $i $i $tmp]
        		if {[incr listessai($i)] >= $aide } {
			$t.ent$i delete 0 end
        		$t.ent$i insert end [lindex $listemotscaches $i]
        		$t.ent$i configure -state disabled
		      }
    		}
	affichecouleur $i $t
	}
	if {$validation >= $aide || $nbreu==[llength $listemotscaches]} {
	testefin $nbreu [llength $listemotscaches] $essais
	} else {
	.wtux.speech configure -text [mc {Il y a des fautes, essaie de corriger}]
	bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"

	.wtux.tux configure -image tux_echoue

	}
}

proc testefin {nbreu total essais} {
global sysFont user categorie nbmotscaches
    catch {destroy .menu.titre}
    catch {destroy .menu.lab}
    set str0 [mc {Exercice termine. }]
    #set str2 [format [mc {%1$s essai(s) pour %2$s mot(s).}] $essais $total]
    set str1 [mc {Dict�e}]
    #label .menu.lab -text $str0$str2
    label .menu.lab -text $str0
    #enregistreval $str1 $categorie $str2 $user
    pack .menu.lab
    bell
set score [expr ($nbreu*100)/($nbmotscaches)]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}
catch {destroy .menu.ok}
}



proc affichecouleur {ind t} {
global sysFont listessai xcol ycol disabledback aide
if {$aide == 1} {set col red} else {set col green}
    catch {
    set xcol [winfo x .w1]
    set ycol [winfo y .w1]
    }
switch $listessai($ind) {
    0 { $t.ent$ind configure -$disabledback yellow -bg yellow}
    1 { $t.ent$ind configure -$disabledback $col -bg $col}
    default { $t.ent$ind configure -$disabledback red -bg red}
    }
}



if {$startdirect == 0 } {
main .text
}

proc fin {} {
global sysFont categorie user essais nbmotscaches nbreu listexo iwish filuser aide startdirect repertoire lecture_mot lecture_mot_cache textdict validation

variable repertconf

#set str2 [format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $nbreu $nbmotscaches]
set str2 $textdict
set score [expr ($nbreu*100)/($nbmotscaches)]
switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte n'est pas visible au debut}]}
}

switch $aide {
1 { set aideconf [mc {Au debut}]}
2 { set aideconf [mc {Apres le premier essai}]}
3 { set aideconf [mc {Apres le deuxieme essai}]}
}


set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf - "
set exoconf "$exoconf Correction : $aideconf"
if {$validation == 0} {set str2 ""}
enregistreval [mc {Exercice de dict�e}]\040[lindex [lindex $listexo 13] 1] \173$categorie\175 $str2 $score $repertconf 13 $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit
}

proc boucle {} {
global  categorie startdirect essais nbreu listexo auto user nbmotscaches listemotscaches
set str2 [format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $nbreu $nbmotscaches]
enregistreval [mc {Exercice de dict�e}] \173$categorie\175 $str2 $user
set essais 0
set nbreu 0
set listemotscaches ""
set listexo ""
set categorie "Au choix"
catch {destroy .w1}
.text configure -state normal
set auto [charge .text $categorie]
focus .text
.menu.b1 configure -text [mc {Commencer}] -command "main .text"
catch {destroy .menu.suiv}
if {$startdirect == 0 } {
main .text
}
.text configure -state disabled

}


