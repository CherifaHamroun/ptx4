var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.lesbornes = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
// Déclarer ici les variables globales à l'exercice.
// 
var aNombres, vitesse, reponseProf, temps, temps1, temps2, decalage, repEleve, route, ferrariR, conteneur,largeurB,consignetcor;
// Référencer les ressources de l'exercice (image, son)
//
exo.oRessources = { 
    borne : "lesbornes/images/borne.png",
	ferrari : "lesbornes/images/ferrari.png",
	ferrariC : "lesbornes/images/ferrariC.png",
	route : "lesbornes/images/route.png",
    illustration : "lesbornes/images/illustration.png"
};
// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        tempsExo:180,
        temps_max:180,
		borne_ini:0,
		totalEssai:1,
		typ_duree:0
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
    // pour compatobilité avec vieille version flash
    exo.options.tempsExo = exo.options.temps_max;
};
// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
	aNombres = [0,0,0,0,0,0,0,0,0,0];
};
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html("Les Bornes");
    exo.blocConsigneGenerale.html("Place la voiture à la bonne borne.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};
//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
	exo.keyboard.config({
		numeric:"disabled",
		arrow:"disabled"
	});
	var q = exo.indiceQuestion+1;
	if (exo.options.typ_duree === 0) {
	if (q == 1) {
		vitesse = 40+20*Math.round(Math.random()*3);
		temps = 2+Math.round(Math.random()*6);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,1*vitesse, 2*vitesse, 3*vitesse, 4*vitesse, 5*vitesse, 6*vitesse, 7*vitesse, 8*vitesse, 9*vitesse];
			reponseProf = temps;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+1)*vitesse, (decalage+2)*vitesse, (decalage+3)*vitesse, (decalage+4)*vitesse, (decalage+5)*vitesse, (decalage+6)*vitesse, (decalage+7)*vitesse, (decalage+8)*vitesse, (decalage+9)*vitesse];
			reponseProf = temps;
		}
	}
	if (q == 2) {
		vitesse = 50;
		temps = 1+Math.round(Math.random()*3);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.5*vitesse, 1*vitesse, 1.5*vitesse, 2*vitesse, 2.5*vitesse, 3*vitesse, 3.5*vitesse, 4*vitesse, 4.5*vitesse];
			reponseProf = temps*2;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.5)*vitesse, (decalage+1)*vitesse, (decalage+1.5)*vitesse, (decalage+2)*vitesse, (decalage+2.5)*vitesse, (decalage+3)*vitesse, (decalage+3.5)*vitesse, (decalage+4)*vitesse, (decalage+4.5)*vitesse];
			reponseProf = temps*2;
		}
	}
	if (q == 3) {
		vitesse = 80;
		temps = 1+Math.round(Math.random()*3);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.5*vitesse, 1*vitesse, 1.5*vitesse, 2*vitesse, 2.5*vitesse, 3*vitesse, 3.5*vitesse, 4*vitesse, 4.5*vitesse];
			reponseProf = temps*2;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.5)*vitesse, (decalage+1)*vitesse, (decalage+1.5)*vitesse, (decalage+2)*vitesse, (decalage+2.5)*vitesse, (decalage+3)*vitesse, (decalage+3.5)*vitesse, (decalage+4)*vitesse, (decalage+4.5)*vitesse];
			reponseProf = temps*2;
		}
	}
	if (q == 4) {
		vitesse = 60;
		temps = 1+Math.round(Math.random()*2);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,vitesse/3, 2*vitesse/3, 1*vitesse, 4*vitesse/3, 5*vitesse/3, 2*vitesse, 7*vitesse/3, 8*vitesse/3, 3*vitesse];
			reponseProf = temps*3;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(3*decalage+1)*vitesse/3, (3*decalage+2)*vitesse/3, (3*decalage+3)*vitesse/3, (3*decalage+4)*vitesse/3, (3*decalage+5)*vitesse/3, (3*decalage+6)*vitesse/3, (3*decalage+7)*vitesse/3, (3*decalage+8)*vitesse/3, (3*decalage+9)*vitesse/3];
			reponseProf = temps*3;
		}
	}
	if (q == 5) {
		vitesse = 75;
		temps = 1+Math.round(Math.random()*2);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,vitesse/3, 2*vitesse/3, 1*vitesse, 4*vitesse/3, 5*vitesse/3, 2*vitesse, 7*vitesse/3, 8*vitesse/3, 3*vitesse];
			reponseProf = temps*3;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(3*decalage+1)*vitesse/3, (3*decalage+2)*vitesse/3, (3*decalage+3)*vitesse/3, (3*decalage+4)*vitesse/3, (3*decalage+5)*vitesse/3, (3*decalage+6)*vitesse/3, (3*decalage+7)*vitesse/3, (3*decalage+8)*vitesse/3, (3*decalage+9)*vitesse/3];
			reponseProf = temps*3;
		}
	}
	} else if (exo.options.typ_duree == 1) {
	temps2 = 30;
	if (q == 1) {
		vitesse = 40+20*Math.round(Math.random()*3);
		temps1 = 1+Math.round(Math.random()*2);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.5*vitesse, 1*vitesse, 1.5*vitesse, 2*vitesse, 2.5*vitesse, 3*vitesse, 3.5*vitesse, 4*vitesse, 4.5*vitesse];
			reponseProf = temps1*2+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.5)*vitesse, (decalage+1)*vitesse, (decalage+1.5)*vitesse, (decalage+2)*vitesse, (decalage+2.5)*vitesse, (decalage+3)*vitesse, (decalage+3.5)*vitesse, (decalage+4)*vitesse, (decalage+4.5)*vitesse];
			reponseProf = temps1*2+1;
		}
	}
	if (q == 2) {
		vitesse = 50;
		temps1 = 1+Math.round(Math.random()*3);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.5*vitesse, 1*vitesse, 1.5*vitesse, 2*vitesse, 2.5*vitesse, 3*vitesse, 3.5*vitesse, 4*vitesse, 4.5*vitesse];
			reponseProf = temps1*2+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.5)*vitesse, (decalage+1)*vitesse, (decalage+1.5)*vitesse, (decalage+2)*vitesse, (decalage+2.5)*vitesse, (decalage+3)*vitesse, (decalage+3.5)*vitesse, (decalage+4)*vitesse, (decalage+4.5)*vitesse];
			reponseProf = temps1*2+1;
		}
	}
	if (q == 3) {
		vitesse = 90;
		temps1 = 1+Math.round(Math.random()*3);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.5*vitesse, 1*vitesse, 1.5*vitesse, 2*vitesse, 2.5*vitesse, 3*vitesse, 3.5*vitesse, 4*vitesse, 4.5*vitesse];
			reponseProf = temps1*2+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.5)*vitesse, (decalage+1)*vitesse, (decalage+1.5)*vitesse, (decalage+2)*vitesse, (decalage+2.5)*vitesse, (decalage+3)*vitesse, (decalage+3.5)*vitesse, (decalage+4)*vitesse, (decalage+4.5)*vitesse];
			reponseProf = temps1*2+1;
		}
	}
	if (q == 4) {
		vitesse = 60;
		temps1 = 1+Math.round(Math.random()*3);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.5*vitesse, 1*vitesse, 1.5*vitesse, 2*vitesse, 2.5*vitesse, 3*vitesse, 3.5*vitesse, 4*vitesse, 4.5*vitesse];
			reponseProf = temps1*2+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.5)*vitesse, (decalage+1)*vitesse, (decalage+1.5)*vitesse, (decalage+2)*vitesse, (decalage+2.5)*vitesse, (decalage+3)*vitesse, (decalage+3.5)*vitesse, (decalage+4)*vitesse, (decalage+4.5)*vitesse];
			reponseProf = temps1*2+1;
		}
	}
	if (q == 5) {
		vitesse = 80;
		temps1 = 1+Math.round(Math.random()*3);
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.5*vitesse, 1*vitesse, 1.5*vitesse, 2*vitesse, 2.5*vitesse, 3*vitesse, 3.5*vitesse, 4*vitesse, 4.5*vitesse];
			reponseProf = temps1*2+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.5)*vitesse, (decalage+1)*vitesse, (decalage+1.5)*vitesse, (decalage+2)*vitesse, (decalage+2.5)*vitesse, (decalage+3)*vitesse, (decalage+3.5)*vitesse, (decalage+4)*vitesse, (decalage+4.5)*vitesse];
			reponseProf = temps1*2+1;
		}
	}
	} else if (exo.options.typ_duree == 2) {
	if (q == 1) {
		vitesse = 60;
		temps1 = 1+Math.round(Math.random()*1);
		temps2 = 15;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.25*vitesse, 0.5*vitesse, 0.75*vitesse, 1*vitesse, 1.25*vitesse, 1.5*vitesse, 1.75*vitesse, 2*vitesse, 2.25*vitesse];
			reponseProf = temps1*4+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.25)*vitesse, (decalage+0.5)*vitesse, (decalage+0.75)*vitesse, (decalage+1)*vitesse, (decalage+1.25)*vitesse, (decalage+1.5)*vitesse, (decalage+1.75)*vitesse, (decalage+2)*vitesse, (decalage+2.25)*vitesse];
			reponseProf = temps1*4+1;
		}
	}
	if (q == 2) {
		vitesse = 60;
		temps1 = 0+Math.round(Math.random()*1);
		temps2 = 45;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.25*vitesse, 0.5*vitesse, 0.75*vitesse, 1*vitesse, 1.25*vitesse, 1.5*vitesse, 1.75*vitesse, 2*vitesse, 2.25*vitesse];
			reponseProf = temps1*4+3;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.25)*vitesse, (decalage+0.5)*vitesse, (decalage+0.75)*vitesse, (decalage+1)*vitesse, (decalage+1.25)*vitesse, (decalage+1.5)*vitesse, (decalage+1.75)*vitesse, (decalage+2)*vitesse, (decalage+2.25)*vitesse];
			reponseProf = temps1*4+3;
		}
	}
	if (q == 3) {
		vitesse = 80;
		temps1 = 1+Math.round(Math.random()*1);
		temps2 = 15;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.25*vitesse, 0.5*vitesse, 0.75*vitesse, 1*vitesse, 1.25*vitesse, 1.5*vitesse, 1.75*vitesse, 2*vitesse, 2.25*vitesse];
			reponseProf = temps1*4+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.25)*vitesse, (decalage+0.5)*vitesse, (decalage+0.75)*vitesse, (decalage+1)*vitesse, (decalage+1.25)*vitesse, (decalage+1.5)*vitesse, (decalage+1.75)*vitesse, (decalage+2)*vitesse, (decalage+2.25)*vitesse];
			reponseProf = temps1*4+1;
		}
	}
	if (q == 4) {
		vitesse = 100;
		temps1 = 0+Math.round(Math.random()*1);
		temps2 = 45;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.25*vitesse, 0.5*vitesse, 0.75*vitesse, 1*vitesse, 1.25*vitesse, 1.5*vitesse, 1.75*vitesse, 2*vitesse, 2.25*vitesse];
			reponseProf = temps1*4+3;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.25)*vitesse, (decalage+0.5)*vitesse, (decalage+0.75)*vitesse, (decalage+1)*vitesse, (decalage+1.25)*vitesse, (decalage+1.5)*vitesse, (decalage+1.75)*vitesse, (decalage+2)*vitesse, (decalage+2.25)*vitesse];
			reponseProf = temps1*4+3;
		}
	}
	if (q == 5) {
		vitesse = 120;
		temps1 = 1+Math.round(Math.random()*1);
		temps2 = 15;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,0.25*vitesse, 0.5*vitesse, 0.75*vitesse, 1*vitesse, 1.25*vitesse, 1.5*vitesse, 1.75*vitesse, 2*vitesse, 2.25*vitesse];
			reponseProf = temps1*4+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(decalage+0.25)*vitesse, (decalage+0.5)*vitesse, (decalage+0.75)*vitesse, (decalage+1)*vitesse, (decalage+1.25)*vitesse, (decalage+1.5)*vitesse, (decalage+1.75)*vitesse, (decalage+2)*vitesse, (decalage+2.25)*vitesse];
			reponseProf = temps1*4+1;
		}
	}
	} else if (exo.options.typ_duree == 3) {
	if (q == 1) {
		vitesse = 60;
		temps1 = 1+Math.round(Math.random()*1);
		temps2 = 20;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,1*vitesse/3, 2*vitesse/3, 1*vitesse, 4*vitesse/3, 5*vitesse/3, 2*vitesse, 7*vitesse/3, 8*vitesse/3, 3*vitesse];
			reponseProf = temps1*3+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(3*decalage+1)*vitesse/3, (3*decalage+2)*vitesse/3, (3*decalage+3)*vitesse/3, (3*decalage+4)*vitesse/3, (3*decalage+5)*vitesse/3, (3*decalage+6)*vitesse/3, (3*decalage+7)*vitesse/3, (3*decalage+8)*vitesse/3, (3*decalage+9)*vitesse/3];
			reponseProf = temps1*3+1;
		}
	}
	if (q == 2) {
		vitesse = 90;
		temps1 = 1+Math.round(Math.random()*1);
		temps2 = 40;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,1*vitesse/3, 2*vitesse/3, 1*vitesse, 4*vitesse/3, 5*vitesse/3, 2*vitesse, 7*vitesse/3, 8*vitesse/3, 3*vitesse];
			reponseProf = temps1*3+2;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(3*decalage+1)*vitesse/3, (3*decalage+2)*vitesse/3, (3*decalage+3)*vitesse/3, (3*decalage+4)*vitesse/3, (3*decalage+5)*vitesse/3, (3*decalage+6)*vitesse/3, (3*decalage+7)*vitesse/3, (3*decalage+8)*vitesse/3, (3*decalage+9)*vitesse/3];
			reponseProf = temps1*3+2;
		}
	}
	if (q == 3) {
		vitesse = 90;
		temps1 = 1+Math.round(Math.random()*1);
		temps2 = 20;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,1*vitesse/3, 2*vitesse/3, 1*vitesse, 4*vitesse/3, 5*vitesse/3, 2*vitesse, 7*vitesse/3, 8*vitesse/3, 3*vitesse];
			reponseProf = temps1*3+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(3*decalage+1)*vitesse/3, (3*decalage+2)*vitesse/3, (3*decalage+3)*vitesse/3, (3*decalage+4)*vitesse/3, (3*decalage+5)*vitesse/3, (3*decalage+6)*vitesse/3, (3*decalage+7)*vitesse/3, (3*decalage+8)*vitesse/3, (3*decalage+9)*vitesse/3];
			reponseProf = temps1*3+1;
		}
	}
	if (q == 4) {
		vitesse = 45;
		temps1 = 1+Math.round(Math.random()*1);
		temps2 = 40;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,1*vitesse/3, 2*vitesse/3, 1*vitesse, 4*vitesse/3, 5*vitesse/3, 2*vitesse, 7*vitesse/3, 8*vitesse/3, 3*vitesse];
			reponseProf = temps1*3+2;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(3*decalage+1)*vitesse/3, (3*decalage+2)*vitesse/3, (3*decalage+3)*vitesse/3, (3*decalage+4)*vitesse/3, (3*decalage+5)*vitesse/3, (3*decalage+6)*vitesse/3, (3*decalage+7)*vitesse/3, (3*decalage+8)*vitesse/3, (3*decalage+9)*vitesse/3];
			reponseProf = temps1*3+2;
		}
	}
	if (q == 5) {
		vitesse = 120;
		temps1 = 1+Math.round(Math.random()*1);
		temps2 = 20;
		if (exo.options.borne_ini === 0) {
			aNombres = [0,1*vitesse/3, 2*vitesse/3, 1*vitesse, 4*vitesse/3, 5*vitesse/3, 2*vitesse, 7*vitesse/3, 8*vitesse/3, 3*vitesse];
			reponseProf = temps1*3+1;
		} else {
			decalage = 1+Math.round(Math.random()*5);
			aNombres = [0+decalage*vitesse,(3*decalage+1)*vitesse/3, (3*decalage+2)*vitesse/3, (3*decalage+3)*vitesse/3, (3*decalage+4)*vitesse/3, (3*decalage+5)*vitesse/3, (3*decalage+6)*vitesse/3, (3*decalage+7)*vitesse/3, (3*decalage+8)*vitesse/3, (3*decalage+9)*vitesse/3];
			reponseProf = temps1*3+1;
		}
	}
	}
	repEleve = 0;
    route = disp.createImageSprite(exo,"route");
	conteneur = disp.createEmptySprite();
    conteneur.css({
		width:800,
        height:200,
        my:"left top+100",
        at:"left top",
        of:exo.blocAnimation
    });
	exo.blocAnimation.append(conteneur);
	exo.blocAnimation.append(route);
    route.position({
        my:'left top+200',
        at:'left top',
        of:exo.blocAnimation
    });
	for (var i=0;i<10;i++) {
		var borne = disp.createImageSprite(exo,"borne");
		conteneur.append(borne);
		var etiquette = disp.createTextLabel(aNombres[i]+"<br>km");
		borne.append(etiquette);
		borne.data("valeur",i);
		etiquette.css({
			fontSize:12,
			textAlign:"center"
		});
		etiquette.position({
			my:'center center+7',
			at:'center center',
			of:borne
		});
		var decal=i*(borne.width()+40)+20;
		largeurB = borne.width()+40;
		borne.position({
			my:'left+'+decal+' bottom',
			at:'left top',
			of:route
		});
		borne.on("click",gestionClique);
		if (exo.options.borne_ini !== 0 && i === 0) {
			anim.glow(borne,"gold",4,4);
		}
	}
	function gestionClique(e) {
		repEleve = $(e.delegateTarget).data("valeur");
		var posX = $(e.delegateTarget).position().left;
		ferrariR.transition({left:posX},500,'linear');
	}
	ferrariR = disp.createImageSprite(exo,"ferrari");
	exo.blocAnimation.append(ferrariR);
    ferrariR.position({
        my:'left+15 top+15',
        at:'left top',
        of:route
    });
	var texte = "";
	var cortexte = "";
	if (exo.options.typ_duree === 0) {
		texte = "La voiture se déplace à la vitesse de "+vitesse+" km/h. <br>Clique sur la borne où se trouve la voiture <br>au bout de "+temps+" heures.";
		cortexte = ""+vitesse+" x "+temps+" km";
		if (exo.options.borne_ini !==0) {
			cortexte = ""+decalage*vitesse+" + ("+vitesse+" x "+temps+") km";
		}
	} else {
		texte = "La voiture se déplace à la vitesse de "+vitesse+" km/h. <br>Clique sur la borne où se trouve la voiture <br>au bout de "+temps1+" h "+temps2+" min.";
		cortexte = "("+vitesse+" x "+temps1+") + ("+vitesse+" x "+temps2+" : 60) km";
		if (exo.options.borne_ini !==0) {
			cortexte = ""+decalage*vitesse+" + ("+vitesse+" x "+temps1+") + ("+vitesse+" x "+temps2+" : 60) km";
		}
	} 
	var consignet = disp.createTextLabel(texte);
    consignet.css({
        fontSize:18,
        fontWeight:'bold',
        color:'#000'
    });
    exo.blocAnimation.append(consignet);
    consignet.position({
        my:'left+45 top+70',
        at:'left top',
        of:route
    });
	consignetcor = disp.createTextLabel(cortexte);
    consignetcor.css({
        fontSize:24,
        fontWeight:'bold',
        color:'green'
    });
    exo.blocAnimation.append(consignetcor);
    consignetcor.position({
        my:'left+110 top-140',
        at:'left top',
        of:route
    });
	consignetcor.hide();
};
// Evaluation doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
   if (repEleve === 0) {
       return "rien" ;
    } else if ( repEleve == reponseProf ) {
		conteneur.children().off("click");
        return "juste";
    } else {
		conteneur.children().off("click");
        return "faux";
    }
};
// Correction (peut rester vide)
exo.corriger = function() {
    var posC = largeurB*reponseProf+15;
	var ferrariC = disp.createImageSprite(exo,"ferrariC");
	exo.blocAnimation.append(ferrariC);
    ferrariC.position({
        my:'left+15 top+15',
        at:'left top',
        of:route
    });
	ferrariC.animate({left:posC},500,'linear');
	consignetcor.show();
};
// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:36,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:3,
        nom:"tempsExo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
	//
	controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"borne_ini",
        texte:"Position initiale : ",
        aLabel:["en 0","autre"],
        aValeur:[0,1]
    });
    exo.blocParametre.append(controle);
	//
	controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"typ_duree",
        texte:"Durées : ",
        aLabel:["Heures","Heures et demi","Heures et quarts","Heures et tiers"],
        aValeur:[0,1,2,3]
    });
    exo.blocParametre.append(controle);
};
/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));