<?php
	// la table de correxpondance
	$AS3toJS = array(
		"quadricalc-as3.swf"=>"quadricalc",
        "addiclic-as3.swf"=>"addiclic",
        "operations-a-trou3.swf"=>"operationsatrous",
        "memory-as3-cp.swf"=>"memory",
        "complement-as3.swf"=>"complement",
        "calcul-differe-as3.swf"=>"calculdiffere",
        "somme-en-ligne-as3.swf"=>"sommeenligne",
        "grenouille-as3.swf"=>"grenouille",
        "carre-as3.swf"=>"carre",
        "cible-cycle2-as3.swf"=>"cibles",
        "lacaisse-as3.swf"=>"lacaisse",
        "banquier-as3.swf"=>"lebanquier",
        "math-brique-as3.swf"=>"mbrique",
        "lancer-franc-as3.swf"=>"basketmath2p",
        "tapis-cartes.swf"=>"tapisdecarte",
        "multiclic-table-as3.swf"=>"multiclic",
        "croupier.swf"=>"croupier",
        "nbres-sympas-as3.swf"=>"nombresympathique",
        "bouboule-as3.swf"=>"bouleetboule",
        "bouboule-decimaux-as3.swf"=>"bouleetbouledecimaux",
        "bocal-as3.swf"=>"bocal",
        "table-attaque-as3.swf"=>"tableattaque",
        "rectangle2.swf"=>"rectangle",
        "balance-as3.swf"=>"balance",
        "balance-plus-as3.swf"=>"balanceadd",
        "recette-as3.swf"=>"recette",
        "diviclic-as3.swf"=>"diviclic",
        "approximation-as3-somme.swf"=>"approximationsomme",
        "lesbornespara.swf"=>"lesbornes",
        "chocolat1.swf"=>"chocolat1",
        "chocolat2.swf"=>"chocolat2",
        "viaduc-as3.swf"=>"viaduc"
    );
	// les fichiers
	$files = array('ressources-cp.xml','ressources-ce1.xml','ressources-ce2.xml','ressources-cm1.xml','ressources-cm2.xml','ressources-6eme.xml');

	foreach ($files as $file) {
		$sXML = file_get_contents($file);
		$docXML = new SimpleXMLElement($sXML);
		foreach ($docXML->xpath('//exercice') as $exercice) {
			// on rajoute d'abord des noeuds fichierjs vide quand ils n'existe pas
			if($exercice->fichierjs->count() == 0){
				$exercice->addChild('fichierjs');
			}
			// on donne a fichierjs la valeur correspondante
			$nomAS3 = $exercice->fichier;
			if(isset($AS3toJS[(string)$nomAS3])){
				$exercice->fichierjs = (string)$AS3toJS[(string)$nomAS3];
			}
		}
		$sXML = $docXML->asXML();
		// Écrit le résultat dans le fichier
		file_put_contents($file, $sXML);
	}
	
	
	
    
	
	
?>