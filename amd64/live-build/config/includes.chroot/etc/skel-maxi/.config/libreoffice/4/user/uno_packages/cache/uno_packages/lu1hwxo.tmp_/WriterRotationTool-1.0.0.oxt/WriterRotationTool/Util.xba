<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="Util" script:language="StarBasic">REM  *****  BASIC  *****

&apos;**************************************************
&apos;AUTHOR		: T. VATAIRE
&apos;DATE		: 13/06/09
&apos;VERSION	: 0.4.0
&apos;**************************************************
&apos;This module contains utilities functions
&apos;**************************************************
&apos;Dependencies : Logger, Message, MessageOutput
&apos;**************************************************
&apos;LICENCE	: LGPL v3.0
&apos;**************************************************

option explicit

&apos;uses a dispatcherHelper service to execute an UNO command on a dispatch provider target. this dispatch provider can be another than the active document.
&apos;param dispatchProvider	: object	: an object which implements the XDispatchProvider interface.
&apos;param action			: string	: the UNO command to execute
&apos;param args	(optional)	: variant	: an array of propertyValue which contains optional arguments for the command to execute. could be empty.
&apos;return					: boolean	: true if success, false otherwise
function executeDispatchCommand(dispatchProvider as object, action as string, optional args() as variant) as boolean

	dim success as boolean
	dim dispatcher as object
	
	if (isMissing(args)) then
		args = array()
	end if
	success = hasUnoInterfaces(dispatchProvider, &quot;com.sun.star.frame.XDispatchProvider&quot;)
	if (success) then
		dispatcher = createUnoService(&quot;com.sun.star.frame.DispatchHelper&quot;)
		on local error goto unexpErr
		dispatcher.executeDispatch(dispatchProvider, action, &quot;&quot;, 0, args)
		goto noErr
		unexpErr:
			success = false
			messageOutput_log(message_new(&quot;executeDispatchCommand : An unexpected error occured while executing UNO command : &lt;&quot; &amp; action &amp; &quot;&gt;.&quot;, INT_MESSAGE_TYPE_ERROR, true))
		noErr:
	else
		messageOutput_log(message_new(&quot;executeDispatchCommand : Illegal argument : not a dispatch provider.&quot;, INT_MESSAGE_TYPE_ERROR))
	end if
	
	executeDispatchCommand = success

end function

&apos;adds an url separator at the end of the url if it doesn&apos;t exists. there is no checking of the validity of the URL.
&apos;param url			: string	: the url to check.
&apos;return				: string	: an url identical to &apos;url&apos; parameter if terminal separator already exists, and &apos;url&apos; + separator otherwise.
function addUrlSepIfNeeded(url as string) as string

	dim const urlSeparator = &quot;/&quot;
	dim result as string
	
	result = url
	if (right(result, 1) &lt;&gt; urlSeparator) then
		result = result &amp; urlSeparator
	end if
	
	addUrlSepIfNeeded = result

end function

&apos;checks if an object provides a service or not
&apos;param obj			: object	: the object to check
&apos;param serviceName	: string	: the searched service
&apos;return				: boolean	: true if the object provides the service, false otherwise or if an error occured 
function supportsService(obj as object, serviceName as string) as boolean

	dim success as boolean
	
	&apos;an error occurs if the object is null or if it isn&apos;t an UNO object
	on local error resume next
	success = obj.supportsService(serviceName)
	
	supportsService = success

end function

&apos;returns true if the object implements the service &quot;com.sun.star.text.GenericTextDocument&quot;, false otherwise
function isTextDocument(document as object) as boolean

	isTextDocument = supportsService(document, &quot;com.sun.star.text.GenericTextDocument&quot;)

end function

</script:module>