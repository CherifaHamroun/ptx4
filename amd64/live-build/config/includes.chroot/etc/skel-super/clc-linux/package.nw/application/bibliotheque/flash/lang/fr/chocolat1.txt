﻿//Français :
// Les questions
p0_1=Mange la moitié de cette tablette.
p0_2=Mange le quart de cette tablette.
p0_3=Mange 3/4 de cette tablette.
p0_4=Mange 1/5 de cette tablette.
p0_5=Mange 3/5 de cette tablette.

p1_1=Mange la moitié de cette tablette.
p1_2=Mange le quart de cette tablette.
p1_3=Mange 3/4 de cette tablette.
p1_4=Mange 1/3 de cette tablette.
p1_5=Mange 2/3 de cette tablette.

p2_1=Mange la moitié de cette tablette.
p2_2=Mange le quart de cette tablette.
p2_3=Mange les trois quarts de cette tablette.
p2_4=Mange 1/3 de cette tablette.
p2_5=Mange 2/3 de cette tablette.

p3_1=Mange la moitié de cette tablette.
p3_2=Mange le quart de cette tablette.
p3_3=Mange 3/4 de cette tablette.
p3_4=Mange 1/3 de cette tablette.
p3_5=Mange 2/3 de cette tablette.

//Le titre et la consiqne générale
p4_1 =La tablette de chocolat
p4_2 =Il faut "manger" une partie de la tablette de chocolat.

//Les boutons propres à l'exo
p5_1 =Annuler\rtout