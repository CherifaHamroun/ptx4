﻿class CadreQuestion {
	public var numquestion:Number;
	private var objExo:CalcExo;
	public var cadre_formulaire:MovieClip;
	private var titre:TextField;
	private var champs_texte:Array;
	private var largeur:Number;
	private var hauteur_titre:Number;
	private var hauteur_question:Number;
	private var format_texte:TextFormat;
	private var texte_question:String;
	private var valeurs_question:Array;
	private var rature:MovieClip;
	private var correction:TextField;
	private var keyListener1:Object;
	private var keyListener2:Object;
	
	public function CadreQuestion(objExo) {
		this.objExo=objExo;
		this.champs_texte = new Array();
		this.largeur = 325;
		this.format_texte = new TextFormat();
		this.format_texte.size = 16;
		this.format_texte.bold = true;
		this.format_texte.font = "emb_tahoma";
		this.format_texte.kerning = true;
		//on cree un cadre vide place par default à x=20 y=310
		this.cadre_formulaire = objExo.rootMovie.createEmptyMovieClip("cadre_formulaire", objExo.rootMovie.getNextHighestDepth());
		this.cadre_formulaire._x = 20;
		this.cadre_formulaire._y = 310;
	}
	public function SetQuestion(question:String, aNombres, nLargeurSaisie) {
		if(arguments.length==1){
			//tableau de nombres non spécifié et largeur de saisie non spécifiée
			nLargeurSaisie=50;
		}else if(arguments.length==2 && (!isNaN(arguments[1]))){
			//tableau de nombres non spécifié mais largeur de saisie spécifiée
			var nLargeurSaisie=arguments[1];
		}else if(arguments.length==2 && isNaN(arguments[1])){
			//tableau de nombres spécifié mais largeur de saisie non spécifiée
			//on donne une largeur de saisie par defaut
			nLargeurSaisie=50;
		}
		this.texte_question = question;
		this.valeurs_question = aNombres;
		//on efface la barre rouge et la correction si elles existent;
		if (this.rature != undefined || this.correction != undefined) {
			this.rature.removeMovieClip();
			this.correction.removeTextField();
		}
		//on affiche "Question n°x" 
		if (this.titre == undefined) {
			this.titre = this.cadre_formulaire.createTextField("titre", this.cadre_formulaire.getNextHighestDepth(), 5, 5, 20, 20);
			this.titre.embedFonts = true;
			this.titre.antiAliasType = "advanced";
		}
		this.titre.text = objExo.GetTradCommun("p5_1")+" "+this.numquestion;
		this.titre.setTextFormat(this.format_texte);
		this.titre.autoSize = true;
		//on efface la question si elle existe deja
		if (this.champs_texte.length>0) {
			for (var i = 0; i<this.champs_texte.length; i++) {
				this.champs_texte[i].removeTextField();
			}
		}
		//on affiche la question et ses zones de saisie si elles existent 
		var largeur_saisie = nLargeurSaisie;
		var texte = question;
		var arrayMots = texte.split(" ");
		var questionx = 15;
		var questiony = this.titre.textHeight+10;
		var indice_nombre = 0;
		for (var i = 0; i<arrayMots.length; i++) {
			var j = 0;
			var d = this.cadre_formulaire.getNextHighestDepth();
			if (arrayMots[i] != "$clip$") {
				objExo.rootMovie["label"+i] = this.cadre_formulaire.createTextField("label"+i, d, 0, 0, 0, 0);
				objExo.rootMovie["label"+i].embedFonts = true;
				objExo.rootMovie["label"+i].antiAliasType = "advanced";
				this.champs_texte.push(objExo.rootMovie["label"+i]);
				if (arrayMots[i] != "$var$") {
					objExo.rootMovie["label"+i].text = arrayMots[i];
				} else if (arrayMots[i] == "$var$") {
					objExo.rootMovie["label"+i].text = aNombres[indice_nombre];
					indice_nombre++;
				}
				objExo.rootMovie["label"+i].setTextFormat(this.format_texte);
				objExo.rootMovie["label"+i].autoSize = true;
				if (questionx+objExo.rootMovie["label"+i]._width<this.largeur-15) {
					objExo.rootMovie["label"+i]._x = questionx;
					objExo.rootMovie["label"+i]._y = questiony;
					questionx += objExo.rootMovie["label"+i]._width;
				} else {
					questiony += objExo.rootMovie["label0"]._height;
					objExo.rootMovie["label"+i]._x = 15;
					objExo.rootMovie["label"+i]._y = questiony;
					questionx = objExo.rootMovie["label"+i]._x+objExo.rootMovie["label"+i]._width;
				}
			} else {
				objExo.rootMovie["saisie"+j] = this.cadre_formulaire.createTextField("saisie"+j, d+1, 0, 0, 20, 20);
				objExo.rootMovie["saisie"+j].embedFonts = true;
				objExo.rootMovie["saisie"+j].antiAliasType = "advanced";
				this.champs_texte.push(objExo.rootMovie["saisie"+j]);
				objExo.rootMovie["saisie"+j].type = "input";
				objExo.rootMovie["saisie"+j].border = true;
				objExo.rootMovie["saisie"+j].text = "";
				//objExo.rootMovie["saisie"+j].autoSize=true;
				objExo.rootMovie["saisie"+j]._width = largeur_saisie;
				var format = new TextFormat();
				format.size = 16;
				format.bold = false;
				format.align = "center";
				format.font = "emb_tahoma";
				objExo.rootMovie["saisie"+j].setNewTextFormat(format);
				objExo.rootMovie["saisie"+j]._height = objExo.rootMovie["label0"]._height-3;
				if (questionx+objExo.rootMovie["saisie"+j]._width<this.largeur-15) {
					objExo.rootMovie["saisie"+j]._x = questionx+3;
					objExo.rootMovie["saisie"+j]._y = questiony;
					questionx += objExo.rootMovie["saisie"+j]._width+3;
				} else {
					questiony += objExo.rootMovie["label0"]._height;
					objExo.rootMovie["saisie"+j]._x = 15;
					objExo.rootMovie["saisie"+j]._y = questiony;
					questionx = objExo.rootMovie["saisie"+j]._width+18;
				}
				j++;
			}
		}
		var largeur = this.largeur;
		var hauteur = questiony+objExo.rootMovie["label0"]._height+10;
		var couleur1 = 0xcccccc;
		var couleur2 = 0xDFE4FF;
		var rayon = 10;
		this.cadre_formulaire.clear();
		this.cadre_formulaire.lineStyle(1, couleur1);
		this.cadre_formulaire.beginFill(couleur2);
		drawRect(this.cadre_formulaire, 0, 0, largeur, hauteur, rayon);
		this.cadre_formulaire.endFill;
		
		
		//transforme l'appui sur latouche "." du clavier ou du pavé numérique en symbole virgule
		function myOnKeyDown() {
			if ((Key.isDown(Key.SHIFT) && Key.getCode() == 190) || (Key.getCode() == 110)) {
				var pos=Selection.getFocus().lastIndexOf(".")+1;
				var sInput=Selection.getFocus().substr(pos);
				if(this.keyListener2==undefined){
   					var keyListener2:Object = new Object();
				}
				keyListener2.onChanged=function(){
					var temp=objExo.rootMovie[sInput].text;
					var aTemp=[];
					aTemp=temp.split(".");
					temp=aTemp.join(",");
					objExo.rootMovie[sInput].text=temp;
					objExo.rootMovie[sInput].removeListener(keyListener2);
				}
				objExo.rootMovie[sInput].removeListener(keyListener2);
				objExo.rootMovie[sInput].addListener(keyListener2);
			}
		}
		if(this.keyListener1==undefined){
			this.keyListener1 = new Object();
		}
		keyListener1.onKeyDown = myOnKeyDown;
		Key.removeListener(keyListener1);
		Key.addListener(keyListener1);
		Selection.setFocus(objExo.rootMovie["saisie"+0]);
	
	}
	//
	public function SetWidth(largeur_cadre:Number) {
		this.largeur = largeur_cadre;
		if (this.titre != undefined) {
			SetQuestion(this.texte_question, this.valeurs_question);
		}
	}
	public function SetPos(x:Number,y:Number){
		this.cadre_formulaire._x = x;
		this.cadre_formulaire._y = y;
	}
	public function SetFormatTexte(fmt:TextFormat) {
		this.format_texte = fmt;
		if (this.titre != undefined) {
			SetQuestion(this.texte_question, this.valeurs_question);
		}
	}
	public function GetInput(nInput:Number) {
		var sExpr = String(objExo.rootMovie["saisie"+nInput].text);
		if (sExpr.length>0) {
			var nNombre = Signe.stringToNum(sExpr);
			return nNombre;
		}
	}
	public function CorrigerSaisie(nInput, sValeur, sAlign) {
		this.rature = this.cadre_formulaire.createEmptyMovieClip("rature", this.cadre_formulaire.getNextHighestDepth());
		this.rature.lineStyle(2, 0x7F0A0D, 60);
		this.rature.moveTo(eval("this.cadre_formulaire.saisie"+nInput)._x, eval("this.cadre_formulaire.saisie"+nInput)._y+eval("this.cadre_formulaire.saisie"+nInput)._height);
		this.rature.lineTo(eval("this.cadre_formulaire.saisie"+nInput)._x+eval("this.cadre_formulaire.saisie"+nInput)._width, eval("this.cadre_formulaire.saisie"+nInput)._y);
		if (sAlign == "droite") {
			var posx = eval("this.cadre_formulaire.saisie"+nInput)._x+eval("this.cadre_formulaire.saisie"+nInput)._width+10;
			var posy = eval("this.cadre_formulaire.saisie"+nInput)._y;
		} else if (sAlign == "bas") {
			var posx = eval("this.cadre_formulaire.saisie"+nInput)._x;
			var posy = eval("this.cadre_formulaire.saisie"+nInput)._y+eval("this.cadre_formulaire.saisie"+nInput)._height+10;
		} else if (sAlign == "haut") {
			var posx = eval("this.cadre_formulaire.saisie"+nInput)._x;
			var posy = eval("this.cadre_formulaire.saisie"+nInput)._y-eval("this.cadre_formulaire.saisie"+nInput)._height-10;
		}
		var largeur = eval("this.cadre_formulaire.saisie"+nInput)._width;
		this.correction = this.cadre_formulaire.createTextField("correction", this.cadre_formulaire.getNextHighestDepth(), posx, posy, largeur, 0);
		this.correction.embedFonts = true;
		this.correction.antiAliasType = "advanced";
		this.correction.text = Signe.numToString(sValeur);
		var fmt = new TextFormat();
		fmt.size = 18;
		fmt.bold = true;
		fmt.font = "emb_tahoma";
		fmt.color = 0x339900;
		this.correction.setTextFormat(fmt);
		this.correction.multiline = false;
		this.correction.wordWrap = false;
		this.correction.autoSize = true;
	}
	public function Hide() {
		this.cadre_formulaire._visible = false;
	}
	public function Show() {
		this.cadre_formulaire._visible = true;
	}
	public function Desactive() {
		for (var i = 0; i<champs_texte.length; i++) {
			champs_texte[i].type = "dynamic";
		}
	}
	public function Active() {
		for (var i = 0; i<champs_texte.length; i++) {
			champs_texte[i].type = "input";
		}
	}
	public function SetFocus(){
		Selection.setFocus(objExo.rootMovie["saisie"+0]);
	}
	/*-------------------------------------------------------------
	mc.drawRect is a method for drawing rectangles and
	rounded rectangles. Regular rectangles are
	sufficiently easy that I often just rebuilt the
	method in any file I needed it in, but the rounded
	rectangle was something I was needing more often,
	hence the method. The rounding is very much like
	that of the rectangle tool in Flash where if the
	rectangle is smaller in either dimension than the
	rounding would permit, the rounding scales down to
	fit.
	-------------------------------------------------------------*/
	private function drawRect(targetClip, x, y, w, h, cornerRadius) {
		// ==============
		// mc.drawRect() - by Ric Ewing (ric@formequalsfunction.com) - version 1.1 - 4.7.2002
		// 
		// x, y = top left corner of rect
		// w = width of rect
		// h = height of rect
		// cornerRadius = [optional] radius of rounding for corners (defaults to 0)
		// ==============
		if (arguments.length<4) {
			return;
		}
		// if the user has defined cornerRadius our task is a bit more complex. :) 
		if (cornerRadius>0) {
			// init vars
			var theta, angle, cx, cy, px, py;
			// make sure that w + h are larger than 2*cornerRadius
			if (cornerRadius>Math.min(w, h)/2) {
				cornerRadius = Math.min(w, h)/2;
			}
			// theta = 45 degrees in radians 
			theta = Math.PI/4;
			// draw top line
			targetClip.moveTo(x+cornerRadius, y);
			targetClip.lineTo(x+w-cornerRadius, y);
			//angle is currently 90 degrees
			angle = -Math.PI/2;
			// draw tr corner in two parts
			cx = x+w-cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+w-cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			angle += theta;
			cx = x+w-cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+w-cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			// draw right line
			targetClip.lineTo(x+w, y+h-cornerRadius);
			// draw br corner
			angle += theta;
			cx = x+w-cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+h-cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+w-cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+h-cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			angle += theta;
			cx = x+w-cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+h-cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+w-cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+h-cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			// draw bottom line
			targetClip.lineTo(x+cornerRadius, y+h);
			// draw bl corner
			angle += theta;
			cx = x+cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+h-cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+h-cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			angle += theta;
			cx = x+cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+h-cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+h-cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			// draw left line
			targetClip.lineTo(x, y+cornerRadius);
			// draw tl corner
			angle += theta;
			cx = x+cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			angle += theta;
			cx = x+cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
		} else {
			// cornerRadius was not defined or = 0. This makes it easy.
			targetClip.moveTo(x, y);
			targetClip.lineTo(x+w, y);
			targetClip.lineTo(x+w, y+h);
			targetClip.lineTo(x, y+h);
			targetClip.lineTo(x, y);
		}
	}
}
