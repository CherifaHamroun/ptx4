set etapes 1
set niveaux {0 1 2 3 5}
::1
set niveau 2
set ope {{1 9} {4 5} {2 4}}
set interope {{1 10 1} {4 10 1} {1 8 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr (int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0])]
set ope4 [expr $ope3*10]
set volatil 0
set fact1 10
for {set k 1} {$k < [expr $ope4/10]} {incr k} {set fact1 [concat $fact1 + 10]}
regsub -all {[\040]} $fact1 "" fact1
regsub -all {[+]} $fact1 "-" fact2
set operations [list [list libre [expr $ope2*10 + $ope1-$ope4]] [list libre [expr $ope2*10 + $ope1]]]
set enonce "Les images.\nJ'ai [expr $ope2*10 + $ope1] images.\nJe donne [expr $ope4/10] paquets de 10 images � ma soeur.\nCombien me reste-t-il d'images?"
set cible {{4 3 {} source0} {4 3 {} source1}}
set intervalcible 20
set taillerect 45
set orgy 40
set orgxorig 50
set orgsourcey 80
set orgsourcexorig 600
set source {paquet_images.gif image.gif}
set orient 1
set labelcible {{Mes paquets} {Mes images}}
set quadri 0
set reponse [list [list {1} [list {Il me reste} [list [expr $ope2*10 + $ope1-$ope4]] {images}]]]
set ensembles [list [list [expr $ope2 - $ope4/10]] [list [expr $ope1]]]
set canvash 350
set c1height 160
set opnonautorise {}
::




