set etapes 1
set niveaux {0 1 2 3 4 5}
::1
set niveau 2
set ope {{2 13}}
set interope {{1 20 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set volatil 0
set operations [list [list [expr $ope1 ]+[expr $ope1]] [list [expr $ope1]*2] [list 2*[expr $ope1]]]
set enonce "Le double.\nTim a cueilli $ope1 champignons. Tom en a cueilli le double.\nCombien Tom a-t-il de champignons?"
set cible {{5 6 {} source0} {5 6 {} source0}}
set intervalcible 10
set taillerect 45
set orgy 40
set orgxorig 10
set orgsourcey 100
set orgsourcexorig 650
set source {champignon.gif}
set orient 0
set labelcible {{Tim} {Tom}}
set quadri 0
set reponse [list [list {1} [list {Tom a cueilli} [expr $ope1*2] {champignons.}]]]
set ensembles [list [expr $ope1] [expr $ope1*2]]
set canvash 300
set c1height 160
set opnonautorise {0}
::