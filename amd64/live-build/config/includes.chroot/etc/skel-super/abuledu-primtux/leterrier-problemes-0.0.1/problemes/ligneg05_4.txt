set etapes 1
set niveaux {0 1 2 3 4 5}
::1
set niveau 2
set ope {{3 5} {10 12}}
set interope {{0 10 1} {10 30 1}}
set scaleb {0 60 1}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set operations [list [list [expr $ope1 ]+[expr $ope2 - $ope1]] [list [expr $ope2 - $ope1]+[expr $ope1 ]] [list [expr $ope2]-[expr $ope1]]]
set enonce "Le jeu de l'oie.\nLe pion de Tim est sur la case $ope1.\nIl veut que son pion aille sur la case $ope2.\nDe combien de cases doit-il le faire avancer?"
set reponse [list [list {1} [list {Il doit le faire avancer de} [expr $ope2 - $ope1] case(s).]]]
set resultdessin [expr $ope2]
set source {{0}}
set dpt $ope1
set opnonautorise {0 1}
set canvash 140
set c1height 50
set placearriv none
::