Package: console-setup
Version: 1.164
Architecture: all
Maintainer: Debian Install System Team <debian-boot@lists.debian.org>
Installed-Size: 437
Pre-Depends: debconf (>= 1.5.34)
Depends: console-setup-linux | console-setup-freebsd | hurd, xkb-data (>= 0.9), keyboard-configuration (= 1.164)
Suggests: lsb-base (>= 3.0-6), locales
Conflicts: console-setup-mini
Breaks: lsb (<< 2.0-6), lsb-base (<< 3.0-6), lsb-core (<< 2.0-6)
Filename: pool/main/c/console-setup/console-setup_1.164_all.deb
Size: 117426
MD5sum: 8d728fb718734467b91e08bc846528e7
SHA1: 8e18dc26750cffec1b559e250aeab53636b1c78d
SHA256: 349933498ab73d7fd8364ea603ea171097c1d96af841ca90219c2f6d3fca611b
Section: utils
Priority: optional
Multi-Arch: foreign
Description: console font and keymap setup program
 This package provides the console with the same keyboard
 configuration scheme as the X Window System. As a result, there is no
 need to duplicate or change the keyboard files just to make simple
 customizations such as the use of dead keys, the key functioning as
 AltGr or Compose key, the key(s) to switch between Latin and
 non-Latin mode, etc.
 .
 The package also installs console fonts supporting many of the
 world's languages.  It provides an unified set of font faces - the
 classic VGA, the simplistic Fixed, and the cleaned Terminus,
 TerminusBold and TerminusBoldVGA.

Package: dmeventd
Source: lvm2 (2.02.168-2)
Version: 2:1.02.137-2
Architecture: amd64
Maintainer: Debian LVM Team <pkg-lvm-maintainers@lists.alioth.debian.org>
Installed-Size: 162
Depends: libc6 (>= 2.15), libdevmapper-event1.02.1 (>= 2:1.02.110), libdevmapper1.02.1 (>= 2:1.02.113), liblvm2cmd2.02 (>= 2.02.168), init-system-helpers (>= 1.18~)
Filename: pool/main/l/lvm2/dmeventd_2%3a1.02.137-2_amd64.deb
Size: 59536
MD5sum: 7630320dc0820252df4981a2ba1bc2da
SHA1: a49690e472fe3f775798ba93479a435868bacd1c
SHA256: b0031d3b05ea681d614767e13098b34b1a206252bb1ced7f1526d2a1d87769bd
Section: admin
Priority: optional
Homepage: http://sources.redhat.com/lvm2/
Description: Linux Kernel Device Mapper event daemon
 The Linux Kernel Device Mapper is the LVM (Linux Logical Volume Management)
 Team's implementation of a minimalistic kernel-space driver that handles
 volume management, while keeping knowledge of the underlying device layout
 in user-space.  This makes it useful for not only LVM, but software raid,
 and other drivers that create "virtual" block devices.
 .
 This package contains a daemon to monitor events of devmapper devices.

Package: efibootmgr
Version: 14-2
Architecture: amd64
Maintainer: Debian UEFI Maintainers <debian-efi@lists.debian.org>
Installed-Size: 83
Depends: libc6 (>= 2.14), libefiboot1 (>= 30), libefivar1 (>= 30), libpopt0 (>= 1.14)
Filename: pool/main/e/efibootmgr/efibootmgr_14-2_amd64.deb
Size: 31616
MD5sum: f80b18c2e366973f54cea60809adb534
SHA1: c5a84f1503106e9cbcdbc83fbc6b59f8652cd6a3
SHA256: e9199da677e04d5b968994c4ffb141dc5a60c8e09d1b13106b2b6e77567c5e45
Section: admin
Priority: optional
Multi-Arch: foreign
Description: Interact with the EFI Boot Manager
 This is a Linux user-space application to modify the Intel Extensible
 Firmware Interface (EFI) Boot Manager configuration. This application can
 create and destroy boot entries, change the boot order, change the next
 running boot option, and more.
 .
 Additional information about (U)EFI can be found at http://www.uefi.org/.
 .
 Note: efibootmgr requires that the kernel module efivars be loaded prior
 to use. 'modprobe efivars' should do the trick if it does not
 automatically load.

Package: firmware-linux-free
Source: firmware-free
Version: 3.4
Architecture: all
Maintainer: Debian Kernel Team <debian-kernel@lists.debian.org>
Installed-Size: 68
Suggests: initramfs-tools
Filename: pool/main/f/firmware-free/firmware-linux-free_3.4_all.deb
Size: 19196
MD5sum: 9bef03ddbdb3e595f6f1d6fbc6571def
SHA1: 2daa9a0cb834269bbe65103a68a7f09bfe092689
SHA256: e8506099feb3ce072d4b99cb86833a8e8e531f97fa8a112dee93d58c1525a36b
Section: kernel
Priority: optional
Multi-Arch: foreign
Homepage: http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git
Description: Binary firmware for various drivers in the Linux kernel
 This package contains firmware which was previously included in the Linux
 kernel and which is compliant with the Debian Free Software Guidelines.
 .
 Most firmware previously included in the Linux kernel is non-free and has
 been moved to the firmware-linux-nonfree package.
 .
 Contents:
  * AV7110 DVB card firmware (av7110/bootcode.bin)
  * Atheros AR9170 free firmware for use with carl9170 (carl9170-1.fw)
  * 3Com Megahertz 3CCFEM556 CIS fix (cis/3CCFEM556.cis)
  * 3Com Megahertz 3CXEM556 CIS fix (cis/3CXEM556.cis)
  * Advantech COMpad-32/85 CIS fix (cis/COMpad2.cis)
  * Advantech COMpad-32/85B-4 CIS fix (cis/COMpad4.cis)
  * NSC DP83903 PCMCIA Ethernet card CIS fix (cis/DP83903.cis)
  * Allied Telesis LA-PCM CIS fix (cis/LA-PCM.cis)
  * MultiTech PCMCIA 56K DataFax CIS fix (cis/MT5634ZLX.cis)
  * NDC PCMCIA Ethernet card CIS fix (cis/NE2K.cis)
  * EN2218-LAN/MODEM CIS fix (cis/PCMLM28.cis)
  * PreMax PE-200 CIS fix (cis/PE-200.cis)
  * LanPro EP-4000A CIS fix (cis/PE520.cis)
  * RS-COM 2P CIS fix (cis/RS-COM-2P.cis)
  * Sierra Aircard 555 CIS fix (cis/SW_555_SER.cis)
  * Sierra Wireless AC710/AC750 CIS fix (cis/SW_7xx_SER.cis)
  * Sierra Wireless AC850/AC860 CIS fix (cis/SW_8xx_SER.cis)
  * Tamarack PCMCIA Ethernet card CIS fix (cis/tamarack.cis)
  * Atari Falcon DSP56001 firmware (dsp56k/bootstrap.bin)
  * Intel C600 SAS/SATA controller default parameters, version 1.3
    (isci/isci_firmware.bin)
  * Keyspan USA-19 firmware (keyspan_pda/keyspan_pda.fw)
  * Xircom PGSDB9/Entrega PortGear firmware (keyspan_pda/xircom_pgs.fw)
  * USB-DUX firmware (usbdux_firmware.bin)
  * USB-DUXfast firmware (usbduxfast_firmware.bin)
  * USB-DUXsigma firmware (usbduxsigma_firmware.bin)

Package: grub-efi-amd64
Source: grub2
Version: 2.02~beta3-5
Architecture: amd64
Maintainer: GRUB Maintainers <pkg-grub-devel@lists.alioth.debian.org>
Installed-Size: 206
Pre-Depends: dpkg (>= 1.17.13)
Depends: debconf (>= 0.5) | debconf-2.0, grub-common (= 2.02~beta3-5), grub2-common (= 2.02~beta3-5), grub-efi-amd64-bin (= 2.02~beta3-5), ucf
Conflicts: elilo, grub, grub-coreboot, grub-efi-ia32, grub-ieee1275, grub-legacy, grub-pc, grub-xen
Replaces: grub, grub-common (<= 1.97~beta2-1), grub-coreboot, grub-efi-ia32, grub-ieee1275, grub-legacy, grub-pc, grub2 (<< 2.02~beta3-5)
Filename: pool/main/g/grub2/grub-efi-amd64_2.02~beta3-5_amd64.deb
Size: 73074
MD5sum: 2fd173103c94c5b46041905cabc9f0de
SHA1: 30e128123057388716de490d95b05ed740af1d21
SHA256: 27ed3fd9b848383861d087ef52a666956ef7068ae06ca4321d7141d5104270a5
Section: admin
Priority: extra
Multi-Arch: foreign
Homepage: http://www.gnu.org/software/grub/
Description: GRand Unified Bootloader, version 2 (EFI-AMD64 version)
 GRUB is a portable, powerful bootloader.  This version of GRUB is based on a
 cleaner design than its predecessors, and provides the following new features:
 .
  - Scripting in grub.cfg using BASH-like syntax.
  - Support for modern partition maps such as GPT.
  - Modular generation of grub.cfg via update-grub.  Packages providing GRUB
    add-ons can plug in their own script rules and trigger updates by invoking
    update-grub.
 .
 This package contains a version of GRUB that has been built for use with
 EFI-AMD64 architecture, such as the one provided by Intel Macs (that is, unless
 a BIOS interface has been activated).

Package: grub-efi-amd64-bin
Source: grub2
Version: 2.02~beta3-5
Architecture: amd64
Maintainer: GRUB Maintainers <pkg-grub-devel@lists.alioth.debian.org>
Installed-Size: 2679
Depends: grub-common (= 2.02~beta3-5), efibootmgr
Replaces: grub-common (<= 1.97~beta2-1), grub-efi-amd64 (<< 1.99-1), grub2 (<< 2.02~beta3-5)
Filename: pool/main/g/grub2/grub-efi-amd64-bin_2.02~beta3-5_amd64.deb
Size: 650250
MD5sum: 881913b80e502db26a2d140443a19cd3
SHA1: 5799946e42ae71c44ff9c2028f0258dbec7476af
SHA256: 314631881b1dcfaf23c190cc611cd758e58049c777e11a72f5bd735c63c0738e
Section: admin
Priority: extra
Multi-Arch: foreign
Homepage: http://www.gnu.org/software/grub/
Description: GRand Unified Bootloader, version 2 (EFI-AMD64 binaries)
 GRUB is a portable, powerful bootloader.  This version of GRUB is based on a
 cleaner design than its predecessors, and provides the following new features:
 .
  - Scripting in grub.cfg using BASH-like syntax.
  - Support for modern partition maps such as GPT.
  - Modular generation of grub.cfg via update-grub.  Packages providing GRUB
    add-ons can plug in their own script rules and trigger updates by invoking
    update-grub.
 .
 This package contains a version of GRUB that has been built for use with
 EFI-AMD64 architecture, such as the one provided by Intel Macs (that is, unless
 a BIOS interface has been activated).  It will not automatically install
 GRUB as the active boot loader, nor will it automatically update grub.cfg
 on upgrade, so most people should install grub-efi-amd64 instead.

Package: grub-pc
Source: grub2
Version: 2.02~beta3-5
Architecture: amd64
Maintainer: GRUB Maintainers <pkg-grub-devel@lists.alioth.debian.org>
Installed-Size: 561
Pre-Depends: dpkg (>= 1.17.13)
Depends: debconf (>= 0.5) | debconf-2.0, grub-common (= 2.02~beta3-5), grub2-common (= 2.02~beta3-5), grub-pc-bin (= 2.02~beta3-5), ucf
Conflicts: grub (<< 0.97-54), grub-coreboot, grub-efi-amd64, grub-efi-ia32, grub-ieee1275, grub-legacy, grub-xen
Replaces: grub, grub-common (<= 1.97~beta2-1), grub-coreboot, grub-efi-amd64, grub-efi-ia32, grub-ieee1275, grub-legacy, grub2 (<< 2.02~beta3-5)
Filename: pool/main/g/grub2/grub-pc_2.02~beta3-5_amd64.deb
Size: 204156
MD5sum: b687312f94b0bc43f749da23b6612700
SHA1: 64144e068c043f2c1c0dc1c6f77c67f2c6af2f4f
SHA256: e4917542818574120b28d4b51be4ead8af47af2dccabc507af2d77867e7931a6
Section: admin
Priority: optional
Multi-Arch: foreign
Homepage: http://www.gnu.org/software/grub/
Description: GRand Unified Bootloader, version 2 (PC/BIOS version)
 GRUB is a portable, powerful bootloader.  This version of GRUB is based on a
 cleaner design than its predecessors, and provides the following new features:
 .
  - Scripting in grub.cfg using BASH-like syntax.
  - Support for modern partition maps such as GPT.
  - Modular generation of grub.cfg via update-grub.  Packages providing GRUB
    add-ons can plug in their own script rules and trigger updates by invoking
    update-grub.
  - VESA-based graphical mode with background image support and complete 24-bit
    color set.
  - Support for extended charsets.  Users can write UTF-8 text to their menu
    entries.
 .
 This package contains a version of GRUB that has been built for use with
 traditional PC/BIOS architecture.

Package: hdmi2usb-fx2-firmware
Version: 0.0.0~git20151225-1
Architecture: all
Maintainer: Stefano Rivera <stefanor@debian.org>
Installed-Size: 43
Suggests: fxload
Filename: pool/main/h/hdmi2usb-fx2-firmware/hdmi2usb-fx2-firmware_0.0.0~git20151225-1_all.deb
Size: 10626
MD5sum: a7fd0f0652f0a7f97d0fba732390b88f
SHA1: 58bfb5ddc6c4cb168422ee557e7a4e19cbb5a369
SHA256: a715253d4bba188c3d83fa3cc174d150bf67c74dee7968da38c3c4402f0f8dc5
Section: video
Priority: optional
Homepage: https://github.com/mithro/fx2lib/tree/cdc-usb-serialno-from-eeprom
Description: FX2 firmware for hdmi2usb board development
 This package contains the FX2 firmware for several modes of the Numato Opsis
 board's USB interface.
 .
 It is used for flashing updates to the board.

Package: ixo-usb-jtag
Version: 0.0.0+git20160908-1
Architecture: all
Maintainer: Stefano Rivera <stefanor@debian.org>
Installed-Size: 78
Suggests: fxload
Filename: pool/main/i/ixo-usb-jtag/ixo-usb-jtag_0.0.0+git20160908-1_all.deb
Size: 9896
MD5sum: d5cd2700467596da5a958ea41b624343
SHA1: 99d0b8f143302801c9d356a115b5c475e94ec41d
SHA256: 241366c7b2d11a976761e672bf7036873b571f25cf3a61ea2b48778413e547f3
Section: embedded
Priority: optional
Homepage: https://github.com/mithro/ixo-usb-jtag
Description: Firmware for USB JTAG programmers
 This firmware allows a USB-capable microcontroller to act like an Altera
 USB-Blaster JTAG pod. Which in turn may allow you to use tools you'd normally
 use with the Altera USB-Blaster, including UrJTAG and openocd.
 .
 Supported hardware: The Cypress FX2 EZ-USB family, or an FTDI FT245 in
 combination with a CPLD. Builds are included for the hdmi2usb project's boards
 (Digilet Atlys and Numato Opsis).

Package: jfsutils
Version: 1.1.15-3
Architecture: amd64
Maintainer: Laszlo Boszormenyi (GCS) <gcs@debian.org>
Installed-Size: 1533
Depends: libc6 (>= 2.14), libuuid1 (>= 2.16)
Filename: pool/main/j/jfsutils/jfsutils_1.1.15-3_amd64.deb
Size: 203924
MD5sum: 14ff7b47c9bbbbc51b4ff828974676aa
SHA1: 2e530be02dbebeb7523b2413218bdb611456d5eb
SHA256: 52154c16aefa624aa329d2d18af661df8cb64669ec7aede86fbd5515d9b6bb5f
Section: admin
Priority: optional
Homepage: http://jfs.sourceforge.net/
Description: utilities for managing the JFS filesystem
 Utilities for managing IBM's Journaled File System (JFS) under Linux.
 .
 IBM's journaled file system technology, currently used in IBM
 enterprise servers, is designed for high-throughput server
 environments, key to running intranet and other high-performance
 e-business file servers.
 .
 The following utilities are available:
  * fsck.jfs - initiate replay of the JFS transaction log, and check and
    repair a JFS formatted device.
  * logdump - dump a JFS formatted device's journal log.
  * logredo - "replay" a JFS formatted device's journal log.
  * mkfs.jfs - create a JFS formatted partition.
  * xchkdmp - dump the contents of a JFS fsck log file created with
    xchklog.
  * xchklog - extract a log from the JFS fsck workspace into a file.
  * xpeek - shell-type JFS file system editor.

Package: kbd
Source: kbd (2.0.3-2)
Version: 2.0.3-2+b1
Architecture: amd64
Maintainer: Console utilities maintainers <pkg-kbd-devel@lists.alioth.debian.org>
Installed-Size: 1632
Depends: libc6 (>= 2.14), lsb-base (>= 3.0-10)
Recommends: console-setup | console-data
Conflicts: console-utilities
Provides: console-utilities
Filename: pool/main/k/kbd/kbd_2.0.3-2+b1_amd64.deb
Size: 342516
MD5sum: 7de3b5acc79c8dfbbe9c162f9698a18f
SHA1: 9559f3ffc14233add75e92eaeb1ec118f35960ed
SHA256: 710ce2ee8e6c15998c853727af07e579b694b1e12711cabb7ade64389a63731e
Section: utils
Priority: optional
Homepage: http://www.kbd-project.org/
Description: Linux console font and keytable utilities
 This package allows you to set up the Linux console, change the font,
 resize text mode virtual consoles and remap the keyboard.
 .
 You will probably want to install a set of data files, such as the one
 in the “console-data” package.

Package: keyboard-configuration
Source: console-setup
Version: 1.164
Architecture: all
Maintainer: Debian Install System Team <debian-boot@lists.debian.org>
Installed-Size: 2451
Pre-Depends: debconf (>= 1.5.34)
Depends: liblocale-gettext-perl
Breaks: console-setup (<< 1.71), console-setup-mini (<< 1.47)
Replaces: console-setup (<< 1.47), console-setup-mini (<< 1.47)
Filename: pool/main/c/console-setup/keyboard-configuration_1.164_all.deb
Size: 644390
MD5sum: d35fe841908db679681a126cca32c949
SHA1: a6b226ffe94c54032f23340fd04f09b8c906a04d
SHA256: b06742ad0c5e3956988754b0f444472085a01d8b0a642a59fc3cac74e5d927e3
Section: utils
Priority: optional
Multi-Arch: foreign
Description: system-wide keyboard preferences
 This package maintains the keyboard preferences in
 /etc/default/keyboard.  Other packages can use the information
 provided by this package in order to configure the keyboard on the
 console or in X Window.

Package: libdevmapper-event1.02.1
Source: lvm2 (2.02.168-2)
Version: 2:1.02.137-2
Architecture: amd64
Maintainer: Debian LVM Team <pkg-lvm-maintainers@lists.alioth.debian.org>
Installed-Size: 72
Depends: libc6 (>= 2.15), libdevmapper1.02.1 (>= 2:1.02.103)
Filename: pool/main/l/lvm2/libdevmapper-event1.02.1_2%3a1.02.137-2_amd64.deb
Size: 41610
MD5sum: a6c4130ee2e6f2c359785120e9b18791
SHA1: 0ad6105de242866c6945100b14abd4463c29c910
SHA256: 26bac013cc7d3e40b2c2494891f41e067e6fa7737ca9ce42b76527b10cd8f439
Section: libs
Priority: optional
Multi-Arch: same
Homepage: http://sources.redhat.com/lvm2/
Description: Linux Kernel Device Mapper event support library
 The Linux Kernel Device Mapper is the LVM (Linux Logical Volume Management)
 Team's implementation of a minimalistic kernel-space driver that handles
 volume management, while keeping knowledge of the underlying device layout
 in user-space.  This makes it useful for not only LVM, but software raid,
 and other drivers that create "virtual" block devices.
 .
 This package contains the userspace library to help with event monitoring
 for devmapper devices, in conjunction with the dmevent daemon.

Package: libefiboot1
Source: efivar
Version: 30-2
Architecture: amd64
Maintainer: Debian UEFI Maintainers <debian-efi@lists.debian.org>
Installed-Size: 64
Depends: libc6 (>= 2.14), libefivar1 (>= 30)
Breaks: efibootmgr (<< 0.12-2)
Filename: pool/main/e/efiboot/libefiboot1_30-2_amd64.deb
Size: 24650
MD5sum: 885b08b63d271a2b9154a9ef1c024e24
SHA1: 00f4a36abd6bc38567795bc1b673b56998103d86
SHA256: b6066c1bc4f19040f941e78f0b699b4f4f1a62e636f02927b05dd85c16750746
Section: libs
Priority: optional
Multi-Arch: same
Homepage: https://github.com/rhinstaller/efivar
Description: Library to manage UEFI variables
 Library to allow for the manipulation of UEFI variables related to booting.

Package: libefivar1
Source: efivar
Version: 30-2
Architecture: amd64
Maintainer: Debian UEFI Maintainers <debian-efi@lists.debian.org>
Installed-Size: 144
Depends: libc6 (>= 2.14)
Breaks: efibootmgr (<< 0.12-2)
Filename: pool/main/e/efivar/libefivar1_30-2_amd64.deb
Size: 40820
MD5sum: 0a2b7cdb374aa3893a78ecfae995bc00
SHA1: 3bfb7180745aabf065ae88a7a5f08142f19335c5
SHA256: 19007e694a0ca59c2feb60685e2a26fdd370d13205d3a4b1eaea8475737d7505
Section: libs
Priority: optional
Multi-Arch: same
Homepage: https://github.com/rhinstaller/efivar
Description: Library to manage UEFI variables
 Library to allow for the simple manipulation of UEFI variables.

Package: liblvm2app2.2
Source: lvm2
Version: 2.02.168-2
Architecture: amd64
Maintainer: Debian LVM Team <pkg-lvm-maintainers@lists.alioth.debian.org>
Installed-Size: 1397
Depends: libblkid1 (>= 2.24.2), libc6 (>= 2.15), libdevmapper-event1.02.1 (>= 2:1.02.74), libdevmapper1.02.1 (>= 2:1.02.135), libudev1 (>= 183)
Filename: pool/main/l/lvm2/liblvm2app2.2_2.02.168-2_amd64.deb
Size: 553812
MD5sum: 1e8d5eb33a522a1b78918b76bad735fe
SHA1: 87ee850211c4728992a54df64f58a31b0a08f165
SHA256: 7cefddaf53e0f151ab950314efcdf75cbf35819be2c385c519682d967ad76c9d
Section: libs
Priority: optional
Multi-Arch: same
Homepage: http://sources.redhat.com/lvm2/
Description: LVM2 application library
 This package contains the lvm2app shared library. It allows easier access
 to the basic LVM objects and provides functions to enumerate, create or
 modify them.

Package: liblvm2cmd2.02
Source: lvm2
Version: 2.02.168-2
Architecture: amd64
Maintainer: Debian LVM Team <pkg-lvm-maintainers@lists.alioth.debian.org>
Installed-Size: 1765
Depends: libblkid1 (>= 2.24.2), libc6 (>= 2.15), libdevmapper-event1.02.1 (>= 2:1.02.74), libdevmapper1.02.1 (>= 2:1.02.135), libudev1 (>= 183), dmeventd
Filename: pool/main/l/lvm2/liblvm2cmd2.02_2.02.168-2_amd64.deb
Size: 682980
MD5sum: 36003841d9719d671d26896d67d2b1b6
SHA1: 1f970eca26a92acb1e767b1c4b2a9023a17c5f94
SHA256: e47ae7aee89760f20c4dace47638575085810ab9f135c7bc7bcf69f4dbd383d5
Section: libs
Priority: optional
Multi-Arch: same
Homepage: http://sources.redhat.com/lvm2/
Description: LVM2 command library
 This package contains the lvm2cmd shared library.

Package: lilo
Source: lilo (1:24.2-2)
Version: 1:24.2-2+b1
Architecture: amd64
Maintainer: Joachim Wiedorn <joodebian@joonet.de>
Installed-Size: 694
Pre-Depends: dpkg (>= 1.15.7.2)
Depends: perl:any, libc6 (>= 2.14), libdevmapper1.02.1 (>= 2:1.02.20), debconf (>= 0.5) | debconf-2.0
Filename: pool/main/l/lilo/lilo_1%3a24.2-2+b1_amd64.deb
Size: 291532
MD5sum: 41673922ae8077fec16832f31b98e61f
SHA1: 57b9fb26074fd7293ebdb1ebac33bb5ef7c70b52
SHA256: 161565ee5e0bb7c831a4f26b1e0b9bee5af70bd2b125a47d921af38fc1d5e577
Section: admin
Priority: optional
Homepage: http://lilo.alioth.debian.org/
Description: LInux LOader - the classic OS boot loader
 You can use LILO to manage your Master Boot Record (with a simple text
 screen, text menu or colorful splash graphics) or call LILO from other
 Boot-Loaders to jump-start the Linux kernel.
 .
 This package contains lilo (the installer) and boot-record-images to
 install Linux, DOS and generic Boot Sectors for other operation systems.

Package: lvm2
Version: 2.02.168-2
Architecture: amd64
Maintainer: Debian LVM Team <pkg-lvm-maintainers@lists.alioth.debian.org>
Installed-Size: 2366
Depends: libblkid1 (>= 2.24.2), libc6 (>= 2.15), libdevmapper-event1.02.1 (>= 2:1.02.74), libdevmapper1.02.1 (>= 2:1.02.135), liblvm2app2.2 (>= 2.02.168), libreadline5 (>= 5.2), libudev1 (>= 183), init-system-helpers (>= 1.18~), lsb-base, dmsetup, dmeventd
Suggests: thin-provisioning-tools
Filename: pool/main/l/lvm2/lvm2_2.02.168-2_amd64.deb
Size: 932906
MD5sum: ed36e8ae741ab1ebdb0befecaa797308
SHA1: 72710f708baaa479eafa85288e82a2a88187f572
SHA256: 3d98279f2bae8b34512d06c257a9dffc3531a8030c1668f7ea95e1bce0ab1929
Section: admin
Priority: optional
Multi-Arch: foreign
Homepage: http://sources.redhat.com/lvm2/
Description: Linux Logical Volume Manager
 This is LVM2, the rewrite of The Linux Logical Volume Manager.  LVM
 supports enterprise level volume management of disk and disk subsystems
 by grouping arbitrary disks into volume groups. The total capacity of
 volume groups can be allocated to logical volumes, which are accessed as
 regular block devices.

Package: mdadm
Source: mdadm (3.4-4)
Version: 3.4-4+b1
Architecture: amd64
Maintainer: Debian mdadm maintainers <pkg-mdadm-devel@lists.alioth.debian.org>
Installed-Size: 1146
Depends: libc6 (>= 2.15), udev, debconf (>= 0.5) | debconf-2.0, lsb-base, debconf
Recommends: default-mta | mail-transport-agent, kmod | module-init-tools
Filename: pool/main/m/mdadm/mdadm_3.4-4+b1_amd64.deb
Size: 430056
MD5sum: 4295106a090d9a5ffd2378f2813f95fb
SHA1: c06822bb02a91a90594308cf75023a29a5a993c1
SHA256: 3d723c84072e472c4df21494c2f40109c13fa9ec1d885e4ebdd73b921938d4e7
Section: admin
Priority: optional
Homepage: http://neil.brown.name/blog/mdadm
Description: tool to administer Linux MD arrays (software RAID)
 The mdadm utility can be used to create, manage, and monitor MD
 (multi-disk) arrays for software RAID or multipath I/O.
 .
 This package automatically configures mdadm to assemble arrays during the
 system startup process. If not needed, this functionality can be disabled.

Package: xfsprogs
Version: 4.9.0+nmu1
Architecture: amd64
Maintainer: XFS Development Team <linux-xfs@vger.kernel.org>
Installed-Size: 4338
Depends: libblkid1 (>= 2.17.2), libc6 (>= 2.14), libreadline5 (>= 5.2), libuuid1 (>= 2.16)
Suggests: xfsdump, acl, attr, quota
Breaks: xfsdump (<< 3.0.0)
Replaces: xfsdump (<< 3.0.0)
Provides: fsck-backend
Filename: pool/main/x/xfsprogs/xfsprogs_4.9.0+nmu1_amd64.deb
Size: 811354
MD5sum: 1f8b355a4ce8c0dd179bd30d9d430981
SHA1: ad273a05ab01ce799b78e7eb87b2cfae248e5d6f
SHA256: 152eedd57ac27650fd928c748354166d5b0f0d8f76087e7d32ed2c4cb6597090
Section: admin
Priority: optional
Homepage: http://xfs.org/
Description: Utilities for managing the XFS filesystem
 A set of commands to use the XFS filesystem, including mkfs.xfs.
 .
 XFS is a high performance journaling filesystem which originated
 on the SGI IRIX platform.  It is completely multi-threaded, can
 support large files and large filesystems, extended attributes,
 variable block sizes, is extent based, and makes extensive use of
 Btrees (directories, extents, free space) to aid both performance
 and scalability.
 .
 Refer to the documentation at http://oss.sgi.com/projects/xfs/
 for complete details.

