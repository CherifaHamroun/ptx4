set etapes 1
set niveaux {0 1 2 3 5}
::1
set niveau 2
set ope {{10 10} {5 4} {5 4}}
set interope {{10 30 1} {1 10 1} {1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0]]
set volatil 1
if {$ope3>=$ope2} {set fact1 [list [expr $ope1]+[expr $ope3 - $ope2]]} else {set fact1 [list [expr $ope1]-[expr $ope2 - $ope3]]}
if {$ope3>=$ope2} {set fact2 [list [expr $ope3 - $ope2]+[expr $ope1]]} else {set fact2 [list [expr $ope1]-[expr $ope2 - $ope3]]}
set operations [list [list [expr $ope1 ]-[expr $ope2]+[expr $ope3]] [list [expr $ope1 ]+[expr $ope3]-[expr $ope2]] [list [expr $ope3 ]-[expr $ope2]+[expr $ope1]] [list [expr $ope3 ]+[expr $ope1]-[expr $ope2]] [list [expr $ope1+$ope3]-[expr $ope2]] [list [expr $ope1-$ope2]+[expr $ope3]] [list [expr $ope3]+[expr $ope1-$ope2]] $fact1 $fact2]
set enonce "Le car.\nDans le car, il y a d�j� $ope1 personnes.\nAu premier arr�t, il y a $ope2 personnes qui descendent et $ope3 personnes qui montent.\nCombien y a-t-il de personnes maintenant dans le car?"
set cible [list [list 6 5 [list garcon.gif $ope1] source0]]
set intervalcible 20
set taillerect 45
set orgy 40
set orgxorig 50
set orgsourcey 80
set orgsourcexorig 600
set source {garcon.gif}
set orient 1
set labelcible {Car}
set quadri 0
set reponse [list [list {1} [list {Il y a} [expr $ope1 - $ope2 + $ope3] {personnes maintenant dans le car}]]]
set ensembles [list [expr $ope1 + $ope3 - $ope2]]
set canvash 350
set c1height 160
set opnonautorise {}
::





