set etapes 1
set niveaux {0 1 3 5}
::1
set niveau 1
set ope {{3 5} {2 3} {2 2} {2 3}  {2 2}}
set interope {{0 10 1} {0 10 1} {0 10 1} {0 10 1} {0 10 1}}
set scaleb {0 60 1}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0]]
set ope4 [expr int(rand()*[lindex [lindex $ope 3] 1]) + [lindex [lindex $ope 3] 0]]
set ope5 [expr int(rand()*[lindex [lindex $ope 4] 1]) + [lindex [lindex $ope 4] 0]]
set operations [list [list libre [expr $ope1 + $ope2*$ope3 + $ope4*$ope5]]]
set enonce "Le jeu de l'oie.\nLe pion de Tom est sur la case $ope1.\nIl avance de $ope2 fois $ope3 cases, puis encore de $ope4 fois $ope5 cases.\nSur quelle case se trouve le pion de Tom maintenant?"
set reponse [list [list {1} [list {Il est maintenant sur la case} [expr $ope1 + $ope2*$ope3 + $ope4*$ope5]]]]
set resultdessin [expr $ope1 + $ope2*$ope3 + $ope4*$ope5]
set source {{0}}
set dpt $ope1
set opnonautorise {}
set canvash 140
set c1height 50
set placearriv none
::