set etapes 1
set niveaux {0 1 2 4}
::1
set niveau 2
set ope {{4 4}}
set interope {{2 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1])*2 + [lindex [lindex $ope 0] 0]]
set operations [list [list [expr $ope1 ]+[expr $ope1]] [list [expr $ope1]*2] [list 2*[expr $ope1]] [list [expr $ope1*2 ]/2]]
set volatil 0
set editenon "([expr $ope1*2] �toiles sont repr�sent�es, $ope1 pour chacun.)"
set enonce "On partage.\nTim et Tom veulent se partager ces �toiles.\nIls en veulent exactement autant l'un que l'autre."
set cible {{6 4 {} source0} {6 4 {} source0}}
set intervalcible 20
set taillerect 25
set orgy 50
set orgxorig 50
set orgsourcey 50
set orgsourcexorig 550
set source [list [list etoile.gif [expr $ope1*2] 8 7]]
set orient 1
set labelcible {Tim Tom}
set quadri 0
set reponse [list [list {1} [list {Il y a} [expr $ope1] {�toiles pour chacun.}]]]
set ensembles [list [expr $ope1] [expr $ope1]]
set canvash 300
set c1height 160
set opnonautorise {0}
::

