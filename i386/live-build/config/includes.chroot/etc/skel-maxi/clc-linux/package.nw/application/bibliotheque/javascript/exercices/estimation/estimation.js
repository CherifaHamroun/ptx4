var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.estimation = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
var aNombre,repEleve,droite,tolerance,aSoluce;

exo.oRessources = {
	txt:"estimation/textes/estimation_fr.json",
	illustration:"estimation/images/illustration.jpg",
	barre:"estimation/images/barre.png",
	grad:"estimation/images/graduation.png",
	boule:"estimation/images/boule.png",
	bouleOrange:"estimation/images/bouleOrange.png",
	bouleRouge:"estimation/images/bouleRouge.png"
};

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:4,
        totalEssai:2,
		tempsQuestion:0,
        tempsExo:0,
		tolerance:5,
		valeurInf:0,
		valeurSup:10,
        plageCible:"2;13 - 8;2 x 3;4 x 0,25;8 : 2"
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    aNombre=exo.options.plageCible.split(";");
    aSoluce=[];
    // securisation l'expression de doit comporter que 0-9 + - x :,
    re = /[^0-9+-x, :]/;
    for( var i = 0; i < aNombre.length; i++ ){
        if(aNombre[i].match(re) !== null){
            alert("Paramètre invalide");
            break;
        }
        else {
            aSoluce[i] = eval(aNombre[i].replace(" ","").replace("x","*").replace(",",".").replace(":","/"));
        }
        
    }
    exo.options.totalQuestion = aNombre.length;
    // la tolerance par défaut est 5% de la longueur totale
    tolerance = exo.options.tolerance*(exo.options.valeurSup-exo.options.valeurInf)/100;
};

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigneGenerale);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);   
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
	exo.keyboard.config({
		numeric:"disabled",
		arrow:"disabled"
	});
	exo.str.info.msgFaux = exo.txt.p2;
	exo.str.info.msgJuste = exo.txt.p1;
	exo.blocInfo.css({top:300});

    // La consigne
    var consigne = disp.createTextLabel(exo.txt.consigne).css({fontSize:18,fontWeight:"bold"});
    exo.blocAnimation.append(consigne);
    consigne.css({left:(735-consigne.width())/2,top:20});

    // le nombre ou expression à placer
    var expre = aNombre[q];
    var paveNombre = disp.createTextLabel(expre);
    paveNombre.css({minWidth:200,background:"#F57D00",color:"#fff",fontSize:42,fontWeight:"bold",textAlign:"center",padding:10,border:"1px solid #ccc",boxShadow: "3px 3px 3px #ccc"});
    exo.blocAnimation.append(paveNombre);
    paveNombre.css({left:(735-paveNombre.width()-20)/2,top:70});

    // la droite numérique
    droite = disp.createImageSprite(exo,"barre").css({cursor:"pointer"});
    droite.css({left:(735-droite.width())/2,top:220});
    exo.blocAnimation.append(droite);

    // la borne inférieure
    var borneInf = disp.createImageSprite(exo,"boule");
    var labelInf = disp.createTextLabel(String(exo.options.valeurInf));
    var size = String(exo.options.valeurInf).length < 4 ? 24 : 22;
    labelInf.css({color:"#fff",width:borneInf.width(),top:(50-size-5)/2,fontSize:size,textAlign:"center",fontWeight:"bold"});
    borneInf.append(labelInf);
    borneInf.css({left:droite.position().left-24,top:droite.position().top-40});
    exo.blocAnimation.append(borneInf);

    // la borne supérieure
    var borneSup = disp.createImageSprite(exo,"boule");
    var labelSup = disp.createTextLabel(String(exo.options.valeurSup));
    size = String(exo.options.valeurSup).length < 4 ? 24 : 22;
    labelSup.css({color:"#fff",width:borneSup.width(),top:(50-size-5)/2,fontSize:size,textAlign:"center",fontWeight:"bold"});
    borneSup.append(labelSup);
    borneSup.css({left:droite.position().left+droite.width()-27,top:droite.position().top-40});
    exo.blocAnimation.append(borneSup);

    // la borne réponse
    var borneReponse = disp.createImageSprite(exo,"bouleOrange");
    borneReponse.css({top:droite.position().top-40});

    // le bloc info
    exo.blocInfo.css({left:(735-exo.blocInfo.width())/2,top:"+=20px"});

    exo.blocAnimation.append(droite);
    repEleve = 0;
    droite.on("mousedown.clc,touchstart.clc",gererClickDroite);


    function gererClickDroite(e){
        var posPixel = e.pageX-$(e.target).offset().left;
        exo.blocAnimation.append(borneReponse);
        exo.blocAnimation.append(droite);
        borneReponse.css({left:droite.position().left+posPixel-25});
        var uniteParPixel = (exo.options.valeurSup - exo.options.valeurInf)/600;
        repEleve = exo.options.valeurInf + posPixel * uniteParPixel;
    }
};
    

// Evaluation doit toujours retourner "juste" "faux" ou "rien"


exo.evaluer = function() {
	var q = exo.indiceQuestion;
    var soluce = aSoluce[q];
    if(repEleve === 0){
        return "rien";
    }
    else if( Math.abs(repEleve-soluce) < tolerance ){
        var pixelParUnite = 600/(exo.options.valeurSup - exo.options.valeurInf);
        var graduation = disp.createImageSprite(exo,"grad");
        var expre = aNombre[q];
        if(aNombre[q].indexOf("+")>-1 || aNombre[q].indexOf("-")>-1 || aNombre[q].indexOf("x")>-1 || aNombre[q].indexOf(":")>-1){
            expre += " = "+soluce;
        }
        var labelExpre = disp.createTextLabel(expre);
        graduation.css({left:droite.position().left+(soluce-exo.options.valeurInf)*pixelParUnite,top:droite.position().top+30});
        exo.blocAnimation.append(graduation);
        console.log(graduation.position().left);
        labelExpre.css({fontSize:20,fontWeight:"bold",textAlign:"center"});
        exo.blocAnimation.append(labelExpre);
        labelExpre.css({left:graduation.position().left-labelExpre.width()/2,top:graduation.position().top+25});
        return "juste";
    }
    else{
        return "faux";
    }
};

// Correction (peut rester vide)

exo.corriger = function() {
    var q = exo.indiceQuestion;
    var soluce = aSoluce[q];
	var pixelParUnite = 600/(exo.options.valeurSup - exo.options.valeurInf);
    var graduation = disp.createImageSprite(exo,"grad");
    // le nombre au dessus de la ligne
    var labelNombre = disp.createTextLabel(util.numToStr(soluce));
    var borneCorrection = disp.createImageSprite(exo,"bouleRouge");
    borneCorrection.css({left:droite.position().left+(soluce-exo.options.valeurInf)*pixelParUnite-25,top:droite.position().top-40});
    exo.blocAnimation.append(borneCorrection);
    borneCorrection.append(labelNombre);
    var size = String(soluce).length < 4 ? 24 : 22;
    labelNombre.css({color:"#fff",width:borneCorrection.width(),top:(50-size-5)/2,fontSize:size,textAlign:"center",fontWeight:"bold"});
    
    // si l'expression est un calcul on le place en dessous de la ligne
    if(aNombre[q].indexOf("+")>-1 || aNombre[q].indexOf("-")>-1 || aNombre[q].indexOf("x")>-1 || aNombre[q].indexOf(":")>-1){
        var expre = disp.createTextLabel(aNombre[q]+" = "+soluce);
        graduation.css({left:droite.position().left+(soluce-exo.options.valeurInf)*pixelParUnite,top:droite.position().top+30});
        exo.blocAnimation.append(graduation);
        expre.css({fontSize:20,fontWeight:"bold",textAlign:"center",color:"#ff0000"});
        exo.blocAnimation.append(expre);
        expre.css({left:graduation.position().left-expre.width()/2,top:graduation.position().top+25});
    }
    
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle;
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:72,
        taille:4,
        nom:"valeurInf",
        texte:exo.txt.option3
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:72,
        taille:4,
        nom:"valeurSup",
        texte:exo.txt.option4
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        taille:1000,
        nom:"plageCible",
        texte:exo.txt.option5
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:2,
        nom:"tolerance",
        texte:exo.txt.option2,
        texteApres:"(en % de la longueur totale)"
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));