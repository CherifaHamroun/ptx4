var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.araignee = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var stage;
var Etapes; //tableau 2d contenant les Pastilles , un tableau par branche, le dernier élément de chaque tableau étant le centre de la toile , de l'extérieur vers l'intérieur.
var Centre; //la Pastille centrale
var BRANCHE //nombre de branches 
var NCERCLE; // nombre de toiles concentriques
var TABLE; // table de multiplication
var LONGUEUR; // Longueur du chemin
var MULTIPLE; // tableau contenant les multiples de Table
var UNMULTIPLE; //tableau contenant les non multiples de Table
var POSITION; // Tableau 2x2 contenant (b,c) b la branche actuelle et c le cercle actuel.
var SOLUTION; // tableau contenant les pastilles solutions
var pos; // position dans le tableau précédent de la pastille solution en cours.
var WIN; // variable indiquant si le choix de pastille est bon ou mauvais
var MOUCHE; //Clip de la mouche
var COCCI; //Clip de la coccinelle
var T;//La toile
var SPIDER;//Le Clip araignée
var Brep;//compteur de bonnes réponses
var A_table,A_branche,A_ncercle; //Tableau contenant les données des 5 questions fu rallye



// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:3,  //nombre de questions
        totalEssai:1,
		totalTentative:5, //longueur du chemin de la coccinnelle 	
        
        tempsExo:180,
        
        table:"5-9",    //table de multiplication min et max. 
        //Attention ! il faut que (max-min)>=totalQuestion car on ne donnera pas deux fois la même table dans l'exercice.
        
        branche:6, //nombre de branches de la toile
        cercle:3, //nombre de polygones concentriques 
        //Attention ! il faut que branche*cercle soit suffisamment grand pour contenir le chemin.
        
        maximum:10, // valeur maximal du deuxième facteur: 10 signifie par exemple que les valeurs tirées ne dépasseront pas 10*table. 
        // Elles seront aussi inférieur à 100 (non paramétrable pour l'instant).
      
        
    };
    
    
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "araignee/textes/araignee_fr.json",
	illustration:"araignee/images/araignee.jpg",
	bred:"araignee/images/B_Red.png",
	bblue:"araignee/images/B_Blue.png",
	bgrey:"araignee/images/B_Grey.png",
	bgreen:"araignee/images/B_Green.png",
	mo1:"araignee/images/Mouche.png",
	mo2:"araignee/images/Mouche2.png",
	mo3:"araignee/images/Mouche3.png",
	co1:"araignee/images/Cocci.png",
	co2:"araignee/images/Cocci2.png",
	co3:"araignee/images/Cocci3.png",
	sp1:"araignee/images/Spider.png",
	grenier:"araignee/images/back.jpg"
	
	
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
   A_table=new Array()
   A_branche=new Array()
   A_ncercle=new Array()
   var t=util.getArrayNombre(exo.options.table);
    
   for(var i=0;i<exo.options.totalQuestion;i++)
        {   var rand=Math.floor(Math.random()*t.length);           
            A_table.push(t[rand]);
            t.splice(rand,1);
            A_branche.push(exo.options.branche)
            A_ncercle.push(exo.options.cercle)
        }  
   
	
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
	var illustration = disp.createImageSprite(exo,"illustration");
   exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    // on ajoute un element canvas au bloc animation 
    // Attention ses dimensions doivent être fixées avec .attr(width:xx,height:xx)
    // et pas avec .css({width:xx,height:yy})
	var canvas = $("<canvas id='cnv'></canvas>");
    canvas.attr({width:735,height:400});
    canvas.css({position:'absolute',top:0,background:"#FFFFFF"});
    exo.blocAnimation.append(canvas);
    //on cree la scene avec createjs
    stage = new createjs.Stage("cnv");
	stage.enableMouseOver()

    /**************************************/
    exo.aStage.push(stage); // pour qu'on puise appeler Touch.disable() au déchargement de l'exercice
//	createjs.Ticker.setFPS(60);
    createjs.Ticker.timingMode = createjs.Ticker.RAF; // pour une animation Tween plus fluide
    createjs.Ticker.addEventListener("tick", stage); // pour que la scene soit redessinée à chaque "tick"
    createjs.Ticker.addEventListener("tick", createjs.Tween); // pour que Tween fonctionne après un Ticker.reset() au déchargement de l'exercice;
    createjs.Touch.enable(stage);// pour compatibilité avec les tablettes tactiles
    /*************************************/
	exo.keyboard.config({
		numeric : "disabled",
		arrow : "disabled",
		large : "disabled"
    });
	exo.str.info.msgTempsDepasseFaux=exo.str.info.msgFin;
	var i,j
	 BRANCHE=A_branche[exo.indiceQuestion]  //nombre de branches 
	 NCERCLE=A_ncercle[exo.indiceQuestion]; // nombre de toiles concentriques
	 LONGUEUR=exo.options.totalTentative-1// Longueur du chemin
	 MULTIPLE=new Array()
	 UNMULTIPLE=new Array()	
	 TABLE=A_table[exo.indiceQuestion]; // table de multiplication
	 Brep=0;
	
	// Création des tableaux de valeurs (multiples ou non de TABLE)
	for(i=1;i<Math.min(exo.options.maximum*TABLE,100);i++){
		if(i%TABLE==0){
			MULTIPLE.push(i);
		} else {
			UNMULTIPLE.push(i);
		}
	}
     util.shuffleArray(MULTIPLE);
	 util.shuffleArray(UNMULTIPLE);
    
    console.log(MULTIPLE)
    console.log(UNMULTIPLE);
	 
	//LONGUEUR=Math.min(BRANCHE+1,Math.min(LONGUEUR,MULTIPLE.length));	 
	
	var background=new createjs.Bitmap(exo.getURI("grenier"))
//	stage.addChild(background);
	
	T=new Toile(BRANCHE,NCERCLE);
	var Scale=1.6*(-0.15* NCERCLE+1.45);
	Centre=new Pastille(-1,Scale);
	Etapes=new Array();
	// on vérifie que les paramètres de la toile sont raisonnables
	var n=Math.max(2,Math.min(NCERCLE,5));
	var m=Math.max(4,Math.min(BRANCHE,10));
	
	// création du tableau contenant les pastilles
	for(i=0;i<m;i++){
		var c=new Array();
		for(j=0;j<n+1;j++){
		if(j==n){c.push(Centre)}else { c.push(new Pastille(-1,Scale));}
		}
		Etapes.push(c);
	}
	
	// placement de la toile sur le stage
	Centre.dessine();	
	Centre.couleur("gris");
	T.dessine(Etapes,Centre);
	T.Clip.x=365;
	T.Clip.y=200;
	stage.addChild(T.Clip);
	
	//création du chemin
	var c=Chemin();
	while(c==false){
		SOLUTION=new Array();
		//console.log("on relance la creation du chemin");
		MULTIPLE=new Array();
		UNMULTIPLE=new Array();
		for(i=1;i<Math.min(11*TABLE,100);i++){
		if(i%TABLE==0){
			MULTIPLE.push(i);
		} else {
			UNMULTIPLE.push(i);
		}
		}
     util.shuffleArray(MULTIPLE);
	 util.shuffleArray(UNMULTIPLE);
		for(i=0;i<Etapes.length;i++){
			for(j=0;j<Etapes[0].length;j++){
		    Etapes[i][j].Text.text="-1";
		  }
	     }
	c=Chemin();	
	}
	
	// remplissage des pastilles avec les nombres 
	for(i=0;i<Etapes.length;i++){
			for(j=0;j<Etapes[0].length;j++){
		   if(Etapes[i][j].Text.text=="-1"){
			   Etapes[i][j].Text.text=UNMULTIPLE.pop();			  
		   } 
		   Etapes[i][j].Clip.N=parseInt(Etapes[i][j].Text.text);
		    Etapes[i][j].Clip.Pos=[i,j]
		   
		  }
	  }
	
	POSITION=[SOLUTION[0].Clip.Pos[0],SOLUTION[0].Clip.Pos[1]];
	pos=0;	
	exo.btnValider.hide()
	// Création des clips MOUCHE,COCCI et SPIDER
	MOUCHE=new Mouche(0.9-0.03*m);
	MOUCHE.fond.x=800
	MOUCHE.fond.y=-50;
	stage.addChildAt(MOUCHE.fond,1);
	MOUCHE.Animer(true);
	var p=SOLUTION[SOLUTION.length-1]
	var delta=2*Math.PI/m		
	var R=180-(p.Clip.Pos[1])*180/n
	var X=R*Math.cos(p.Clip.Pos[0]*delta)+T.Clip.x
	var Y=R*Math.sin(p.Clip.Pos[0]*delta)+T.Clip.y	
	createjs.Tween.get(MOUCHE.fond).to({x:X,y:Y,scaleX:MOUCHE.S,scaleY:MOUCHE.S},4000,createjs.Ease.linear()).call(Stop1);	
	
	COCCI=new Cocci(0.9-0.03*m);
	COCCI.fond.x=800
	COCCI.fond.y=500;
	stage.addChild(COCCI.fond);
	COCCI.Animer(true);
	p=SOLUTION[0]
	R=250-(p.Clip.Pos[1])*250/n
	X=R*Math.cos(0)+T.Clip.x
	Y=R*Math.sin(0)+T.Clip.y	
	
	function Stop1(){
		MOUCHE.Animer(false);
		createjs.Tween.get(COCCI.fond).to({x:X,y:Y,scaleX:COCCI.S,scaleY:COCCI.S},2000,createjs.Ease.linear()).call(Stop2);
	}	
	function Stop2(){
		COCCI.Animer(false);
		Etapes[0][0].couleur("bleu");
		Etapes[1][0].couleur("bleu");
		Etapes[BRANCHE-1][0].couleur("bleu");
		exo.chrono.start;
	}
	
	SPIDER=new Spider();
	SPIDER.fond.x=50;
    SPIDER.fond.y=350;
	SPIDER.Animer(true);
	stage.addChild(SPIDER.fond);
	
	// Texte de consigne
	var Rappel = disp.createTextLabel(exo.txt.rappel1+TABLE+exo.txt.rappel2);
    	Rappel.css({
        fontSize:16,
        border:"1px solid #FFCCCC",
		"background-color":"#fff",
		padding:5,
		top:390,
		left:5
    });
	 exo.blocAnimation.append(Rappel);
	
	
	
		
			
}

// fonction révélant les pastilles aux alentours
function Reveler(){
	var i,j;
	
	for(i=0;i<Etapes.length;i++){
			for(j=0;j<Etapes[0].length;j++){				
					Etapes[i][j].couleur("gris");				 
			}
	}
	
	if(POSITION[0]<Etapes.length-1){
		Etapes[POSITION[0]+1][POSITION[1]].couleur("bleu");	
		
	}
	if(POSITION[0]==Etapes.length-1){
		Etapes[0][POSITION[1]].couleur("bleu");	
		
	}
	if(POSITION[0]>0){
		Etapes[POSITION[0]-1][POSITION[1]].couleur("bleu");	
		
	}
	if(POSITION[0]==0){
		Etapes[Etapes.length-1][POSITION[1]].couleur("bleu");
			
	}
	if(POSITION[1]==0){
		Etapes[POSITION[0]][POSITION[1]+1].couleur("bleu");	
		
	}
	if(POSITION[1]>0 && POSITION[1]!=Etapes[0].length-1){
		
		Etapes[POSITION[0]][POSITION[1]-1].couleur("bleu");	
		Etapes[POSITION[0]][POSITION[1]+1].couleur("bleu");
		
	}
	if(POSITION[1]==Etapes[0].length-1){
		for(i=0;i<Etapes.length;i++){
			Etapes[i][POSITION[1]-1].couleur("bleu");
		}
		
	}
	
	for(i=0;i<Etapes.length;i++){
			for(j=0;j<Etapes[0].length;j++){
				switch(Etapes[i][j].Clip.N){
					case "bon":Etapes[i][j].couleur("vert");break;
					case "faux":Etapes[i][j].couleur("rouge");break;					
					}					 
				}
	}	
	
}

// fonction s'assurant qu'il n'y a pas de multiples adjacents à la pastille en x (branche), y (cercle)
function Proximite(x,y){
	var n=0;
	if(x>0 && x<Etapes.length-1){
		if(Etapes[x+1][y].Text.text!="-1"){n++}
		if(Etapes[x-1][y].Text.text!="-1"){n++}
	}
	if(x==0 && Etapes[x+1][y].Text.text!="-1"){n++}
	if(x==0 && Etapes[Etapes.length-1][y].Text.text!="-1"){n++}
	if(x==Etapes.length-1 && Etapes[0][y].Text.text!="-1"){n++}
	if(x==Etapes.length-1 && Etapes[x-1][y].Text.text!="-1"){n++}
	if(y>0 && y<Etapes[0].length-1){
		if(Etapes[x][y+1].Text.text!="-1"){n++}
		if(Etapes[x][y-1].Text.text!="-1"){n++}
	}
	if(y==0 && Etapes[x][y+1].Text.text!="-1"){n++}
	if(y==Etapes[0].length-1){
		for(var i=0;i<Etapes.length;i++){
			if(Etapes[i][y-1].Text.text!="-1"){n++}
		}
	}
	if(n>1){
		return(false)}
	return(true)
}

// fonction de création d'un chemin jusque la mouche
function Chemin(){
	var i,Current_B,Current_C,InCenter;
	var c=Etapes[0].length; // nombre de cercle+1
	var b=Etapes.length; //nombre de branches
	var r=Math.random()
	if(r<0.33){
	SOLUTION=[Etapes[0][0]];
	Current_B=0;
	Current_C=0;}
	if(r>=0.33 && r<0.66){
	SOLUTION=[Etapes[1][0]];
	Current_B=1;
	Current_C=0;
	}
	if(r>=0.66){
	SOLUTION=[Etapes[b-1][0]];
	Current_B=b-1;
	Current_C=0;
	}	
	Etapes[Current_B][Current_C].Text.text=MULTIPLE.pop()
	console.log(r);
	Current_C++;
	SOLUTION.push(Etapes[Current_B][Current_C])
	var count=0; // pour empêcher une boucle infinie en cas de chemin trop mal parti pour être finalisé.
	Etapes[Current_B][Current_C].Text.text=MULTIPLE.pop()	
	r=Math.random();	
	for(i=1;i<LONGUEUR;i++){
		if(Current_C<c-1)	{
			while(Etapes[Current_B][Current_C].Text.text!="-1" && count<100){
			count++;
			var s1=Current_B
			var s2=Current_C
			r=Math.random();
			if(r<0.25){Current_B--;if(Current_B<0){Current_B=b-1}
			}
			if(r>=0.25 && r<0.5){Current_B++;if(Current_B>b-1){Current_B=0}
			}
    		if(r>=0.5 && r<0.75 ){Current_C--;if(Current_C<0){Current_C++}
			}
			if(r>=0.75 ){Current_C++;}	
			if(Etapes[Current_B][Current_C].Text.text!="-1" || Proximite(Current_B,Current_C)==false || (Current_B==1 && Current_C==0) || (Current_B==b-1 && Current_C==0) || (Current_B==0 && Current_C==0) ){Current_B=s1;Current_C=s2;}	
			}	
		} else {		
			Current_B=Math.floor(Math.random()*b);
			Current_C--;
			while((Etapes[Current_B][Current_C].Text.text!="-1" || Proximite(Current_B,Current_C)==false) && count<100){count++;Current_B=Math.floor(Math.random()*b);}			
				}	
		if(count>=100){return(false)} 
			else {
				count=0;
				Etapes[Current_B][Current_C].Text.text=MULTIPLE.pop()	
				SOLUTION.push(Etapes[Current_B][Current_C]);	
				}	
	}
	
}
// Déclaration de l'objet Mouche,Cocci et Spider
function Mouche(scale){
	this.S=scale; // facteur de réduction
	var Go=false; // variable qui contrôle le fait d'être animé ou pas.
	var timer;
	this.fond=new createjs.Container(); 
	var movieClip=new createjs.Container();	// le clip animè
	var a=new createjs.Bitmap(exo.getURI("mo1"))	
	var b=new createjs.Bitmap(exo.getURI("mo2"))
	var c=new createjs.Bitmap(exo.getURI("mo3"))
	c.x=-3.25;
	a.visible=b.visible=c.visible=false;
	var image=new Array(a,b,a,c);
	var pos=0;
	movieClip.addChild(a);
	movieClip.addChild(b);
	movieClip.addChild(c);
	this.fond.addChild(movieClip);
	function Float(e){
			var currentDate = new Date();
			movieClip.y = (Math.cos(currentDate.getTime() * 0.01) * 20);
			if(Go==false){
				e.remove();
				createjs.Tween.get(movieClip).to({y:0},100,createjs.Ease.linear())
			}
	}
	Mouche.prototype.Animer=function(b){
		function NextFrame(){
		image[pos].visible=false;
		pos++;
		if(pos==4){pos=0}
		image[pos].visible=true;			
	    }		
		if(b==true){
			Go=true;
			timer=exo.setInterval(NextFrame,100) 
			var fn = createjs.Ticker.on("tick", Float, this);			
		}	else {
			Go=false;			
			}	
	}
}

function Cocci(scale){
	this.S=scale;
	var Go=false
	var timer;
	this.fond=new createjs.Container();	
	var movieClip=new createjs.Container();	
	var a=new createjs.Bitmap(exo.getURI("co1"))	
	var b=new createjs.Bitmap(exo.getURI("co2"))
	var c=new createjs.Bitmap(exo.getURI("co3"))
	a.x=c.x=1;
	a.visible=b.visible=c.visible=false;
	var image=new Array(b,a,c,a);
	var pos=0;
	movieClip.addChild(a);
	movieClip.addChild(b);
	movieClip.addChild(c);
	this.fond.addChild(movieClip);
	function Float(e){
			var currentDate = new Date();
			movieClip.y = (Math.cos(currentDate.getTime() * 0.01) * 20);
			if(Go==false){
				e.remove();
				createjs.Tween.get(movieClip).to({y:-20},100,createjs.Ease.linear())
			}
	}
	Cocci.prototype.Animer=function(b){
		function NextFrame(){
		image[pos].visible=false;
		pos++;
		if(pos==4){pos=0}
		image[pos].visible=true;			
	    }		
		if(b==true){
			Go=true;
			timer=exo.setInterval(NextFrame,100) 
			var fn = createjs.Ticker.on("tick", Float, this);			
		}
		else {
			Go=false;				
		}	
	}
}

function Spider(){	
	var Go=false
	var timer;
	this.fond=new createjs.Container();	
	var movieClip=new createjs.Container();	
	var a=new createjs.Bitmap(exo.getURI("sp1"))	
	var b=new createjs.Bitmap(exo.getURI("sp1"))
	var c=new createjs.Bitmap(exo.getURI("sp1"))
	a.regX=b.regX=c.regX=42
	a.regY=b.regY=c.regY=15
	b.rotation=5;
	c.rotation=-5;
	a.visible=b.visible=c.visible=false;
	var image=new Array(a,b,a,c);
	var pos=0;
	movieClip.addChild(a);
	movieClip.addChild(b);
	movieClip.addChild(c);
	this.fond.addChild(movieClip);	
	function Float(e){
			var currentDate = new Date();
				movieClip.y = (Math.cos(currentDate.getTime() * 0.001) * 20);
			if(Go==false){
				e.remove();
				//exo.clearInterval(timer)
				createjs.Tween.get(movieClip).to({y:0},100,createjs.Ease.linear())
			}
		}
	Spider.prototype.Animer=function(b){
		function NextFrame(){
		image[pos].visible=false;
		pos++;
		if(pos==4){pos=0}
		image[pos].visible=true;			
	    }		
		if(b==true){
			Go=true;
			timer=exo.setInterval(NextFrame,100) 
			var fn = createjs.Ticker.on("tick", Float, this);			
		}
		else {
			Go=false;				
		}	
	}
}


//  Création de l'objet Toile
function Toile(branche,ncercle){
	this.Clip=new createjs.Container();		
	Toile.prototype.dessine=function(t,c){
		var shape = new createjs.Shape();
		var n=Math.max(2,Math.min(ncercle,5));
		var m=Math.max(4,Math.min(branche,10));
		this.Clip.addChild(shape);
		shape.graphics.setStrokeStyle(4,"round").beginStroke("#000000");
		var i,j;
		var delta=2*Math.PI/m
		for(j=0;j<n;j++){
			var R=180-j*180/n
			for(i=0;i<m;i++){
				shape.graphics.moveTo(0,0).lineTo(R*Math.cos(i*delta),R*Math.sin(i*delta)).quadraticCurveTo(R*13/18*Math.cos(i*delta+delta/2),R*13/18*Math.sin(i*delta+delta/2),R*Math.cos((i+1)*delta),R*Math.sin((i+1)*delta))
				Etapes[i][j].dessine();			
				Etapes[i][j].couleur("gris");			
				Etapes[i][j].Clip.x=R*Math.cos(i*delta)
				Etapes[i][j].Clip.y=R*Math.sin(i*delta)			
				this.Clip.addChild(Etapes[i][j].Clip);
				}
			}		
		c.Clip.x=c.Clip.y=0;
		this.Clip.addChild(c.Clip);
	}
}

//  Créationb de l'objet Pastille
function Pastille(t,s){
	this.Clip=new createjs.Container();
	this.Rouge=new createjs.Bitmap(exo.getURI("bred"))	
	this.Bleu=new createjs.Bitmap(exo.getURI("bblue"))	
	this.Gris=new createjs.Bitmap(exo.getURI("bgrey"))		
	this.Vert=new createjs.Bitmap(exo.getURI("bgreen"))	
	this.Text= new createjs.Text(t, "16px Calibri", "#000000");
	
	Pastille.prototype.dessine=function(){
		this.Rouge.regX=this.Bleu.regX=this.Vert.regX=this.Gris.regX=11
		this.Rouge.regY=this.Bleu.regY=this.Vert.regY=this.Gris.regY=11
		this.Text.textBaseline = "alphabetic";
		this.Text.textAlign = "center";
		this.Text.mouseEnabled=false;
		var r=this.Text.getBounds();
		//this.Text.regX=r.width/2;
		this.Text.y=5;
		this.Text.x=1;
		this.Clip.N=t; // la valeur de la pastille 
		this.Clip.Pos=new Array(); // la position de la pastille sous la forme (branche,cercle)
		this.Clip.addChild(this.Rouge);
		this.Clip.addChild(this.Vert);
		this.Clip.addChild(this.Gris);
		this.Clip.addChild(this.Bleu);
		this.Clip.addChild(this.Text);	
		this.Clip.scaleX=this.Clip.scaleY=s;
	}
	//  Fonction changeant la couleur de la pastille
	Pastille.prototype.couleur=function(c){
		this.Rouge.visible=this.Bleu.visible=this.Vert.visible=this.Gris.visible=false;
		this.Clip.alpha=1;
		switch(c){
			case "rouge": this.Rouge.visible=true;break;
			case "bleu": this.Bleu.visible=true;break;
			case "vert": this.Vert.visible=true;break;
			case "gris": this.Gris.visible=true;this.Clip.alpha=1;break;
		}		
		if(c=="bleu"){
			this.Clip.addEventListener("click",onClick)
			this.Bleu.cursor="pointer";
			} else {
			this.Clip.removeEventListener("click",onClick)
		}
	}
	// Fonction faisant apparaître le texte de correction
	Pastille.prototype.Clic=function(type){
		var textL,temps,Y,bg;
		T.Clip.addChild(this.Clip);
		bg=new createjs.Shape();
		if(type=="error"){
			textL = new createjs.Text("ERREUR !", "20px Arial", "#FF0000");
			temps=500;
			Y=-60
		} else {			
			textL = new createjs.Text(type, "20px Arial", "#00CC00");
			temps=1500;
			Y=-80
			bg.graphics.beginFill("#E5E5E5").drawRect(0,0,textL.getBounds().width,20);
			bg.alpha=0;
			bg.x=-textL.getBounds().width/2;
			bg.y=-40;
			}
		textL.x=-textL.getBounds().width/2;	
		textL.y=-40
		textL.alpha=0;		
		function vanish(){			
			createjs.Tween.get(textL).wait(2000).to({y:Y/3,alpha:0},temps,createjs.Ease.linear())
			createjs.Tween.get(bg).wait(2000).to({y:Y/3,alpha:0},temps,createjs.Ease.linear())
			}
		createjs.Tween.get(textL).to({y:Y/3,alpha:1},200,createjs.Ease.linear()).call(vanish);	
		createjs.Tween.get(bg).to({y:Y/3,alpha:1},200,createjs.Ease.linear());			
		this.Clip.addChild(bg);
		this.Clip.addChild(textL);
	}	
}

// Gestion du click sur une pastille
function onClick(e){
	e.target.parent.removeEventListener("click",onClick)
	POSITION=e.target.parent.Pos;
	T.Clip.addChild(e.target.parent); //on fait passer la pastille au premier plan
	var Old_X=COCCI.fond.x; // on conserve la position initiale de COCCI
	var Old_Y=COCCI.fond.y;
	//On déplace COCCI sur la pastille
	var n=Math.max(2,Math.min(NCERCLE,5));
	var m=Math.max(4,Math.min(BRANCHE,10));	
	var delta=2*Math.PI/m		
	var R=190-(POSITION[1])*190/n
	var X=R*Math.cos(POSITION[0]*delta)+T.Clip.x
	var Y=R*Math.sin(POSITION[0]*delta)+T.Clip.y	
	createjs.Tween.get(COCCI.fond).to({x:X,y:Y},1000,createjs.Ease.linear())
	exo.setTimeout(Next,1100);
	//Quand elle est arrivée, on teste si c'est la bonne
	function Next(){	
	if(e.target.parent.N%TABLE!=0){
	Etapes[POSITION[0]][POSITION[1]].couleur("rouge");
	Etapes[POSITION[0]][POSITION[1]].Clic("error");
	e.target.parent.N="faux"
	WIN=false;	
	} else {	
		WIN=true;
			}
	// On allume indépendamment de cela la pastille solution, qui devient la destination finale de COCCI
	SOLUTION[pos].couleur("vert");
	SOLUTION[pos].Clic(SOLUTION[pos].Clip.N/TABLE+"x"+TABLE);
	SOLUTION[pos].Clip.N="bon";
	POSITION=SOLUTION[pos].Clip.Pos;
	pos++;
	// La question est finie ?
	if(pos==SOLUTION.length){
		
		for(i=0;i<Etapes.length;i++){
			for(j=0;j<Etapes[0].length;j++){
				switch(Etapes[i][j].Clip.N){
					case "bon":Etapes[i][j].couleur("vert");break;
					case "faux":Etapes[i][j].couleur("rouge");break;
					default: Etapes[i][j].couleur("gris");break;
					
				}					 
			}
	     }
		 // Si COCCI n'a pas trop fait d'erreurs et si elle ne s'est pas trompée au dernier déplacement:
		 if(Brep/exo.indiceTentative>=0 && WIN==true){ 
		      createjs.Tween.get(COCCI.fond).to({y:-100},3000,createjs.Ease.linear())
		      createjs.Tween.get(MOUCHE.fond).to({y:-100},3000,createjs.Ease.linear());
			  exo.setTimeout(Next3,3100);}
		    else {  // sinon, c'est l'araignée qui gagne.
			   SPIDER.Animer(false);
			   createjs.Tween.get(SPIDER.fond).to({x:MOUCHE.fond.x,y:MOUCHE.fond.y},2000,createjs.Ease.linear());
			   exo.setTimeout(Next4,2100);		     				
			}		
	} else{
	// La question n'est pas finie
	 if(WIN==false){ // en cas d'erreur l'araignée se rapproche	
	 createjs.Tween.get(COCCI.fond).to({x:Old_X,y:Old_Y},1000,createjs.Ease.linear()).call(Corriger);
	 var sx=SPIDER.fond.x+(MOUCHE.fond.x-SPIDER.fond.x)/2.5;
	 var sy=SPIDER.fond.y+(MOUCHE.fond.y-SPIDER.fond.y)/2.5;;
	 createjs.Tween.get(SPIDER.fond).to({x:sx,y:sy},1000,createjs.Ease.linear())
	 				}
	    else {Brep++;Next2();}
	     }
	}
	function Corriger(){
	 R=190-(POSITION[1])*190/n 
	 X=R*Math.cos(POSITION[0]*delta)+T.Clip.x
	 Y=R*Math.sin(POSITION[0]*delta)+T.Clip.y	
	 createjs.Tween.get(COCCI.fond).to({x:X,y:Y},1000,createjs.Ease.linear());
	 exo.setTimeout(Next2,1100);
	 }
	function Next2(){Reveler();exo.poursuivreQuestion();}
	function Next3(){exo.poursuivreQuestion();}
	function Next4(){
		createjs.Tween.get(SPIDER.fond).to({x:-100},2000,createjs.Ease.linear());
		createjs.Tween.get(MOUCHE.fond).to({x:-100},2000,createjs.Ease.linear());
		exo.setTimeout(Next3,2000);
	}	
}

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {	

	if(WIN){return "juste"}else {
		return("faux");
	}
	    
};

// Correction (peut rester vide)
exo.corriger = function() {
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;

    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);  
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:60,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:60,
        taille:3,
        nom:"table",
        texte:exo.txt.opt4
    });
    exo.blocParametre.append(controle);
     //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:60,
        taille:3,
        nom:"totalTentative",
        texte:exo.txt.opt2
    });
    exo.blocParametre.append(controle);
     //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:60,
        taille:3,
        nom:"branche",
        texte:exo.txt.opt5
    });
    exo.blocParametre.append(controle);
     //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:60,
        taille:3,
        nom:"cercle",
        texte:exo.txt.opt6
    });
    exo.blocParametre.append(controle);
     //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:60,
        taille:3,
        nom:"maximum",
        texte:exo.txt.opt7
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));