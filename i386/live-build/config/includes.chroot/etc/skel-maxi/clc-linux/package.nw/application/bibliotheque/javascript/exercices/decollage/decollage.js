var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.decollage = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
var champReponse1, champReponse2, soluceH, soluceM, monAvion;
var conteneur,repereV,choixV,dureeV;

// Référencer les ressources de l'exercice (textes, image, son)
exo.oRessources = { 
    txt : "decollage/textes/decollage_fr.json",
    carte : "decollage/images/france.png",
    illustration : "decollage/images/illustration.png",
    avion : "decollage/images/avion.png"
};
//
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0,
        typeQ:1
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};
//
exo.creerDonnees = function() {
    repereV = [["LILLE",180,3],["PARIS",170,70],["NANTES",60,130],["LYON",235,195],["STRASBOURG",300,80],["BORDEAUX",90,220],["MARSEILLE",245,280]];
    //duree [première ville][deuxième ville][durée heure,durée minute],
    choixV = [[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],
                  [1,2],[1,3],[1,4],[1,5],[1,6],
                  [2,3],[2,4],[2,5],[2,6],
                  [3,4],[3,5],[3,6],
                  [4,5],[4,6],
                  [5,6]];
    //choixV.sort(function(){return Math.floor(Math.random()*3)-1});
    util.shuffleArray(choixV);
    dureeV = [[[0,0],[0,50],[1,10],[1,20],[1,12],[1,25],[1,30]],
                  [[0,50],[0,0],[0,55],[1,5],[1,2],[1,10],[1,25]],
                  [[1,10],[0,55],[0,0],[1,5],[1,27],[0,45],[1,25]],
                  [[1,20],[1,5],[1,5],[0,0],[1,7],[1,23],[0,50]],
                  [[1,12],[1,2],[1,27],[1,7],[0,0],[1,35],[1,28]],
                  [[1,25],[1,10],[0,45],[1,23],[1,35],[0,0],[1,15]],
                  [[1,30],[1,25],[1,25],[0,50],[1,28],[1,15],[0,0]]];
};
//
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};
//
exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
    exo.btnValider.hide();
    exo.keyboard.config({
		numeric   : "enabled",
		large     : "disabled",
		arrow     : "disabled"
    });
    exo.blocInfo.css({left:400});
    var nbDepart = choixV[q][0];
    var nbArrivee = choixV[q][1];
    //donnees horaires-durees
    //dureeH heure
    var dureeH = dureeV[nbDepart][nbArrivee][0];
    //dureeM minutes
    var dureeM = dureeV[nbDepart][nbArrivee][1];
    //
    var heureDepartH = 0;
    var heureDepartM = 0;
    var heureArriveeH = 0;
    var heureArriveeM = 0;
    //decor
    var macarte = disp.createImageSprite(exo,"carte");
    macarte.css({
	    left:20,
	    top:20
    });
    exo.blocAnimation.append(macarte);
    for (i=0;i<7;i++) {
        //creation des noms des villes
        var label = disp.createTextLabel(repereV[i][0]);
        if (i==4) {
            label.css({left:repereV[i][1]-30,top:repereV[i][2]+38,fontSize:16,color:"#333",textAlign:"right"});
        } else {
            label.css({left:repereV[i][1],top:repereV[i][2]+38,fontSize:16,color:"#333",textAlign:"right"});
        }
        exo.blocAnimation.append(label);
        /*var label = cnt.paper.text(repereV[i][0]);
        label.move(repereV[i][1],repereV[i][2]+12);
	label.attr({"font-size":14,"text-anchor":"middle","fill":"#222"});*/
    }
    /*var zoneDepart = cnt.paper.rect(300,50).attr({"rx":6,"ry":6});
    zoneDepart.move(400,55).fill("#DDDDDD");
    var zoneArrivee = cnt.paper.rect(300,50).attr({"rx":6,"ry":6});
    zoneArrivee.move(400,145).fill("#DDDDDD");
    var zoneDuree = cnt.paper.rect(300,50).attr({"rx":6,"ry":6});
    zoneDuree.move(400,235).fill("#DDDDDD");*/
    var dx = repereV[nbArrivee][1]-repereV[nbDepart][1];
    var dy = repereV[nbArrivee][2]-repereV[nbDepart][2];
    var angleDeg = Math.atan2(dy,dx)*180/Math.PI;
    monAvion = disp.createImageSprite(exo,"avion");
    exo.blocAnimation.append(monAvion);
    monAvion.css({left:repereV[nbDepart][1]+15,top:repereV[nbDepart][2]+10});
    monAvion.transition({rotate:angleDeg},3000,"linear").transition({left:repereV[nbArrivee][1]+15,top:repereV[nbArrivee][2]},3000,"linear");
    //zones de texte
    var text0 = repereV[nbDepart][0]+" - "+repereV[nbArrivee][0];
    var etiqTitre = disp.createTextLabel(text0);
    exo.blocAnimation.append(etiqTitre);
    etiqTitre.css({
            fontSize:'18',
            fontWeight:'bold',
            padding:10,
            color:'#FFF',
            backgroundColor:'#123456'
    });
    etiqTitre.position({
            my:"center top",
            at:"center+210 top+17",
            of:exo.blocAnimation
    });
    var text1 = "";
    var text2 = "";
    var etiqDepart,etiqArrivee;
    if (exo.options.typeQ == 1) {
        heureDepartH = Math.floor(6+15*Math.random());
        if (q===0) {
            heureDepartM = 0;
        } else if (q==1 || q==2) {
            heureDepartM = 10*Math.floor(1+2*Math.random())+Math.floor(1+8*Math.random());
        } else if (q==3) {
            heureDepartM = 10*Math.floor(3+2*Math.random())+Math.floor(1+8*Math.random());
        } else {
            heureDepartM = 10*Math.floor(4+2*Math.random())+Math.floor(1+8*Math.random());
        } 
        if (heureDepartM+dureeM<60) {
            heureArriveeH = heureDepartH+dureeH;
            heureArriveeM = heureDepartM+dureeM;
        } else {
            heureArriveeH = heureDepartH+dureeH+1;
            heureArriveeM = heureDepartM+dureeM-60;
        }
        soluceH = dureeH;
        soluceM = dureeM;
        //
        text1 = "DEPART : "+heureDepartH+" H "+heureDepartM;
        if (heureDepartM<10) {
            text1 = "DEPART : "+heureDepartH+" H 0"+heureDepartM;
        }
        text2 = "ARRIVEE : "+heureArriveeH+" H "+heureArriveeM;
        if (heureArriveeM<10) {
           text2 = "ARRIVEE : "+heureArriveeH+" H 0"+heureArriveeM;
        }
        etiqDepart = disp.createTextLabel(text1);
        exo.blocAnimation.append(etiqDepart);
        etiqDepart.css({
            width:220,
            textAlign:'center',
            fontSize:24,
            fontWeight:'bold',
            color:'#000',
            padding:5,
            border:"1px solid #ccc"
        });
        etiqDepart.position({
            my:"center top",
            at:"center+210 top+97",
            of:exo.blocAnimation
        });
        etiqArrivee = disp.createTextLabel(text2);
        exo.blocAnimation.append(etiqArrivee);
        etiqArrivee.css({
            width:220,
            textAlign:'center',
            fontSize:24,
            fontWeight:'bold',
            color:'#000',
            padding:5,
            border:"1px solid #ccc"
        });
        etiqArrivee.position({
            my:"center top",
            at:"center+210 top+177",
            of:exo.blocAnimation
        });
        etiquetteQuestion = disp.createTextLabel("DUREE :");
        exo.blocAnimation.append(etiquetteQuestion);
        etiquetteQuestion.position({
            my:'center top',
            at:'center+136 top+267',
            of:exo.blocAnimation
        });
        etiquetteQuestion.css({
            fontSize:24,
            fontWeight:'bold',
            color:'#000'
        });
        var animSuite1 = anim.creerDemon({},fSuite1,6000,1);
        animSuite1.start();
        //Avancer sur le signe "+"
    }
    else if (exo.options.typeQ == 2) {//trouver l'heure d'arrivée
        heureDepartH = Math.floor(6+15*Math.random());
        //heureDepartM = 10*Math.floor(3*Math.random())+Math.floor(1+8*Math.random());
        if (q===0) {
            heureDepartM = 0;
        } else if (q==1 || q==2) {
            heureDepartM = 10*Math.floor(1+2*Math.random());
        } else {
            heureDepartM = 10*Math.floor(3+2*Math.random())+Math.floor(1+8*Math.random());
        } 
        if (heureDepartM+dureeM<60) {
            heureArriveeH = heureDepartH+dureeH;
            heureArriveeM = heureDepartM+dureeM;
        } else {
            heureArriveeH = heureDepartH+dureeH+1;
            heureArriveeM = heureDepartM+dureeM-60;
        }
        if (heureDepartM+dureeM<60) {
            heureArriveeH = heureDepartH+dureeH;
            heureArriveeM = heureDepartM+dureeM;
        } else {
            heureArriveeH = heureDepartH+dureeH+1;
            heureArriveeM = heureDepartM+dureeM-60;
        }
        soluceH = heureArriveeH;
        soluceM = heureArriveeM;
        //
        text1 = "DEPART : "+heureDepartH+" H "+heureDepartM;
        if (heureDepartM<10) {
            text1 = "DEPART : "+heureDepartH+" H 0"+heureDepartM;
        }
        text2 = "DUREE : "+dureeH+" H "+dureeM;
        if (dureeM<10) {
           text2 = "DUREE : "+dureeH+" H 0"+dureeM;
        }
        etiqDepart = disp.createTextLabel(text1);
        exo.blocAnimation.append(etiqDepart);
        etiqDepart.css({
            fontSize:'30',
            fontWeight:'bold',
            color:'#000'
        });
        etiqDepart.position({
            my:"center top",
            at:"center+210 top+87",
            of:exo.blocAnimation
        });
        etiqArrivee = disp.createTextLabel(text2);
            exo.blocAnimation.append(etiqArrivee);
            etiqArrivee.css({
                fontSize:'30',
                fontWeight:'bold',
                color:'#000'
            });
            etiqArrivee.position({
                my:"center top",
                at:"center+210 top+267",
                of:exo.blocAnimation
            });
            etiquetteQuestion = disp.createTextLabel("ARRIVEE :");
            exo.blocAnimation.append(etiquetteQuestion);
            etiquetteQuestion.position({
                my:'center top',
                at:'center+120 top+177',
                of:exo.blocAnimation
            });
            etiquetteQuestion.css({
                fontSize:'30',
                fontWeight:'bold',
                color:'#000'
            });
        var animSuite2 = anim.creerDemon({},fSuite2,6000,1);
        animSuite2.start();
        //Avancer sur le signe "+"
    }
    else if (exo.options.typeQ == 3) {//trouver l'heure de depart
        heureDepartH = Math.floor(6+15*Math.random());
        if (q===0) {
            heureDepartM = 0;
        } else if (q==1 || q==2) {
            heureDepartM = 10*Math.floor(1+2*Math.random());
        } else {
            heureDepartM = 10*Math.floor(3+2*Math.random())+Math.floor(1+8*Math.random());
        } 
        if (heureDepartM+dureeM<60) {
            heureArriveeH = heureDepartH+dureeH;
            heureArriveeM = heureDepartM+dureeM;
        } else {
            heureArriveeH = heureDepartH+dureeH+1;
            heureArriveeM = heureDepartM+dureeM-60;
        }
        soluceH = heureDepartH;
        soluceM = heureDepartM;
        //
        text1 = "ARRIVEE : "+heureArriveeH+" H "+heureArriveeM;
        if (heureArriveeM<10) {
        text1 = "ARRIVEE : "+heureArriveeH+" H 0"+heureArriveeM;
        }
        text2 = "DUREE : "+dureeH+" H "+dureeM;
        if (dureeM<10) {
           text2 = "DUREE : "+dureeH+" H 0"+dureeM;
        }
        etiqDepart = disp.createTextLabel(text1);
        exo.blocAnimation.append(etiqDepart);
        etiqDepart.css({
            fontSize:'30',
            fontWeight:'bold',
            color:'#000'
        });
        etiqDepart.position({
            my:"center top",
            at:"center+210 top+177",
            of:exo.blocAnimation
        });
        etiqArrivee = disp.createTextLabel(text2);
        exo.blocAnimation.append(etiqArrivee);
        etiqArrivee.css({
            fontSize:'30',
            fontWeight:'bold',
            color:'#000'
        });
        etiqArrivee.position({
            my:"center top",
            at:"center+210 top+267",
            of:exo.blocAnimation
        });
        etiquetteQuestion = disp.createTextLabel("DEPART :");
        exo.blocAnimation.append(etiquetteQuestion);
        etiquetteQuestion.position({
            my:'center top',
            at:'center+130 top+87',
            of:exo.blocAnimation
        });
        etiquetteQuestion.css({
            fontSize:'30',
            fontWeight:'bold',
            color:'#000'
        });
        var animSuite3 = anim.creerDemon({},fSuite3,6000,1);
        animSuite3.start();
        //Avancer sur le signe "+"
    }
    function fSuite1() {
        champReponse1 = disp.createTextField(exo,2);
        champReponse2 = disp.createTextField(exo,2);
        exo.blocAnimation.append(champReponse1);
        exo.blocAnimation.append(champReponse2);
        champReponse1.css({
            width:48,
        });
        champReponse2.css({
            width:48,
        });
        champReponse1.position({
            my:'center top',
            at:'center+240 top+260',
            of:exo.blocAnimation
        });
        champReponse1.focus();
        champReponse1.on("touchstart",function(e){
            champReponse1.focus();
        });
        symboleH = disp.createTextLabel("H");
        exo.blocAnimation.append(symboleH);
        symboleH.position({
            my:'center top',
            at:'center+284 top+267',
            of:exo.blocAnimation
        });
        symboleH.css({
            fontSize:'30',
            fontWeight:'bold',
            color:'#000'
        });
        champReponse2.position({
            my:'center top',
            at:'center+329 top+262',
            of:exo.blocAnimation
        });
        champReponse2.on("touchstart",function(e){
            champReponse2.focus();
        });
        if (exo.tabletSupport) {
			exo.keyboard.config({
				numeric : "enabled",
				float : "enabled",
				delete : "enabled",
				large : "enabled"
			});
		} else {
			exo.btnValider.show();
		}
    }
    function fSuite2() {
        champReponse1 = disp.createTextField(exo,2);
        champReponse2 = disp.createTextField(exo,2);
        exo.blocAnimation.append(champReponse1);
        exo.blocAnimation.append(champReponse2);
        champReponse1.css({
            width:48,
        });
        champReponse2.css({
            width:48,
        });
        champReponse1.position({
            my:'center top',
            at:'center+246 top+172',
            of:exo.blocAnimation
        });
        champReponse1.focus();
        champReponse1.on("touchstart",function(e){
            champReponse1.focus();
        });
        symboleH = disp.createTextLabel("H");
        exo.blocAnimation.append(symboleH);
        symboleH.position({
            my:'center top',
            at:'center+285 top+177',
            of:exo.blocAnimation
        });
        symboleH.css({
            fontSize:'30',
            fontWeight:'bold',
            color:'#000'
        });
        champReponse2.position({
            my:'center top',
            at:'center+325 top+172',
            of:exo.blocAnimation
        });
        champReponse2.on("touchstart",function(e){
            champReponse2.focus();
        });
        if (exo.tabletSupport) {
			exo.keyboard.config({
				numeric : "enabled",
				float : "enabled",
				delete : "enabled",
				large : "enabled"
			});
		} else {
			exo.btnValider.show();
		}
    }
    function fSuite3() {
        champReponse1 = disp.createTextField(exo,2);
        champReponse2 = disp.createTextField(exo,2);
        champReponse1.css({
            width:48,
        });
        champReponse2.css({
            width:48,
        });
        exo.blocAnimation.append(champReponse1);
        exo.blocAnimation.append(champReponse2);
        champReponse1.position({
            my:'center top',
            at:'center+245 top+82',
            of:exo.blocAnimation
        });
        champReponse1.focus();
        champReponse1.on("touchstart",function(e){
            champReponse1.focus();
        });
        symboleH = disp.createTextLabel("H");
        exo.blocAnimation.append(symboleH);
        symboleH.position({
            my:'center top',
            at:'center+288 top+87',
            of:exo.blocAnimation
        });
        symboleH.css({
            fontSize:'30',
            fontWeight:'bold',
            color:'#000'
        });
        champReponse2.position({
            my:'center top',
            at:'center+330 top+82',
            of:exo.blocAnimation
        });
        champReponse2.on("touchstart",function(e){
            champReponse2.focus();
        });
        if (exo.tabletSupport) {
			exo.keyboard.config({
				numeric : "enabled",
				float : "enabled",
				delete : "enabled",
				large : "enabled"
			});
		} else {
			exo.btnValider.show();
		}
    }
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    var repH = Number(champReponse1.val());
    var repM = Number(champReponse2.val());
    if (champReponse1.val()==="" && champReponse2.val()==="") {
	    return "rien";
    }
    if (repH == soluceH && repM == soluceM){
        monAvion.transition({scale:2},500,"linear").transition({scale:1},500,"linear");
	   return "juste";
    } else {
	   return "faux";
    }
};

// Correction (peut rester vide)
exo.corriger = function() {
    if (soluceH === 0) {
        soluceH = ""+0;
    }
    if (soluceM<10) {
        soluceM = "0"+soluceM;
    }
    var correction1 = disp.createCorrectionLabel(soluceH);
    var correction2 = disp.createCorrectionLabel(soluceM);
    exo.blocAnimation.append(correction1);
    correction1.position({
        my:"center center",
        at:"center center+30",
        of:champReponse1,
    });
    var barre1 = disp.drawBar(champReponse1);
    exo.blocAnimation.append(barre1);
    exo.blocAnimation.append(correction2);
    correction2.position({
        my:"center center",
        at:"center center+30",
        of:champReponse2,
    });
    var barre2 = disp.drawBar(champReponse2);
    exo.blocAnimation.append(barre2);
    monAvion.transition({rotate:800},1000,"linear").transition({scale:3},500,"linear").transition({opacity:0,scale:0.5},500,"linear");
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:3,
        nom:"typeQ",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[1,2,3]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));