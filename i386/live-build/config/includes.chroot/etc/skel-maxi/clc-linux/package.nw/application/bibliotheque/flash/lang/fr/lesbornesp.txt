//Titre de l'exo et consigne générale
p1_1 =La borne
p1_2 =Place la voiture à la bonne borne.
//La question
p2_1 =La voiture se déplace à la vitesse de $var$ km/h. Clique sur la borne où se trouve la voiture au bout de $var$ minutes.
p2_2 =La voiture se déplace à la vitesse de $var$ km/h. Clique sur la borne où se trouve la voiture au bout de $var$ heures.
p2_3 =La voiture se déplace à la vitesse de $var$ km/h. Clique sur la borne où se trouve la voiture au bout de $var$ h $var$ min.
