proc getid {} {
global indexlibre
incr indexlibre
set idt uident$indexlibre
return $idt
}


proc getimgid {} {
global imgid
incr imgid
set idt kimage$imgid
return $idt
}



proc sauvepage {c ind type} {
global indexpage listdata tid tabpage tabson lsauve canvw canvh tabobj tabevent
set lsauve {}
set oldpage $indexpage
set indexpage $ind

#enumeration des objets de la page
foreach it [$c find all] {
if {[lsearch -regexp [$c gettags $it] uident*] !=-1} {
set ident [lindex [$c gettags $it] [lsearch -regexp [$c gettags $it] uident*]]
set typ [lindex [$c gettags $it] [lsearch -regexp [$c gettags $it] type*]]

set lbox [$c bbox $it]
#preparation du code de sauvegarde de chaque objet
    switch $typ {

	typepolygon {
	set arrfill [$c itemcget $it -fill]
	if {$arrfill ==""} {
	set arrfill \"\"
	}
	set sm [$c itemcget $it -smooth]
	if {$sm=="bezier"} {
	set sm 1
	} else {
	set sm 0}
	lappend lsauve "createpolygon \$c \"[$c coords $it]\" $arrfill [$c itemcget $it -outline] $sm $ident"
		if {$type == "symv"} {
		set listemp [$c coords $it] 
			for {set i 0} {$i < [llength $listemp]} {incr i 2} { 
			set listemp [lreplace $listemp $i $i [expr $canvw - [lindex $listemp $i]]]
			}
		lappend lsauve "createpolygon \$cc \"$listemp\" white [$c itemcget $it -outline] $sm $ident"
		}

		if {$type == "symh"} {
		set listemp [$c coords $it] 
			for {set i 1} {$i < [llength $listemp]} {incr i 2} { 
			set listemp [lreplace $listemp $i $i [expr $canvh - [lindex $listemp $i]]]
			}
		lappend lsauve "createpolygon \$cc \"$listemp\" white [$c itemcget $it -outline] $sm $ident"
		}

	}

}
lappend lsauve "set tabobj($ident) $tabobj($ident)" 
}
}
#lappend lsauve "\$c configure -background [$c cget -background] -width $canvw -height $canvh"
set listdata [lreplace $listdata $oldpage $oldpage $lsauve]
}

proc delitem {c} {
global tid tabpage tabobj tabevent
catch {$c delete enveloppe}
foreach it [$c find withtag cible] {
set id [lindex [$c gettags $it] [lsearch -regexp [$c gettags $it] uident*]]
# on supprime la r�f�rence � l'objet � effacer du tableau tabevent (remplac� par objet?)
	foreach item [array names tabevent] {
	set tabevent($item) [string map "{$tabobj($id)} {Objet?}" $tabevent($item)]
	}
#on supprime les entr�es des tables tabson et tabevent
catch {unset tabobj($id) }

#destruction de l'objet proprement dit, pour un bouton, suppression de l'entr�e dans tabpage
set typ [lindex [$c gettags $it] [lsearch -regexp [$c gettags $it] type*]]

array unset  tid($id)
$c delete $id
catch {destroy .waction}
}
}


proc sauve {c type} {
global tid indexpage listdata nomf indexlibre Home nompage Home_data baseHome

sauvepage $c $indexpage $type

set listdata [lreplace $listdata end end "set indexlibre $indexlibre ; array set nompage \173[array get nompage]\175"]


if {$nomf =="" || $type == "new"} {
set types {    {{Fichiers kdb}            {.kdb}        }}
set ext .kdb
catch {set nomf [tk_getSaveFile -filetypes $types -initialdir $Home_data -title "Symcolor"]}
    if {$nomf != ""} {
        if  {[string match -nocase *.kdb $nomf]==0} {
        set nomf $nomf$ext
        }
 }
 }
if {$type =="symv" || $type=="symh"} {
set direct [file join $baseHome]
} else {
set direct [file join $Home]
}
if {$nomf !=""} {
set f [open [file join $direct $nomf] "w"]
puts $f $listdata
close $f
 wm title . $nomf
}
}


proc nouveau {c} {
global tid indexpage totalpage listdata indexlibre nomf tabpage canvw canvh tabobj tabevent nompage
catch {unset tabobj}


$c delete all
creetool canvas $c
catch {unset tid}

set indexpage 0
set indexlibre 0
set nomf ""
set listdata {{} {set indexlibre $indexlibre}}
wm title . "Symcolor"
}

proc ouvre {c imge} {
global tid indexpage totalpage listdata nomf indexlibre tabpage tabson Home canvh canvw tabobj tabevent nompage Home_data
set nomf [file join $Home_data $imge]
set listdata {}
set indexpage 0
set totalpage 1
set indexlibre 0

$c delete all
catch {unset tid}
catch {unset tabobj}
catch {unset tabevent}
catch {unset nompage}
set tabevent(canvas) {pageenter {} pageleave {}}

set f [open [file join $nomf] "r"]
set listdata [gets $f]
close $f
set totalpage [expr [llength $listdata] -1]
for {set i 0} {$i < $totalpage} {incr i} {
set nompage($i) "[mc {Page}][expr $i +1]"
}


foreach it [lindex $listdata 0] {
eval $it
}
#evaluer les noms des pages
#evaluation del adonn�e concernant indexlibre
eval [lindex $listdata end]
set canvw [$c cget -width]
set canvh [$c cget -height]
$c dtag all cible
$c delete enveloppe
$c focus ""

}


proc charge {c} {
global tid indexpage totalpage listdata nomf indexlibre tabpage tabson Home canvh canvw tabobj tabevent nompage Home_data

set types {    {{Fichiers kdb}            {.kdb}        }}
set ext .kdb
catch {set nomf [tk_getOpenFile -filetypes $types -initialdir $Home_data -title "Symcolor"]}
    if {$nomf != ""} {
        if  {[string match -nocase *.kdb $nomf]==0} {
        set nomf $nomf$ext
        }
set listdata {}
set indexpage 0
set totalpage 1
set indexlibre 0

$c delete all
catch {unset tid}
catch {unset tabobj}
catch {unset tabevent}
catch {unset nompage}
set tabevent(canvas) {pageenter {} pageleave {}}

set f [open [file join $Home $nomf] "r"]
set listdata [gets $f]
close $f
set totalpage [expr [llength $listdata] -1]
for {set i 0} {$i < $totalpage} {incr i} {
set nompage($i) "[mc {Page}][expr $i +1]"
}


foreach it [lindex $listdata 0] {
eval $it
}
#evaluer les noms des pages
#evaluation del adonn�e concernant indexlibre
eval [lindex $listdata end]
set canvw [$c cget -width]
set canvh [$c cget -height]
$c dtag all cible
$c delete enveloppe
$c focus ""

}
}

proc devant {c} {
global tid
set typ [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] type*]]
switch $typ {
typetext {
set id [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
catch {$c raise [expr [lindex $tid($id) 0]]}
catch {$c raise cible}
}

typeimage {
catch {$c raise cible}
}
typepolygon {
catch {$c raise cible}
}
typebouton1 {
catch {$c raise cible}
}
typebouton2 {
set id [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
catch {$c raise cible}
catch {$c raise [expr [lindex $tid($id) 4]]}

}
}
}

proc derriere {c} {
global tid
set typ [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] type*]]
switch $typ {
typetext {
set id [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
#catch {$c lower [expr $tid($id)]}
catch {$c lower cible}
catch {$c lower [expr [lindex $tid($id) 0]]}

}
typeimage {
catch {$c lower cible}
}
typepolygon {
catch {$c lower cible}
}

typebouton1 {
catch {$c lower cible}
}
typebouton2 {
set id [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
catch {$c lower [expr [lindex $tid($id) 4]]}
catch {$c lower cible}

}
}
}

proc imprime {c} {
if {[info exists env(PRINTER)]} {
    set psCmd "lpr -P$env(PRINTER)"
} else {
    set psCmd "lpr"
}

set postscriptOpts {-pageheight 190m -pagewidth 280m -pageanchor c}

if {[catch {eval exec $psCmd <<    \
		  {[eval $c postscript $postscriptOpts]}} msg]} {
		    tk_messageBox -message  \
		      "Error when printing: $msg" -icon error -type ok
		}


}

proc changetaillecanvas {c} {
global canvw canvh
variable irep
switch $irep {
	0 {
	set canvw 570
	set canvh 380
	}
	1 {
	set canvw 730
	set canvh 490

	}
	2 {
	set canvw 954
	set canvh 668
	}

}
$c configure -width $canvw -height $canvh
update
#destruction de la boite de s�lection de taille
catch {destroy .taillebox}
wm title . [mc {Symcolor - La symetrie en couleur}]
}

########################################################################################"
# Code import� de "The Coccinella" by Mats Bengtsson
#######################################################################
#--- Polygon tool procedures ---------------------------------------------------

# CanvasDraw::PolySetPoint --
#
#       Polygon drawing routines.
#   
# Arguments:
#       w      the canvas widget.
#       x,y    the mouse coordinates.
#       
# Results:
#       none

proc PolySetPoint {w x y} {
    
    variable thePoly

    if {![info exists thePoly(0)]} {
	
	# First point.
	catch {unset thePoly}
	set thePoly(N) 0
	set thePoly(0) [list $x $y]
    } elseif {[expr   \
      hypot([lindex $thePoly(0) 0] - $x, [lindex $thePoly(0) 1] - $y)] < 6} {
	
	# If this point close enough to 'thePoly(0)', close polygon.
	PolyDrag $w [lindex $thePoly(0) 0] [lindex $thePoly(0) 1]
	set thePoly(last) {}
	incr thePoly(N)
	set thePoly($thePoly(N)) $thePoly(0)
	FinalizePoly $w [lindex $thePoly(0) 0] [lindex $thePoly(0) 1]
	return
    } else {
	set thePoly(last) {}
	incr thePoly(N)
	set thePoly($thePoly(N)) $thePoly(xy)
    }
    
    # Let the latest line segment follow the mouse movements.
    focus $w
    bind $w <Motion> {PolyDrag %W [%W canvasx %x] [%W canvasy %y]}
    bind $w <Shift-Motion> {PolyDrag %W [%W canvasx %x] [%W canvasy %y] 1}
    bind $w <KeyPress-space> {FinalizePoly %W [%W canvasx %x] [%W canvasy %y]}
}               

# CanvasDraw::PolyDrag --
#
#       Polygon drawing routines.
#   
# Arguments:
#       w      the canvas widget.
#       x,y    the mouse coordinates.
#       shift  constrain.
#       
# Results:
#       none

proc PolyDrag {w x y {shift 0}} {
    global  prefs

    variable thePoly
#d    set wtop [::UI::GetToplevelNS $w]
#d    upvar ::${wtop}::state state

    # Move one end point of the latest line segment of the polygon.
    # If anchor not set just return.
    if {![info exists thePoly(0)]} {
	return
    }
    catch {$w delete $thePoly(last)}
    
    # Vertical or horizontal.
    if {$shift} {
	#set anch $thePoly($thePoly(N))
	#set newco [ConstrainedDrag $x $y [lindex $anch 0] [lindex $anch 1]]
	#foreach {x y} $newco {}
    }
#d    if {$prefs(hasDash)} {
#d	set extras [list -dash $state(dash)]
#d    } else {
	set extras ""
#d    }

#++
    set state(fgCol) black
    set state(penThick) 1
#++/
    # Keep track of last coordinates. Important for 'shift'.
    set thePoly(xy) [list $x $y]
    set thePoly(last) [eval {$w create line} $thePoly($thePoly(N))  \
      {$x $y -tags "polylines" -fill $state(fgCol)  \
      -width $state(penThick)} $extras]
}

# CanvasDraw::FinalizePoly --
#
#       Polygon drawing routines.
#   
# Arguments:
#       w      the canvas widget.
#       x,y    the mouse coordinates.
#       
# Results:
#       none

proc FinalizePoly {w x y} {
    global  myItpref itno prefs allIPnumsToSend
    
    variable thePoly
#d    set wtop [::UI::GetToplevelNS $w]
#d    upvar ::${wtop}::state state

    bind $w <Motion> {}
    bind $w <KeyPress-space> {}
    
    # If anchor not set just return.
    if {![info exists thePoly(0)]} {
	return
    }
    
    # If too few segment.
    if {$thePoly(N) <= 1} {
	$w delete polylines
	catch {unset thePoly}
	return
    }
    
    # Delete last line segment.
    catch {$w delete $thePoly(last)}
    
    # Find out if closed polygon or open line item. If closed, remove duplicate.
    set isClosed 0
    if {[expr   \
      hypot([lindex $thePoly(0) 0] - $x, [lindex $thePoly(0) 1] - $y)] < 4} {
	set isClosed 1
	unset thePoly($thePoly(N))
	incr thePoly(N) -1
    }
    
    # Transform the set of lines to a polygon (or line) item.
    set coords {}
    for {set i 0} {$i <= $thePoly(N)} {incr i} {
	append coords $thePoly($i) " "
    }
    $w delete polylines
#++
    set state(fill) 0
    set state(smooth) 0
    set state(splinesteps) 0
    set myItpref ""
    set itno 0
    set state(fgCol) black
    set state(penThick) 1

#++/
    if {$state(fill) == 0} {
	set theFill "-fill {}"
    } else {
	set theFill "-fill $state(fgCol)"
    }
#d    if {$prefs(hasDash)} {
#d	set extras [list -dash $state(dash)]
#d    } else {
	set extras ""
#d    }
    if {$isClosed} {
	
	# This is a (closed) polygon.
	set cmd "create polygon $coords -tags {poly $myItpref/$itno}  \
	  -outline $state(fgCol) $theFill -width $state(penThick)  \
	  -smooth $state(smooth) -splinesteps $state(splinesteps) $extras"
    } else {
	
	# This is an open line segment.
	set cmd "create line $coords -tags {poly $myItpref/$itno}  \
	  -fill $state(fgCol) -width $state(penThick)  \
	  -smooth $state(smooth) -splinesteps $state(splinesteps) $extras"
    }
    #eval $w $cmd
    #puts "FinalizePoly: cmd=$cmd"
    catch {unset thePoly}
    
    # Let all other clients know.
#d    if {[llength $allIPnumsToSend]} {
#d	SendClientCommand [::UI::GetToplevelNS $w] "CANVAS: $cmd"
#d    }
    incr itno
createpolygon2 $w $coords Blue Black 0 new
bind $w <1> "CanvasStart $w %x %y"
bind $w <B1-Motion> "CanvasDrag $w %x %y"
bind $w <ButtonRelease-1> "CanvasStopDrag $w %x %y"
$w configure -cursor left_ptr
}

################## End import

proc tracepoly {c} {
    bind $c <1> {PolySetPoint %W [%W canvasx %x] [%W canvasy %y] }
    bind $c <B1-Motion> ""
    bind $c <ButtonRelease-1> ""
    $c configure -cursor crosshair
}

proc symv {c} {
global nomf indexpage
set categorie $nomf
if {$categorie== ""} {set categorie "aucune"}
set nomf temp.kds
set indexpage 0
sauve $c symv
exec wish symv.tcl $categorie &
set nomf ""
wm title . "Symcolor"
}

proc symh {c} {
global nomf indexpage categorie
set categorie $nomf
if {$categorie== ""} {set categorie "aucune"}
set nomf temp.kds
set indexpage 0
sauve $c symh
exec wish symh.tcl $categorie &
set nomf ""
wm title . "Symcolor"
}


proc charge2 {c cc what} {
global tid indexpage listdata nomf indexlibre Home canvh canvw tabobj baseHome

        set nomf $what
set listdata {}
set indexpage 0
set indexlibre 0

$c delete all
$cc delete all

catch {unset tid}
catch {unset tabobj}

set f [open [file join $baseHome $nomf] "r"]
set listdata [gets $f]
close $f


foreach it [lindex $listdata 0] {
eval $it
}
#evaluer les noms des pages
#evaluation del adonn�e concernant indexlibre
eval [lindex $listdata end]
set canvw [$c cget -width]
set canvh [$c cget -height]
$c dtag all cible
$c delete enveloppe
$c focus ""


}
