global indexlibre
set indexlibre 0
###################################################################################################"
proc stretchpolygon {c x y} {
global envparam lcord cible tid idd
set lcord [$c coords enveloppe]
set imgcor [$c bbox cible]
set id [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
switch $envparam {
      0 {
                 zoomepolygon1 $c $id $imgcor 0 $x
        }

      1 {
                 zoomepolygon2 $c $id $imgcor 0 $y
        }

     2 {
               zoomepolygon1 $c $id $imgcor 1 $x
       }

      3 {
                 zoomepolygon2 $c $id $imgcor 1 $y
        }

                 }
}


#####################################################################
 
proc zoomepolygon1 {c id imgcor cote x} {
global tid startx lcord
set fact [expr ([lindex $lcord 2] - [lindex $lcord 0])/([lindex $imgcor 2] - [lindex $imgcor 0])]

$c scale $id [lindex [$c bbox $id] 0] [lindex [$c bbox $id] 1] $fact 1
$c move $id [expr $x -[lindex $imgcor 0] - $cote*([lindex $lcord 2] - [lindex $lcord 0])] 0
redessineenveloppe $c $id
}


proc zoomepolygon2 {c id imgcor cote y} {
global tid startx lcord
set fact [expr ([lindex $lcord 3] - [lindex $lcord 1])/([lindex $imgcor 3] - [lindex $imgcor 1])]
$c scale $id [lindex [$c bbox $id] 0] [lindex [$c bbox $id] 1] 1 $fact
 
$c move $id 0 [expr $y -[lindex $imgcor 1] - $cote*([lindex $lcord 3] - [lindex $lcord 1])]

redessineenveloppe $c $id
}


#############################################################################################""
proc createpolygon {c objet xpos ypos} {

global tid canvw canvh
set object [lindex $objet 0]
set id [getid]
set f [open [file join polygon [lindex $object 0].pol] "r"]
set it [gets $f]
close $f
set sm 0
if {[lindex $object 0] == "cercle"} {set sm 1}
$c create polygon $it -fill [lindex $object 1] -tags poly -outline black -smooth $sm -state hidden

$c move $id $xpos $ypos
#set x [expr ([lindex [$c bbox $id] 2] - [lindex [$c bbox $id] 0])/2]
#set y [expr ([lindex [$c bbox $id] 3] - [lindex [$c bbox $id] 1])/2]
if {[lindex $object 2] == "grand"} {
$c scale $id $xpos $ypos 1.2 1.2
} else {
$c scale $id $xpos $ypos 0.9 0.9
}
set tid($id) $object
}



proc getid {} {
global indexlibre
incr indexlibre
set idt $indexlibre
return $idt
}
