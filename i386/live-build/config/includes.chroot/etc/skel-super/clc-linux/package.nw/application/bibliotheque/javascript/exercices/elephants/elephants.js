var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.elephants = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aNombre,chargeMax,totalElephant,aConteneurElephant,aElephant,aConteneurRadeau,aRadeau,aSoluceA,aSoluceB;
var resEvalRadeau;

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:240,
        chargeMax:5000,
        totalElephant:8
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "elephants/textes/elephants_fr.json",
    fond : "elephants/images/fond-fleuve.png",
    radeau  : "elephants/images/radeau.png",
    vague  : "elephants/images/vague.png",
    elephant:"elephants/images/elephant.png",
    illustration : "elephants/images/illustration-elephants.jpg"
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    
    chargeMax = exo.options.chargeMax;
    totalElephant = exo.options.totalElephant;
    aSoluceA=[];aSoluceB=[];aNombre=[];
    for(i=0;i<exo.options.totalQuestion;i++){
        var nA,nB,nC,nD,nE;
        if ( i%3 === 0 ) {
            //cp
            nA = (30 + Math.floor(Math.random()*(33-30+1)))*100;
            nB = (14 + Math.floor(Math.random()*(16-14+1)))*100;
            nC = 5000 - nA - nB;
            aSoluceA[i]=[nA,nB,nC];
            nD = (7 + Math.floor(Math.random()*(13-7+1)))*100;
            nE = (7 + Math.floor(Math.random()*(13-7+1)))*100;
            nF = (7 + Math.floor(Math.random()*(13-7+1)))*100;
            nG = (7 + Math.floor(Math.random()*(13-7+1)))*100;
            nH = 5000 - nD - nE - nF - nG;
            if (nD+nE!=nB && nD+nF!=nB && nD+nG!=nB && nD+nH!=nB && nE+nF!=nB && nE+nG!=nB && nE+nH!=nB && nF+nG!=nB && nF+nH!=nB && nG+nH!=nB && nD+nE+nF+nG<5000) {
                aSoluceB[i] = [nD,nE,nF,nG,nH];
            } else {
                i --;
            }
            console.log(nA+" , "+nB+" , "+nC+" , "+nD+" , "+nE+" , "+nF+" , "+nG+" , "+nH);
            //cp
        }
        else {
            // quatre de chaque côté
            while(nA == nB || nA == nC || nA == nD 
                  || nB == nC || nB == nD 
                  || nA+nB+nC+nD > 5000 || nA+nB+nC+nD < 4800
                  )
            {
                nA = (3 + Math.floor(Math.random()*(28-3+1)))*100;
                nB  = (3 + Math.floor(Math.random()*(28-3+1)))*100;
                nC  = (3 + Math.floor(Math.random()*(28-3+1)))*100;
                nD  = (3 + Math.floor(Math.random()*(28-3+1)))*100;
            }
            aSoluceA[i] = [nA,nB,nC,nD];
            //
            nA=nB=nC=nD=0;
            while(nA == nB || nA == nC || nA == nD 
                  || nB == nC || nB == nD 
                  || nA+nB+nC+nD > 5000 || nA+nB+nC+nD < 4800
                  )
            {
                nA = (3 + Math.floor(Math.random()*(28-3+1)))*100;
                nB  = (3 + Math.floor(Math.random()*(28-3+1)))*100;
                nC  = (3 + Math.floor(Math.random()*(28-3+1)))*100;
                nD  = (3 + Math.floor(Math.random()*(28-3+1)))*100;
            }
            aSoluceB[i] = [nA,nB,nC,nD];
        }
    }
    for(i=0;i<exo.options.totalQuestion;i++){
        aNombre[i]=aSoluceA[i].concat(aSoluceB[i]);
        console.log(aNombre);
        //aNombre[i].sort(function(a,b){return a> b});
        util.shuffleArray(aNombre[i]);
    }
    
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.keyboard.config({
        numeric:"disabled",
        arrow:"disabled"
    });
    exo.blocInfo.css({left:280});
    //
    exo.str.info.msgTempsDepasseJuste  = "Temps dépassé<br>Passe à l'exercice suivant.";
    exo.str.info.msgTempsDepasseFaux = "Temps dépassé !<br>Passe à l'exercice suivant.";
    exo.str.info.msgRien = exo.txt.p3;
    //
    var fond = disp.createImageSprite(exo,"fond");
    fond.css({top:450-fond.height()});
    exo.blocAnimation.append(fond);
    //
    var consigne = disp.createTextLabel(exo.txt.p1+"<br>"+exo.txt.p2);
    consigne.css({fontSize:18,textAlign:"center"});
    exo.blocAnimation.append(consigne);
    consigne.position({my:"center top",at:"center top+20",of:exo.blocAnimation});
    //
    aConteneurElephant=[];
    aElephant=[];
    var q = exo.indiceQuestion;
    var i;
    for ( i=0; i < totalElephant ; i++ ) {
        var valeur = aNombre[q][i];
        var valeurMax = Math.max.apply(this,aNombre[q]);
        var echelle = (valeur/valeurMax)*20+80;
        aConteneurElephant[i] = disp.createEmptySprite();
        aConteneurElephant[i].css({left:i*90+8,top:80,width:91.8,height:150});
        aElephant[i] = disp.createEmptySprite();
        aElephant[i].css({width:76,height:75,top:90});
        aElephant[i].data({index:i,valeur:valeur});
        aElephant[i].addClass("elephant");
        var etiquette = disp.createTextLabel(util.numToStr(valeur)+"kg");
        etiquette.css({fontSize:14,color:"#000", fontWeight:"bold",textAlign:"center",width:"100%"});
        
        var imgElephant = disp.createImageSprite(exo,"elephant");
        aElephant[i].append(imgElephant);
        aConteneurElephant[i].append(aElephant[i]);
        exo.blocAnimation.append(aConteneurElephant[i]);
        imgElephant.css({width:echelle+"%"});
        console.log("echelle",echelle);
        imgElephant.css({top:75-imgElephant.find("img").height()-19});
        aElephant[i].append(etiquette);
        etiquette.css({top:75-etiquette.height()-2});
        aConteneurElephant[i].droppable({
            accept:".elephant",
            drop:gererDropConteneurElephant
        });
        aElephant[i].draggable({
            start:function(e,ui){
                $(this).draggable("option", "revert", "invalid");
            },
            stack:".elephant"
        });
    }
    //
    aConteneurRadeau=[];
    aRadeau=[];
    for( i=0; i < 2; i++){
        aConteneurRadeau[i] = disp.createEmptySprite();
        aConteneurRadeau[i].data({index:i});
        aConteneurRadeau[i].css({left:20+(i*(306+80)),top:230,width:306,height:100});
        exo.blocAnimation.append(aConteneurRadeau[i]);
        aRadeau[i] = disp.createImageSprite(exo,"radeau");
        aConteneurRadeau[i].append(aRadeau[i]);
        aRadeau[i].css({top:100-aRadeau[i].height()});
        aConteneurRadeau[i].droppable({
            accept:".elephant",
            drop:gereDropConteneurRadeau
        });
    }
    //
    var vague = disp.createImageSprite(exo,"vague");
    vague.css({top:aConteneurRadeau[0].position().top+85});
    exo.blocAnimation.append(vague);


    /* gestionnaire drop */
    function gererDropConteneurElephant(e,ui){
        console.log($(this).children('.elephant').length);
        if ($(this).children('.elephant').length === 0) {
            ui.draggable.css({left:0,top:150-ui.draggable.height()+14});
            $(this).append(ui.draggable);
        } else {
            ui.draggable.draggable({revert:true});
        }
    }

    function gereDropConteneurRadeau(e,ui){
        var radeau = $(this);
        radeau.append(ui.draggable);
        var countElephant = radeau.children(".elephant").length;
        radeau.children(".elephant").each(function(index,value){
            var posX = ((index/countElephant)*(radeau.width()+20)-10);
            $(value).css({left:posX,top:radeau.height()-$(value).height()-5});
        });
    }
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    resEvalRadeau = [];
    var total=0;
    var nElephant = 0;
    //
    var i;
    //
    for( i = 0; i < 2; i++ ) {
        total = 0;
        aConteneurRadeau[i].find(".elephant").each(function(index,value){
            total+=$(value).data().valeur;
            nElephant++;
        });
        resEvalRadeau[i] = total <= chargeMax ? "juste" : "faux";
    }
    //
    if (nElephant < totalElephant ) {
        return "rien";
    }
    else if (nElephant == totalElephant && util.find("faux",resEvalRadeau) >=0 ) {        
        return "faux";
    }
    else {
        aConteneurRadeau[1].transition({x:"+=735px"},2000,"linear");
        aConteneurRadeau[0].transition({x:"+=735px",delay:500},2000,"linear",function(){
            exo.afficherBtnSuite();
        });
        return "juste";
    }
};

// Correction (peut rester vide)
exo.corriger = function() {
    exo.blocInfo.show();
    var indexFaux = util.find("faux",resEvalRadeau);
    if(indexFaux > -1 ){
        exo.cacherBtnSuite();
        console.log("radeau n°"+indexFaux+" trop lourd.");
        // affichage de l'avertissement
        var avert = disp.createTextLabel("Trop lourd !");
        console.log("indexFaux",indexFaux);
        avert.css({fontSize:18,color:"#fff",left:aConteneurRadeau[indexFaux].position().left+80,top:350});
        exo.blocAnimation.append(avert);
        // le radeau et les éléphants descendent
        aConteneurRadeau[indexFaux].transition({y:"+=95px"},2000,"easeOutCubic",function(e){
            var signe = "+",rotation="0deg";
            if ( indexFaux === 0 ) {
                signe="-";
                rotation="180deg";
            }
            var conteneur = $(this);
            var elephants = conteneur.children(".elephant");
            // les éléphants se mettent en position "nage"
            elephants.each(function(index,value){
                $(value).children(".text-sprite").hide();
                $(value).css({rotateY:rotation});
                $(value).css({rotate:"-65.5deg"});
            });
            avert.remove();
            // le radeau et les élphants remontent
            conteneur.transition({y:"-=40px"},400,"easeOutBack");
            //Le radeau et les éléphants sortent
            conteneur.transition({x:signe+"=350px",delay:500},2000,"linear",function(e){
                $(this).css({x:0,y:0});
                //var index = $(this).data().index;
                $(this).css({left:20+(indexFaux*(306+80)),top:230});
                for( var i = 0; i < aElephant.length; i++ ) {
                        aElephant[i].remove();
                }
                // on affiche la solution;
                afficherSoluce();
            });
        });
    }

    function afficherSoluce(){
        aElephant=[];
        var q = exo.indiceQuestion;
        var aSoluce = aSoluceA[q].concat(aSoluceB[q]);
        for ( var j = 0; j < totalElephant ; j++ ) {
            var valeur = aSoluce[j];
            var offset;
            offset = j < aSoluceA[q].length ? 20 : 220;
            var valeurMax = Math.max.apply(this,aNombre[q]);
            var echelle = (valeur/valeurMax)*20+80;
            aElephant[j] = disp.createEmptySprite();
            aElephant[j].css({width:76,height:75,top:230,left:(j*60)+offset});
            aElephant[j].addClass("elephant");
            var etiquette = disp.createTextLabel(util.numToStr(valeur)+"kg");
            etiquette.css({fontSize:14,fontWeight:"bold",textAlign:"center",width:"100%"});
            aElephant[j].append(etiquette);
            var imgElephant = disp.createImageSprite(exo,"elephant");
            aElephant[j].append(imgElephant);
            imgElephant.css({width:echelle+"%"});
            imgElephant.css({top:19});
            
            (function(){
                var index=j;
                exo.setTimeout(function(){
                    console.log(index);
                    exo.blocAnimation.append(aElephant[index]);
                    if (index==totalElephant-1) {
                        exo.afficherBtnSuite();
                    }
                },400*index+500);
            })(j);
        }
    }
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));
