var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.memory = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Variables globales à l'exercice
var aNombres = new Array();
var aPhrases = new Array();
var aTableau = new Array(aNombres,aPhrases);	
var repCarteUN;
var repCarteDEUX;
var repCarteUNData;
var repCarteDEUXData;
var iCompteur;
var iNbPaires;

var q;

// Référencer les ressources de l'exercice (image, son)
exo.oRessources = { 
	txt:"memory/textes/memory_fr.json",
    	illustration:"memory/images/illustration.png",
	carte:"memory/images/carte.png",
	carteRetournee:"memory/images/carteRetournee.png",
	carteRetourneeSmall:"memory/images/carteRetourneeSmall.png"
}

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:1,
	tempsExo:0
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    
	aNombres[0] = "3";
	aPhrases[0] = "moitié<br> de 6";
	aNombres[1] = "4";
	aPhrases[1] = "moitié<br> de 8";
	aNombres[2] = "5";
	aPhrases[2] = "moitié<br> de 10";
	aNombres[3] = "6";
	aPhrases[3] = "double<br> de 3";
	aNombres[4] = "14";
	aPhrases[4] = "double<br> de 7";
	aNombres[5] = "16";
	aPhrases[5] = "double<br> de 8";
	aNombres[6] = "18";
	aPhrases[6] = "double<br> de 9";
	aNombres[7] = "10";
	aPhrases[7] = "double<br> de 5";
}

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
}

/************************************************************************************************************************************
* Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation  
 - garder en mémoire la donnée de la carte 
*************************************************************************************************************************************/
exo.creerPageQuestion = function() {
	
	exo.btnValider.hide();

	//pour le clavier virtuel
	exo.keyboard.config({
        numeric:"disabled",
        arrow:"disabled",
        large:"disabled"
    });


	exo.blocScore.hide();
	var i = 0;
	iCompteur=0;
	iNbPaires=0;
	var bTrouve = false;
    
    q=0;
	
	
	repCarteUNData = 0;
	repCarteDEUXData = 0;
	
		
	//mélanger les cartes
	var aCartes = new Array();
	for ( i = 0 ; i < 8 ; i++)
	{
		aCartes[i] = aNombres[i];
		aCartes[i+8] = aPhrases[i];
	}	
	fMelange(aCartes);
		
	//Pour obtenir les tailles des images	
	var carteInfo = disp.createImageSprite(exo,"carteRetourneeSmall");
	var carteRetourneeInfo = disp.createImageSprite(exo,"carteRetournee");		
	
		
	conteneur = disp.createEmptySprite();
	conteneur.css({
        width:(100+2)*4, //taille largeur carte*nbre cartes sur 1 ligne * ne pas oublier les espaces de 2 entre les cartes
        height:(73+2)*4, //taille hauteur carte*nbre cartes sur 1 colonne * ne pas oublier les espaces de 2 entre les cartes
		//border:'1px solid red'
	 });
	console.log(conteneur.width(),conteneur.height())
    	exo.blocAnimation.append(conteneur);
	

	
	
	conteneurScore = disp.createEmptySprite();
	
    	conteneurScore.css({
        width:(carteInfo.width()+2)*2, //taille largeur carte*nbre cartes sur 1 ligne * ne pas oublier les espaces de 2 entre les cartes
        height:(carteInfo.height()+2)*8 //taille hauteur carte*nbre cartes sur 1 colonne * ne pas oublier les espaces de 2 entre les cartes
		//background:'blue'
    	})
    	exo.blocAnimation.append(conteneurScore);

    	for (  i = 0 ; i < 16 ; i++) {
		var carteRetournee = disp.createImageSprite(exo,"carteRetournee");	
		conteneur.append(carteRetournee);
					
		var etiquette = disp.createTextLabel(aCartes[i]);
		etiquette.css({
			fontSize:18,
			fontWeight:'bold',
			textAlign:'center',
			color:'#0000'
		});
		carteRetournee.append(etiquette);
		
		etiquette.position({
			my:"center center",
			at:"center center",
			of:carteRetournee
		})
		
		var carte = disp.createImageSprite(exo,"carte");
		
		conteneur.append(carte);
			
		var posX = (i % 4) * (carte.width() + 2); // 4 cartes sur une ligne
		var posY = Math.floor(i / 4) * (carte.height() + 2); // 4 cartes sur une colonne
		carte.css({
			//opacity:0.5, // Pour voir les cartes si on doit dev 
			left:posX,
			top:posY
		});
		
		carteRetournee.css({
			
			left:posX,
			top:posY
		});
			
			
		// fonctions à activer suite à l'événement "click"
		carte.data("valeur",aCartes[i]);	
		carte.attr("class",aCartes[i]);
		carteRetournee.attr("class",aCartes[i]);
		//carte.on("click",gestionClickCarte);
		carte.on("mousedown touchstart",gestionClickCarte);
	}
	 conteneur.position({
        my:"center center",
        at:"center-16 center",
        of:exo.blocAnimation
        //offset:"-16px 0px"
    })
	
	conteneurScore.position({
        my:"left",
        at:"left+10",
        of:exo.blocAnimation
        //offset:"10px 0px"
    })
}
// Evaluation doit toujours retourner "juste" "faux" ou "rien"


// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    
    
}

/****************************************
*   Mélanger les données d'un tableau
*****************************************/
function fMelange(tableau)
{
   	var iTaille = tableau.length; //enregistrement du nombre de cartes 
	if (iTaille == 0)	{ return false;	} //si nombre de carte est égale à 0 le melange s'arrete
	while (iTaille--)
	{//le nombre de carte à melanger est diminué de 1
		var j = Math.floor(Math.random() * (iTaille + 1));//valeur aleatoire compris dans le nombre de cartes restants à mélanger
		var tempi=tableau[iTaille];//enregistrement de la derniere case
		var tempj=tableau[j];//enregistrement de la case aléatoire
		tableau[iTaille]=tempj;//placement à la fin de la case aléatoire
		tableau[j]=tempi;//placement de laderniere case à la place de la case aléatoire
	}
	return(tableau);//retourne le tableau
}

/***********************************************
*   Si clic sur le dos de la carte...
*   - garder en mémoire la donnée de la carte 
***********************************************/
function gestionClickCarte(e) {
  
  
  
  // pour éviter d'avoir un troisième click pendant le masquage des
  // deux cartes non bonnes
  if(repCarteDEUXData == 0)
  {
 
   // désigner la carte comme l'élément sur lequel on travaille 
   carteClique = $(e.delegateTarget);
   carteClique.hide();
	
	if(repCarteUNData==0)
	{
		repCarteUN = carteClique;  
		repCarteUNData = carteClique.data("valeur");  
		
	}
	else //(repCarteDEUX==0)
	{
		repCarteDEUX = carteClique;  
		repCarteDEUXData = carteClique.data("valeur");
		
		
		// vérifier si les deux paires sont bonnes
		var bTrouve = false;
		
		for ( i = 0 ; i < 8 ; i++)
	    {
			if(repCarteUNData == aNombres[i])
			{	
				if(repCarteDEUXData == aPhrases[i])
				{
					bTrouve=true;
				}
			}	
			
			if(repCarteUNData == aPhrases[i])
			{	
				if(repCarteDEUXData == aNombres[i])
				{
					bTrouve=true;
				}
			}	
		}	
		
		if(bTrouve==true)
		{
			//carteClique.show();
			carteClique.off("mousedown touchstart");	
			carteClique.disableSelection();
			repCarteUN.off("mousedown touchstart");	
			repCarteUN.disableSelection();
			//*******************************************************
			// // ranger les cartes sur le côté
			//*******************************************************
			var carteRetourneeSmallUN = disp.createImageSprite(exo,"carteRetourneeSmall");
			var carteRetourneeSmallDEUX = disp.createImageSprite(exo,"carteRetourneeSmall");
        	
			conteneurScore.append(carteRetourneeSmallUN);
			conteneurScore.append(carteRetourneeSmallDEUX);
			
			var etiquetteSmallUN = disp.createTextLabel(repCarteUNData);
			var etiquetteSmallDEUX = disp.createTextLabel(repCarteDEUXData);
		        etiquetteSmallUN.css({
            				fontSize:13,
					fontWeight:'bold',
					textAlign:'center',
					color:'#0000'
        				});
			etiquetteSmallDEUX.css({
	            			fontSize:13,
					fontWeight:'bold',
					textAlign:'center',
					color:'#0000'
        				});
        		carteRetourneeSmallUN.append(etiquetteSmallUN);
			carteRetourneeSmallDEUX.append(etiquetteSmallDEUX);
		        etiquetteSmallUN.position({
         	 	  my:"center center",
           		 at:"center center",
			of:carteRetourneeSmallUN
		        })
			etiquetteSmallDEUX.position({
         	 	  my:"center center",
           		 at:"center center",
			of:carteRetourneeSmallDEUX
		        })
        
			    var posXsmallUN = (iCompteur % 2) * (carteRetourneeSmallUN.width() + 2); // 2 cartes sur une ligne
		        var posYsmallUN = Math.floor(iCompteur / 2) * (carteRetourneeSmallUN.height() + 2); // 8 cartes sur une colonne
        		var posXsmallDEUX = ((iCompteur+1) % 2) * (carteRetourneeSmallDEUX.width() + 2); // 2 cartes sur une ligne
		        var posYsmallDEUX = Math.floor((iCompteur+1) / 2) * (carteRetourneeSmallDEUX.height() + 2); // 8 cartes sur une colonne
        
		        carteRetourneeSmallUN.css({
            					left:posXsmallUN,
            					top:posYsmallUN
		        });
			   
			   carteRetourneeSmallDEUX.css({
            					left:posXsmallDEUX,
            					top:posYsmallDEUX
		        });

			iCompteur= iCompteur+2;
			
					
			$('div[class="' + repCarteUNData + '"]').hide();
			$('div[class="' + repCarteDEUXData + '"]').hide();
			 
			repCarteUNData=0;
			repCarteDEUXData=0;
			
			
			q=q+1;
			exo.blocScore.show();
			
			if(iCompteur==16)
			{
				exo.blocScore.html("8 / "+(q-1)); 
				exo.poursuivreExercice();
			}
			else
			{
				exo.blocScore.html((iCompteur/2)+" / "+q); 
			}
		}
		else
		{
			q=q+1;
			exo.blocScore.show();
			exo.blocScore.html((iCompteur/2)+" / "+q); 
			// faire une petite pause pour laisser les cartes affichées
			repCarteDEUX.show(1500, function (){
							// réinititaliser
							repCarteUNData = 0;
							repCarteDEUXData = 0;
							   } );
			repCarteUN.show(1500);
		}
	}
	
	}
	
}

exo.evaluer = function() {
	
   // si le compteur = nombre de paires de cartes trouvées
   if(iCompteur==16)
   {
	exo.indiceQuestion=q-1;
	exo.options.totalQuestion=q-1;
	exo.score=7;
        return "juste";
   }
}

// Correction (peut rester vide)
exo.corriger = function() {
	
}
/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))
