﻿class OptionCheckBox {
	private var cadre_checkbox:MovieClip;
	private var cadre_x:Number;
	private var cadre_y:Number;
	private var cadre_largeur:Number;
	private var aTextes:Array;
	private var aValeurs:Array;
	public var aCases:Array;
	private var aLabels:Array;
	private var valeur;
	private var nom:String;
	private var orientatio:String;
	//public function OptionRadio(sNom:String, cadre_x:Number, cadre_y:Number, cadre_largeur:Number) {
	public function OptionCheckBox(mc_conteneur:MovieClip, nom:String,sOrientation:String) {
		this.cadre_checkbox = mc_conteneur.createEmptyMovieClip(nom, mc_conteneur.getNextHighestDepth());
		this.nom = nom;
		if(sOrientation==undefined){
			this.orientatio="horizontale";
		}else {
			this.orientatio=sOrientation;
		}
	}
	public function AjouteCases(aTextes:Array, aValeurs:Array) {
		this.aTextes = aTextes;
		this.aValeurs = aValeurs;
		this.aCases = new Array();
		this.aLabels = new Array();
		for (var i = 0; i<aTextes.length; i++) {
			this.aCases[i] = this.cadre_checkbox.attachMovie("case_a_cocher", "case_"+i, this.cadre_checkbox.getNextHighestDepth());
			this.aCases[i].bChecked = false;
			this.aCases[i].valeur = aValeurs[i];
			this.aLabels[i] = this.cadre_checkbox.createTextField("lbl_case_"+i, this.cadre_checkbox.getNextHighestDepth(), this.aCases[i]._width+2, 0, 0, 0);
			this.aLabels[i].embedFonts=true;
			this.aLabels[i].antiAliasType = "advanced";
			this.aLabels[i].text = aTextes[i];
			var fmt = new TextFormat();
			fmt.size = 14;
			fmt.bold = false;
			fmt.font = "emb_tahoma";
			this.aLabels[i].setTextFormat(fmt);
			this.aLabels[i].autoSize = true;
		}
		if(orientatio=="horizontale"){
			for (var i = 1; i<this.aCases.length; i++) {
				this.aCases[i]._x = this.aLabels[i-1]._x+this.aLabels[i-1]._width+20;
				this.aLabels[i]._x = this.aCases[i]._x+this.aCases[i]._width+2;
			}
		} else if(orientatio=="verticale") {
			for (var i = 1; i<this.aCases.length; i++) {
				this.aCases[i]._y = this.aLabels[i-1]._y+30;
				this.aLabels[i]._x = this.aCases[i]._x+this.aCases[i]._width+2;
				this.aLabels[i]._y = this.aCases[i]._y;
			}
		} 
		//Gestion du clic sur les cases
		for (var i = 0; i<this.aCases.length; i++) {
			var objGroupeCases = this;
			this.aCases[i].onPress = function() {
				if (this.bChecked == false) {
					this.bChecked = true;
					this.gotoAndStop(2);
				}else{
					this.bChecked = false;
					this.gotoAndStop(1);
				}
				//on genere un evt onClick qui pourra etre récupéré
				AsBroadcaster.initialize(this.aCases[i]);
				this.broadcastMessage("onClick");
			};
		}
	}
	public function SetPos(pos_x:Number, pos_y:Number) {
		this.cadre_checkbox._x = pos_x;
		this.cadre_checkbox._y = pos_y;
	}
	public function SetValeur(nValeurBouton:String) {
		for (var j = 0; j<this.aCases.length; j++) {
			if (this.aCases[j].valeur == nValeurBouton) {
				//this.valeur = nValeurBouton;
				this.aCases[j].bChecked = true;
				this.aCases[j].gotoAndStop(2);
			} 
			/*else {
				this.aCases[j].bChecked = false;
				this.aCases[j].gotoAndStop(1);
			}*/
		}
	}
	public function GetValeur() {
		var aValeursCochees=new Array();
		for (var i = 0; i<this.aCases.length; i++) {
			if(this.aCases[i].bChecked == true){
				aValeursCochees.push(this.aValeurs[i]);
			}
		}
		return aValeursCochees;
	}
}
