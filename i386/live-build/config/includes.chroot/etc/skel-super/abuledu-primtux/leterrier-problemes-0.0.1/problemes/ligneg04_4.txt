set etapes 1
set niveaux {0 1 2 3 5}
::1
set niveau 2
set ope {{3 5} {2 10} {2 10}}
set interope {{0 10 1} {1 20 1} {1 20 1}}
set scaleb {0 60 1}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0]]
set operations [list [list [expr $ope1 ]+[expr $ope2]+[expr $ope3]] [list [expr $ope1 ]+[expr $ope3]+[expr $ope2]] [list [expr $ope2]+[expr $ope1]+[expr $ope3]] [list [expr $ope2]+[expr $ope3]+[expr $ope1]] [list [expr $ope3]+[expr $ope1]+[expr $ope2]] [list [expr $ope3]+[expr $ope2]+[expr $ope1]] [list [expr $ope1]+[expr $ope2 + $ope3]] [list [expr $ope2 + $ope3]+[expr $ope1]] [list [expr $ope1 + $ope2]+[expr $ope3]] [list [expr $ope3]+[expr $ope1 + $ope2]] [list [expr $ope2]+[expr $ope3 + $ope2]] [list [expr $ope3 + $ope2]+[expr $ope2]]]
set enonce "Le jeu de l'oie.\nLe pion de Tim est sur la case $ope1.\nIl avance de  $ope2 cases, puis encore de $ope3 cases.\nSur quelle case se trouve le pion de Tim maintenant?"
set reponse [list [list {1} [list {Il est maintenant sur la case} [expr $ope1 + $ope2 + $ope3]]]]
set resultdessin [expr $ope1 + $ope2 + $ope3]
set source {{0}}
set dpt $ope1
set opnonautorise {}
set canvash 140
set c1height 50
set placearriv none
::