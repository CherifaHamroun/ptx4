############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editvoir.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editvoir.tcl,v 1.1.1.1 2004/04/16 11:45:47 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome


switch $tcl_platform(platform) {
    unix {
	source editvoirlinux.tcl
        }
    windows {
	source editvoirwindows.tcl

        }
}


set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"

global arg formes couleurs tailles
set arg [lindex $argv 0]
#set arg "Que_voir"
set formes {cercle carre rectangle triangle}
set couleurs {gray75 red green blue  yellow cyan magenta  brown}
set tailles {petit grand}
. configure -background white
frame .geneframe -background white -height 300 -width 400
pack .geneframe -side left

#variables
#################################################génération interface
proc interface { what} {
global sysFont arg  typexo formes couleurs tailles
set compteligne 4
set ext .gif
foreach item $formes {
image create photo $item -file [file join sysdata $item$ext]
}
foreach item {petit grand} {
image create photo $item -file [file join sysdata $item$ext]
}

catch {destroy .geneframe}

catch {destroy .leftframe}

frame .leftframe -background white -height 480 -width 100 
pack .leftframe -side left -anchor n

frame .geneframe -background white -height 300 -width 200 -background gray90
pack .geneframe -side left -anchor n
label .leftframe.lignevert1 -bg red -text "" -font {Arial 1}
pack .leftframe.lignevert1 -side right -expand 1 -fill y

######################leftframe.frame1
frame .leftframe.frame1 -background white -width 100 
pack .leftframe.frame1 -side top 

label .leftframe.frame1.lab1 -foreground red -bg white -text [mc $arg] -font $sysFont(l)
pack .leftframe.frame1.lab1 -side top -fill x -pady 5

label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {Scenario}] -font $sysFont(l)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listsce -yscrollcommand ".leftframe.frame1.scrollpage set" -width 15 -height 6
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listsce yview" -width 7
pack .leftframe.frame1.listsce .leftframe.frame1.scrollpage -side left -fill y -expand 1 -pady 10
bind .leftframe.frame1.listsce <ButtonRelease-1> "changelistsce %x %y"

############################leftframe.frame2
frame .leftframe.frame2 -background white -width 100 
pack .leftframe.frame2 -side top 
button .leftframe.frame2.but0 -text [mc {Nouveau}] -command "additem" -activebackground white
pack .leftframe.frame2.but0 -pady 5 -side top
button .leftframe.frame2.but1 -text [mc {Supprimer}] -command "delitem" -activebackground white
pack .leftframe.frame2.but1 -pady 5 -side top


########################leftframe.frame3
frame .leftframe.frame3 -background white -width 100
pack .leftframe.frame3 -side top 
label .leftframe.frame3.lab0 -foreground white  -text "        " -font $sysFont(l)
pack .leftframe.frame3.lab0 -side top -fill both -expand 1 -pady 10 
button .leftframe.frame3.but0 -text [mc {ok}] -command "fin" -activebackground white
pack .leftframe.frame3.but0 -side bottom

##########################################################################################################
set tmp 3
for {set i 0} {$i < $compteligne} {incr i 1} {
  label .geneframe.labll$i -bg red -text "" -font {Arial 1}
  grid .geneframe.labll$i -column 0 -row $tmp -columnspan 10 -sticky "w e"
  incr tmp 2 

}

#############
label .geneframe.lab0_0 -bg white -text [mc {Proprietes des objets}] -font $sysFont(s)
grid .geneframe.lab0_0 -column 0 -row 0 -sticky "w e" -columnspan 9
label .geneframe.lab0_1 -bg #78F8FF -text [mc {Propriete discriminante}] -font $sysFont(s)
grid .geneframe.lab0_1 -column 9 -row 0 -sticky "w e"

label .geneframe.labl1 -bg red -text "" -font {Arial 1}
grid .geneframe.labl1 -column 0 -row 1 -columnspan 10 -sticky "w e"

set tmp 2
label .geneframe.forme -text [mc {Formes :}] -activebackground grey
grid .geneframe.forme -column 0 -row $tmp -padx 10 -pady 5
set compt 1
foreach item $formes {
checkbutton .geneframe.$item -image $item -activebackground grey -indicatoron 0 -width 40 -height 40 -variable $item
grid .geneframe.$item -column $compt -row $tmp -padx 5 -pady 5 -columnspan 2
bind .geneframe.$item <ButtonRelease-1> "forme_event $item"
incr compt 2
}
incr tmp 2
label .geneframe.couleur -text [mc {Couleurs :}] -activebackground grey
grid .geneframe.couleur -column 0 -row $tmp -padx 5 -pady 5
set compt 1
foreach coul $couleurs {
checkbutton .geneframe.$coul  -text "    " -activebackground $coul -background $coul -indicatoron 0 -selectcolor $coul -variable $coul
grid .geneframe.$coul -column $compt -row $tmp -padx 5 -pady 5
bind .geneframe.$coul <ButtonRelease-1> "coul_event $coul"
incr compt
}
incr tmp 2
label .geneframe.taille -text [mc {Tailles :}] -activebackground grey
grid .geneframe.taille -column 0 -row $tmp -padx 5 -pady 5
set compt 1
foreach taill $tailles {
variable $taill
checkbutton .geneframe.$taill  -image $taill -activebackground grey -indicatoron 0 -width 72 -height 72 -variable $taill
grid .geneframe.$taill -column $compt -row $tmp -padx 5 -pady 5 -columnspan 4
bind .geneframe.$taill <ButtonRelease-1> "taille_event $taill"
incr compt 4
}
incr tmp 2

label .geneframe.labvisible -text [mc {Nombre d'objets visibles(max)}] -activebackground grey
grid .geneframe.labvisible -column 0 -row $tmp -padx 5 -pady 5 -columnspan 2

scale .geneframe.visible -orient horizontal -length 80 -from 2 -to 10 -tickinterval 8 -borderwidth 0 -font $sysFont(s) -variable visible
grid .geneframe.visible -column 2 -row $tmp -sticky "w e" -columnspan 6
bind .geneframe.visible <ButtonRelease-1> "changescales"

incr tmp 2
label .geneframe.minmax -text [mc {Nombre total d'objets(min max) :}] -activebackground grey
grid .geneframe.minmax -column 0 -row $tmp -padx 5 -pady 5 -columnspan 2
scale .geneframe.min -orient horizontal -length 80 -from 5 -to 30 -tickinterval 25 -borderwidth 0 -font $sysFont(s) -variable min
grid .geneframe.min -column 2 -row $tmp -sticky "w e" -columnspan 3
scale .geneframe.max -orient horizontal -length 80 -from 5 -to 30 -tickinterval 25 -borderwidth 0 -font $sysFont(s) -variable max
grid .geneframe.max -column 5 -row $tmp -sticky "w e" -columnspan 3
bind .geneframe.min <ButtonRelease-1> "changescalemin"
bind .geneframe.max <ButtonRelease-1> "changescalemax"

set compt 2
foreach props {1 2 3} {
checkbutton .geneframe.prop$props -text "  " -activebackground #78F8FF -background #78F8FF -selectcolor #78F8FF -variable prop([expr $props -1])
grid .geneframe.prop$props -column 9 -row $compt -sticky "n e w s"
bind .geneframe.prop$props <ButtonRelease-1> "prop_event [expr $props - 1]"
incr compt 2
}
label .geneframe.labt1 -text "  " -activebackground #78F8FF -background #78F8FF
grid .geneframe.labt1 -column 9 -row $compt -sticky "n e w s"
incr compt 2
label .geneframe.labt2 -text "  " -activebackground #78F8FF -background #78F8FF
grid .geneframe.labt2 -column 9 -row $compt -sticky "n e w s"
charge 0
}

proc changescales {} {
#global copie_visible
#variable visible
#set min [expr ([.geneframe.min cget -from]*$visible)/$copie_visible]
#set max [expr ([.geneframe.max cget -to]*$visible)/$copie_visible]
#set copie_visible $visible
#updatescales $min $max
global formes tailles couleurs
set total0 0
set total1 0
set total2 0
foreach item $formes {
variable $item
set total0 [expr $total0 + $$item]
}
foreach item $couleurs {
variable $item
set total1 [expr $total1 + $$item]
}
foreach item $tailles {
variable $item
set total2 [expr $total2 + $$item]
}
majscales $total0 $total1 $total2
}

proc updatescales {mini maxi} {
variable min
variable max
variable visible

.geneframe.max configure -to $maxi
if {$max > $maxi} {
set max $maxi
}
.geneframe.min configure -from $mini
if {$min < $mini} {
set min $mini
}
.geneframe.max configure -from $mini
.geneframe.min configure -to $maxi
.geneframe.max configure -tickinterval [expr $maxi - $mini]
.geneframe.min configure -tickinterval [expr $maxi - $mini]
}



####################################################################"""
proc charge {index} {
global listdata activelist indexpage totalpage arg compteligne Home formes couleurs tailles
variable typeexo
variable prop
variable visible
variable min
variable max

set ext .conf
.geneframe.visible set 2
.geneframe.min set 5
.geneframe.max set 5
foreach item {1 2 3} {
.geneframe.prop$item configure -state normal
}


set f [open [file join $Home reglages $arg$ext] "r"]
set listdata [gets $f]
close $f


set totalpage [llength $listdata] 
set indexpage $index
	.leftframe.frame1.listsce delete 0 end
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.leftframe.frame1.listsce insert end [lindex [lindex [lindex $listdata $k] 0] 0]
	}
set activelist [lindex $listdata $indexpage]
.leftframe.frame1.listsce selection set $indexpage
	set typeexo [lindex [lindex $activelist 0] 1]
################################################################################modifier
set compt 0
set total 0
foreach itm $formes {
variable $itm 
set $itm [lindex [lindex [lindex $activelist 1] 0] $compt]
set total [expr $total + $$itm]
incr compt
}
if {$total == 1} {.geneframe.prop1 configure -state disabled}

set compt 0
set total 0
foreach itm $couleurs {
variable $itm 
set $itm [lindex [lindex [lindex $activelist 1] 1] $compt]
set total [expr $total + $$itm]
incr compt
}
if {$total == 1} {.geneframe.prop2 configure -state disabled}

set compt 0
set total 0
foreach itm $tailles {
variable $itm 
set $itm [lindex [lindex [lindex $activelist 1] 2] $compt]
set total [expr $total + $$itm]
incr compt
}
if {$total == 1} {.geneframe.prop3 configure -state disabled}

set compt 0
foreach itm {0 1 2} {
set prop($itm) [lindex [lindex [lindex $activelist 1] 3] $compt]
incr compt
}
.geneframe.visible configure -from [lindex [lindex [lindex $activelist 1] 4] 0] -to [lindex [lindex [lindex $activelist 1] 4] 2] -tickinterval [expr [lindex [lindex [lindex $activelist 1] 4] 2] -[lindex [lindex [lindex $activelist 1] 4] 0]]
set visible [lindex [lindex [lindex $activelist 1] 4] 1]

.geneframe.min configure -from [lindex [lindex [lindex $activelist 1] 5] 0] -to [lindex [lindex [lindex $activelist 1] 5] 2] -tickinterval [expr [lindex [lindex [lindex $activelist 1] 5] 2] -[lindex [lindex [lindex $activelist 1] 5] 0]]
set min [lindex [lindex [lindex $activelist 1] 5] 1]
.geneframe.max configure -from [lindex [lindex [lindex $activelist 1] 6] 0] -to [lindex [lindex [lindex $activelist 1] 6] 2] -tickinterval [expr [lindex [lindex [lindex $activelist 1] 6] 2] -[lindex [lindex [lindex $activelist 1] 6] 0]]
set max [lindex [lindex [lindex $activelist 1] 6] 1]

}



###################################################################################"
proc enregistre_sce {} {
global listdata activelist indexpage totalpage arg compteligne Home formes couleurs tailles

set ext .conf
variable typeexo
variable prop
variable visible
variable min
variable max
set listpartiel ""
set activelist \173[.leftframe.frame1.listsce get $indexpage]\040$typeexo\175
set listforme ""
foreach itm $formes {
variable $itm
lappend listforme [expr $$itm]
}
lappend listpartiel $listforme

set listcouleur ""
foreach itm $couleurs {
variable $itm
lappend listcouleur [expr $$itm] 
}
lappend listpartiel $listcouleur

set listtaille ""
foreach itm $tailles {
variable $itm
lappend listtaille [expr $$itm]  
}
lappend listpartiel $listtaille

set listprop ""
foreach itm {0 1 2} {
lappend listprop $prop($itm)  
}
lappend listpartiel $listprop

lappend listpartiel [expr int([.geneframe.visible cget -from])]\040[.geneframe.visible get]\040[expr int([.geneframe.visible cget -to])]
lappend listpartiel [expr int([.geneframe.min cget -from])]\040[.geneframe.min get]\040[expr int([.geneframe.min cget -to])]
lappend listpartiel [expr int([.geneframe.max cget -from])]\040[.geneframe.max get]\040[expr int([.geneframe.max cget -to])]

lappend activelist $listpartiel
set listdata [lreplace $listdata $indexpage $indexpage $activelist]
set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $listdata
close $f

}


###################################################################
proc changescalemin {} {
variable visible
variable min
variable max
if {[.geneframe.min get] > [.geneframe.max get] } { 
.geneframe.max set [.geneframe.min get] 
}
}

proc changescalemax {} {
variable visible
variable min
variable max
if {[.geneframe.max get] < [.geneframe.min get] } { 
.geneframe.min set [.geneframe.max get] 
}
}


proc changelistsce {x y} {
enregistre_sce
set ind [.leftframe.frame1.listsce index @$x,$y]
charge $ind
}

proc fin {} {
enregistre_sce
destroy .
}

#######################################################################
proc delitem {} {
global listdata indexpage totalpage arg Home
set ext .conf
	if {$totalpage < 2} {
	tk_messageBox -message [mc {Impossible de supprimer la fiche}] -type ok -title [mc $arg]
	return
	}
set response [tk_messageBox -message [format [mc {Voulez-vous vraiment supprimer la fiche %1$s ?}] [lindex [lindex [lindex $listdata $indexpage] 0] 0]] -type yesno -title [mc $arg]]
#set response [tk_messageBox -message "Voulez-vous vraiment supprimer la fiche [lindex [lindex [lindex $listdata $indexpage] 0] 0]?" -type yesno -title $arg]
	if {$response == "yes"} {
	set totalpage [expr $totalpage - 1]
		set listdata [lreplace $listdata $indexpage $indexpage]
     		if {$indexpage > 0} {
     		set indexpage [expr $indexpage - 1]						
		} 
	set f [open [file join $Home reglages $arg$ext] "w"]
	puts $f $listdata
	close $f
	charge $indexpage

	} 

}


proc additem {} {
global listdata indexpage totalpage 
enregistre_sce
catch {destroy .nomobj}
toplevel .nomobj -background grey -width 250 -height 100
wm geometry .nomobj +50+50
frame .nomobj.frame -background grey -width 250 -height 100
pack .nomobj.frame -side top
label .nomobj.frame.labobj -font {Helvetica 10} -text [mc {Nom du scenario :}] -background grey
pack .nomobj.frame.labobj -side top 
entry .nomobj.frame.entobj -font {Helvetica 10} -width 10
pack .nomobj.frame.entobj -side top 
button .nomobj.frame.ok -background gray75 -text [mc {Ok}] -command "verifnomobj"
pack .nomobj.frame.ok -side top -pady 10
}

proc verifnomobj {} {
global nom listdata indexpage totalpage arg tabcoulwagon compteligne Home
set ext .conf
set nom [join [.nomobj.frame.entobj get] ""]
if {$nom !=""} {
set indexpage $totalpage
incr totalpage
lappend listdata \173$nom\0401\175\040\173\1731\0401\0400\0400\175\040\1731\0401\0400\0400\0400\0400\0400\0400\175\040\1731\0401\175\040\1731\0401\0401\175\040\1732\0405\04010\175\040\1738\04010\04040\175\040\1738\04012\04040\175\175

set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $listdata
close $f
charge $indexpage
}
catch {destroy .nomobj}
}

interface $arg
