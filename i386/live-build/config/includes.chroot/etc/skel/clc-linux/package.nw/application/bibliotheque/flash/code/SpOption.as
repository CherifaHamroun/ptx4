﻿package code
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	public class SpOption extends Sprite
	{
		public var tfTitre:CalcTextField;
		public var aCtrlOption:Array;
		
		public function SpOption()
		{
			aCtrlOption = new Array();
			tfTitre = new CalcTextField();
			tfTitre.multiline=false;
			tfTitre.wordWrap = false;
			tfTitre.textFormat.align="center";
			tfTitre.textFormat.size=18;
			tfTitre.textFormat.ajouteGras();
			tfTitre.defaultTextFormat = tfTitre.textFormat;
			tfTitre.text = "Options";
			tfTitre.x = (735 - tfTitre.width)/2;
			tfTitre.y = 10;
			addChild(tfTitre);
		}
		
		
	}
}